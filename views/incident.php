<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');
	
	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}
$IS_ADMIN=$_SESSION['IS_ADMIN'];
if($IS_ADMIN==1)
{
//header("Location: dashboard.php");	
}

			$USER_EMAIL=$_SESSION['USER_EMAIL'];
			//GETTING ID---------------
			$sql= "SELECT MAX(Incident_Id) AS TICKET FROM incident ";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$ID_TEMP=$res[0]['TICKET']+1;
			//echo $ID_TEMP;die;
			$TICKET_ID=str_pad($ID_TEMP, 8, '0', STR_PAD_LEFT);
			//---------------------------
			 
			
			$sql= "SELECT * FROM impact ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$impact="";
			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Type=$res[$y]['Type'];
			$Description=$res[$y]['Description'];
			$impact .="<option value='$Type'>$Description</option>";
			
			}
			$sql= "SELECT * FROM incident_type ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$incident="";
			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Type=$res[$y]['Type'];
			$Description=$res[$y]['Description'];
			$incident .="<option value='$Type'>$Description</option>";
			
			}
			$sql= "SELECT * FROM urgency ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$urgency="";
			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Type=$res[$y]['Type'];
			$Description=$res[$y]['Description'];
			$urgency .="<option value='$Type'>$Description</option>";
			
			}
			
			$sql= "SELECT MAX(Login_Id) AS MAX_ID FROM users";
			//echo $sql;die;

			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$LAST_ID=$res[0]['MAX_ID']+1;
			$sql= "SELECT Company_Id, Company_Name FROM client WHERE Company_Id >0";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Company_Id=$res[$y]['Company_Id'];
			$Company_Name=$res[$y]['Company_Name'];
			$option_data .="<option value='$Company_Id'>$Company_Name</option>";
			
			}
			
			
			
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
	   
</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

	   <?php include 'nav.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-8">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Create New Incident </h4>
									<!-- <p class="category">Your Ticket ID : <font color ='black' ><b id='tick'><?php echo $TICKET_ID;?></b></font>&nbsp;</p> -->
	                            </div>
	                            <div class="card-content" id='form_content'>
	                           
	                                    <div class="row">
	                                        <div class="col-md-5">
												<div class="form-group label-floating">
													<!-- <label class="control-label">Incident ID</label> -->
													<input type="hidden" class="form-control" id='incident_id' value=" <?php echo $TICKET_ID?>" readonly style='color:red;background:#f7f1f1;border-radius:15px;width:100px;'>
												</div>
	                                        </div>
	                                        <div class="col-md-3">
												<div class="form-group label-floating">
													
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													
												</div>
	                                        </div>
	                                    </div>
									
										<?php  if($_SESSION['IS_CLIENT']==0)
										{?>
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Company</label>
													<select  class="form-control"  id='company' onchange='change_company(this.value);'>
													<option></option>
													<?php echo $option_data;?>
													</select>
												</div>
	                                        </div>
	                                    </div>
										<?php
										}?>
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Incident Summary</label>
														<input type="text" class="form-control" id='summary'>
												</div>
	                                        </div>
	                                    </div>
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Incident Details</label>
														<textarea class="form-control" rows="5" id='details'></textarea>
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
										    <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Affected Products</label>
													<select class="form-control" id='affected_product' onchange="populate_(this.value);">
													<option value=""></option>
													<option value="x100">Core-Banking(x100)</option>
													<option value="Internet Banking">Internet Banking</option>
													<option value="Atm">Atm</option>
													<option value="Corporate Internet Banking">Corporate Internet Banking</option>
													<option value="Mobile Banking(USSD)">Mobile Banking(USSD)</option>
													<option value="Mobile Banking(APP)">Mobile Banking(APP)</option>
													<option value="Document Management">Document Management</option>
													<option value="Offline Report Server">Offline Report Server</option>
													<option value="Systems">Systems</option>
													<option value="Sms Alerts">Sms Alerts</option>
													<option value="Email Alerts">Email Alerts</option>
													<option value="Network">Network</option>
													<option value="Operating System">Operating System</option>
													<option value="Others">Others</option>
													</select>
												</div>
	                                        </div>
											
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Area Affected</label>
													<select class="form-control" id='affected_area'>
													<option value=''></option>
													</select>
												</div>
	                                        </div>
	                                    
											<div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Incident Type</label>
													<select class="form-control" id='incident'>
													<option value=""></option>
													<?php echo $incident;?>
													</select>
												</div>
	                                        </div>
	                                    </div>

	                                   

	                                    <div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Impact</label>
													<select class="form-control" id='impact'>
													<option value='Moderate'>Moderate/Limited</option>
													<?php echo $impact;?>
													</select>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Urgency</label>
													<select class="form-control" id='urgency'>
													<option value=''></option>
													<?php echo $urgency;?>
													</select>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Priority</label>
													<select class="form-control" id='priority'>
													<option value=''></option>
													<option value='1'>High</option>
													<option value='2'>Medium</option>
													<option value='3'>Low</option>
													</select>
												</div>
	                                        </div>
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Department</label>
														<input type="text" class="form-control" id='department'>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Person Affected</label>
														<input type="text" class="form-control" id='person_affected'>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Affected Person's Mobile</label>
														<input type="tel" class="form-control" id='affected_contact'>
												</div>
	                                        </div>
	                                    </div>
										
										 <div class="row">
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Affected Person's Email(separate multiple emails with comma)</label>
														<input type="email" class="form-control" id='affected_person_email'>
												</div>
	                                        </div>
											


											 <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">BAU / Project issues</label>
														<select class="form-control" id='bau' onchange="append_ticket(this);">
													<option value=''></option>
													<option value='BAU'>BAU</option>
													<option value='PROJECT'>PROJECT</option>
													
													</select>
												</div>
	                                        </div>
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Number of Users affected for BAU issues<span style="color:red;">(required)*</span></label>
														<input type="text" class="form-control" id='No_affected_users' required>
												</div>
	                                        </div>
	                                    </div>
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
												<iframe src="../up.php" style='width:100%' scrolling='yes'></iframe>
													<div id='attachment' style='background:#ccc;width:200px;height:200px;display:none'  ></div>
												</div>
	                                        </div>
	                                    </div>
										 

	                                    
	                                    <button type="button" class="btn btn-primary pull-right" id='add'>Submit &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>
	                                    <div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						<div class="col-md-4">
    						<div class="card card-profile">
    							<div class="card-avatar">
    								<a href="#">
    									<img class="img" src="../assets/img/faces/ticket.png" />
    								</a>
    							</div>

    							<div class="content" >
    								<h6 class="category text-gray">New Ticket</h6>
    							
    								<p class="card-content">
    									Please fill in all detials before submiting. Once submitted, We will send a comfirmation email to acknowlege our receipt ...
    								</p><img src="../assets/img/faces/support.png" style='width:150px;'>
    								<a href="#" class="btn btn-primary btn-round" onclick='reload_("Creating New Environment Please wait","danger");'>Create New Ticket</a>
    							</div>
    						</div>
		    			</div>
					
	        </div>

	       	<?php include 'footer.php'?>
		</div>
		
	</div>

</body>

	<?php include 'footer_script.php'?>
		<script src="../js/add_incident.js" type="text/javascript"></script>
		
		<script>
	
	function getpath(path) {
    document.getElementById("uploadFile").value = path;
	};
	
function checkfile(sender) {

    var validExts = new Array(".jpeg", ".jpg",".png",".gif",".ico");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
	 fileExt=fileExt.toLowerCase();
	 
    if (validExts.indexOf(fileExt) < 0) {
		echo("Invalid file selected, Please Select Valid Image file of extension "+validExts.toString() + " types.","danger");
     
               $('input[type=file]').val('');
			       document.getElementById("uploadFile").value ="";
      return false;
    }
    else return true;
}
//echo("Please Register A New User Here","success");
function change_company(a)
{
	$.post( "change_company.php", 
	{ comp_id: a})
	  .done(function( data ) 
	  {
		  
				
		  });
}
function append_ticket(a_)

{
		var tick= $('#incident_id').val();
		
		if(tick[0]!=' ')
		{
			
		tick=tick.substring(2);
		}
		tick=tick.trim();

		
	if(a_.value=='BAU')
	{
	 document.getElementById("incident_id").value ="IN"+tick;
	  document.getElementById("tick").innerHTML ="IN"+tick;
	}
	else if(a_.value=='PROJECT')
	{
	document.getElementById("incident_id").value ="PJ"+tick;
	  document.getElementById("tick").innerHTML ="PJ"+tick;	
	}
	else
	{
	document.getElementById("incident_id").value =tick;
	  document.getElementById("tick").innerHTML =tick;		
	}
	return;
	
}
function populate_(products)
{
	switch(products)
	{
	case "x100":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
                                        			'<option value="Remittance">Remittance</option>'+
                                        			'<option value="Back Office">Back Office</option>'+
                                        			'<option value="E-Statement">E-Statement</option>'+
                                        			'<option value="Customer Service">Customer Service</option>'+
                                        			'<option value="Loan / Credit">Loan / Credit</option>'+
                                        			'<option value="Treasury/ Investment">Treasury / Investment</option>'+
                                        			'<option value="Finance">Finance</option>'+
                                        			'<option value="Hr">Hr</option>'+
                                        			'<option value="Cheque Processing">Cheque Processing</option>'+
                                        			'<option value="Cash Operation / Teller">Cash Operation / Teller</option>'+
                                        			'<option value="End of Day">End of Day</option>'+
                                        			'<option value="End of Year">End of Year</option>'+
                                        			'<option value="End of Month">End of Month</option>'+
													'<option value="Online Reports">Online Reports</option>'+
													'<option value="Signature">Signature</option>'+
													'<option value="ACP/ACH">ACP/ACH</option>'+
													'<option value="Treasury Bills">Treasury Bills</option>'+
													'<option value="Fixed Deposits">Fixed Deposits</option>'+
													'<option value="Funds Transfer">Funds Transfer</option>'+
													'<option value="GL Account Statements">GL Account Statements</option>'+
													'<option value="Authorization">Authorization</option>'+
													'<option value="SWIFT">SWIFT</option>'+
													'<option value="RTGS">RTGS</option>'+
													'<option value="Signature">Signature</option>'+
													'<option value="Thump print">Thump print</option>'+
													'<option value="Camera">Camera</option>'+
													'<option value="Voucher Scanner">Voucher Scanner</option>'+
													'<option value="Customer Statement">Customer Statement</option>'+
													'<option value="Statement by Email">Statement by Email</option>'+
													'<option value="SMS/Alerts">SMS/Alerts</option>'+
													'<option value="Direct Credit">Direct Credit</option>'+
													'<option value="Cheque Book">Cheque Book</option>'+
													'<option value="CHQ Maintenance">CHQ Maintenance</option>'+
													'<option value="Cashier Till">Cashier Till</option>'+
													'<option value="Salary Upload">Salary Upload</option>'+
													'<option value="Block Account">Block Account</option>'+
													'<option value="Charges">Charges</option>'+
													'<option value="Password">Password</option>'+
													'<option value="Counter Cheque">Counter Cheque</option>'+
													'<option value="Charges Concession">Charges Concession</option>'+
													'<option value="Network">Network</option>'+
													'<option value="Printer">Printer</option>'+
													'<option value="Trade Finance">Trade Finance</option>'+
													'<option value="Loans">Loans</option>'+
													'<option value="Overdraft">Overdraft</option>'+
													'<option value="Temporary Overdraft(TOD)">Temporary Overdraft(TOD)</option>'+
													'<option value="Operations">Operations</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Internet Banking":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Online Reports">Online Statements</option>'+
													'<option value="Funds Transfer">Funds Transfer</option>'+
													'<option value="Account Balance">Account Balance</option>'+
													'<option value="SWIFT">SWIFT</option>'+
													'<option value="RTGS">RTGS</option>'+
													'<option value="SMS/Alerts">SMS/Alerts</option>'+
													'<option value="Cheque Book Request">Cheque Book Request</option>'+
													'<option value="Atm Card Request">Atm Card Request</option>'+
													'<option value="Password">Password</option>'+
													'<option value="Network">Network</option>'+
													'<option value="Blink Pay">Blink Pay</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Corporate Internet Banking":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Online Reports">Online Statements</option>'+
													'<option value="Funds Transfer">Funds Transfer</option>'+
													'<option value="Account Balance">Account Balance</option>'+
													'<option value="SWIFT">SWIFT</option>'+
													'<option value="RTGS">RTGS</option>'+
													'<option value="SMS/Alerts">SMS/Alerts</option>'+
													'<option value="Online Salary Upload">Online Salary Upload</option>'+
													'<option value="Online Cheque Deposit">Online Cheque Deposit</option>'+
													'<option value="Cheque Book Request">Cheque Book Request</option>'+
													'<option value="Password">Password</option>'+
													'<option value="Network">Network</option>'+
													'<option value="Approval">Approval</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Atm":
	{
			document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Not Working">Not Working</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	
	case "Mobile Banking(USSD)":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Not Working">Not Working</option>'+
													'<option value="Others">Others</option>';
		break;
	}
	case "Mobile Banking(APP)":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Online Reports">Online Statements</option>'+
													'<option value="Funds Transfer">Funds Transfer</option>'+
													'<option value="Account Balance">Account Balance</option>'+
													'<option value="SMS/Alerts">SMS/Alerts</option>'+
													'<option value="Cheque Book Request">Cheque Book Request</option>'+
													'<option value="Atm Card Request">Atm Card Request</option>'+
													'<option value="Password">Password</option>'+
													'<option value="Network">Network</option>'+
													'<option value="Others">Others</option>';
		break;
	}
	case "Document Management":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Scanner Problems">Scanner Problems</option>'+
													'<option value="Reference No">Reference No</option>'+
													'<option value="Others">Others</option>';
		break;
	}
	case "Offline Report Server":
	{
			document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Report Not Showing">Report Not Showing</option>'+
													'<option value="Wrong Reports">Wrong Report</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Systems":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Offline">Database Offline</option>'+
													'<option value="Database Slow">Database Slow</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Sms Alerts":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Unable To Recieve Alerts">Unable To Recieve Alerts</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Email Alerts":
	{
			document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Unable To Recieve Alerts">Unable To Recieve Alerts</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Network":
	{
			document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Local Area Network">Local Area Network</option>'+
													'<option value="Wide Area Network">Wide Area Network</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
	case "Operating System":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Blue Screens">Blue Screens</option>'+
													'<option value="Dead Locks">Dead Locks</option>'+
													'<option value="License">License</option>'+
													'<option value="Others">Others</option>';
		break;
	}	
		case "Others":
	{
		document.getElementById("affected_area").innerHTML='<option value=""></option>'+
													'<option value="Browser">Browser</option>'+
													'<option value="Others">Others</option>';




		break;
	}	
	default:
	{
		alert("option not selected");
	}
	}
	
	return;
	
}
</script>
	
</html>
