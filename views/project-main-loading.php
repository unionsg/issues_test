
<?php




$ProjectDesc="";
$projectClient="";
$projectCountry="";
$assignedTo="";

require_once('../Connections/ticket.php');
require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();

	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");
}
$IS_ADMIN=$_SESSION['IS_ADMIN'];
if($IS_ADMIN==1)
{
//header("Location: dashboard.php");
}

$sql= "SELECT Company_Id, Company_Name FROM client";
//echo $sql;die;
$option_data="";
$stmt = $conn->prepare($sql);
$stmt->execute();
$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
for ($y = 0; $y < count($res); $y++)
{
 $count=$y+1;
$Company_Id=$res[$y]['Company_Id'];
$Company_Name=$res[$y]['Company_Name'];
$option_data .="<option value='$Company_Id'>$Company_Name</option>";

}


if(isset($_POST["create_project"]))
{
	//saving project section
	$projectId=$_POST["projectid"];
	$projectName=$_POST["projectname"];
	$projectDesc=$_POST["project_description"];
	$projectClient=$_POST["client_name"];
	$projectCountry=$_POST["project_country"];
	$assignedTo=$_POST["support_id"];
	$start_date=$_POST["start_date"];
	$end_date=$_POST["end_date"];
	$percentage_completion=$_POST["percentage_completion"];
	$expected_resolution_date=$_POST["expected_resolution_date"];





	$sql= "INSERT INTO project (project_id, project_name,
		project_description,country,client_id,assigned_to_support_person,start_date,end_date,percentage_completion,expected_resolution_date)
		VALUES ('$projectId','$projectName','$projectDesc','$projectCountry','$projectClient','$assignedTo','$start_date','$end_date','$percentage_completion','$expected_resolution_date')";
	$stmt = $conn->prepare($sql);
 	$result=$stmt->execute();
}

if(isset($_POST["btnedit"]))
{
	//saving project section
	$projectId=$_POST["edit_projectid"];
	$projectName=$_POST["edit_projectname"];
	$projectDesc=$_POST["edit_project_description"];
	$projectClient=$_POST["edit_client_name"];
	$projectCountry=$_POST["edit_project_country"];
	$assignedTo=$_POST["edit_support_id"];
	$start_date=$_POST["edit_start_date"];
	$end_date=$_POST["edit_end_date"];

	$sql= "UPDATE project SET project_name=$projectName',project_description='$projectDesc',
	country='$projectCountry',client_id='$projectClient',assigned_to_support_person='$assignedTo',
	start_date='$start_date',end_date='$end_date' WHERE project_id='$projectId' ";
	$stmt = $conn->prepare($sql);
 	$result=$stmt->execute();
}


$projectList=queryList(null,$conn);

if(isset($_POST["btnsort"]))
{
	//sorting section
	$projectList=queryList($_POST,$conn);

}


function queryList($DATA,$conn)
{
	if($DATA==null)
	{
		//load all
		$sql= "SELECT * FROM project inner join users on users.Login_Id=project.assigned_to_support_person inner join client on client.Company_Id=project.client_id";

	}
	else
	{
		//sorting

		$sort_client_name=$DATA["sort_client_name"];
		$sort_support_name=$DATA["sort_support_name"];
    $where="";
    if(strlen($sort_client_name)>1 && strlen($sort_support_name)>1)
    {
      $where=" WHERE project.client_id='$sort_client_name' AND project.assigned_to_support_person='$sort_support_name' ";

    }
    elseif(strlen($sort_client_name)>1)
    {
      $where=" WHERE project.client_id='$sort_client_name' ";

    }
    elseif(strlen($sort_support_name)>1)
    {
      $where=" WHERE project.assigned_to_support_person='$sort_support_name' ";

    }


		$sql= "SELECT * FROM project inner join users on users.Login_Id=project.assigned_to_support_person inner join client on client.Company_Id=project.client_id $where";

	}

  	$projectList="";
  	$stmt = $conn->prepare($sql);
  	$stmt->execute();
  	$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if(count($res)==0){
				$projectList="<div class='row'>
										<div class='col-md-12 card-1 cardv' >
										<center><h3>Nothing To Display</h3></center>
										</div>
										</div>";
		}
  	for ($y = 0; $y < count($res); $y++)
  	{
  	$ProjectName=$res[$y]['project_name'];
  	$ProjectDesc=$res[$y]['project_description'];
  	$country=$res[$y]['Country'];
  	$client_id=$res[$y]['Company_Name'];
  	$assigned_to_support_person=$res[$y]['Full_Name'];
  	$date_Created=$res[$y]['Date_Created'];
  	$project_id=$res[$y]['project_id'];
	$start_date=$res[$y]['start_date'];
	$end_date=$res[$y]['end_date'];
	$percentage_completion=$res[$y]['percentage_completion'];
	$expected_resolution_date=$res[$y]['expected_resolution_date'];
	$projectClient="";
	$projectCountry="";
	$assignedTo="";





    $decodedProjectId=base64_encode($project_id);
		$supportDecoded=base64_encode($assigned_to_support_person);
  	$projectList.="
  						<div class='row'>
  								<div class='col-md-12 card-1 cardv' >

  									<div class='row' style='margin-top:-30px;'>
  										<div class='col-md-1'>
  										<center><img src='../images/project.png' width='50px;'/></center>
  										</div>
  										<div class='col-md-10'>
  										<center><h3>$ProjectName-#PR-$project_id</h3></center>
  										</div>

											<div class='col-md-1'>
											<a href='#demo$y' class='pull-right' data-toggle='collapse'><b style='font-size:20px'>+</b></a>

  										</div>

  									</div>


  									<div class='row'>

  										<div class='col-md-12'>
  											<div class='row'>
  													<div class='col-md-1'>
  													<b>Country:</b><br/><i>$country</i>
  													</div>
  													<div class='col-md-2'>
  													<b>Client Name :</b><br/><i>$client_id</i>
  													</div>
  													<div class='col-md-2'>
  													<b>Assigned To :</b><br/> <i>$assigned_to_support_person</i>
  													</div>
														<div class='col-md-1'>
  													<b>Start Date :</b><br/> <i>$start_date</i>
  													</div>
														<div class='col-md-1'>
  													<b>End Date:</b><br/> <i>$end_date</i>
  													</div>


  													<div class='col-md-1'>
  													<b>Percentage Completion:</b><br/> <i>$percentage_completion</i>
  													</div>


  													<div class='col-md-1'>
  													<b>Expected resolution date:</b><br/> <i>$expected_resolution_date</i>
  													</div>



														<!--<div class='col-md-1'>
														 <a id='delete' name='$project_id'><i class='material-icons'>delete</i> <br/>Delete</a>
														</div>-->
														<div class='col-md-2'>
													<a id='edit' name='$project_id' onclick=\"editProject('$project_id','$ProjectName','$ProjectDesc','$projectClient','$projectCountry','$assignedTo','$start_date','$end_date');\"><i class='material-icons'>edit</i><br/>Edit</a>
													 </div>

													 <div class='col-md-1'>
												 <a id='edit' name='$project_id' onclick=\"goto('$decodedProjectId','$supportDecoded');\" style='color:red;'><u><i class='material-icons'>content_paste</i><br/>View</u></a>
													</div>

  											</div>

												<div class='row'>
														<div class='col-md-12'>
															 <div id='demo$y' class='collapse'>
															 	 <hr/>
															 <b>Description :</b><br/><i>$ProjectDesc</i><!--<br/>Date Created:$date_Created-->


															 </div>
															 </div>

													</div>


  										</div>


  									</div>
  								</div>

  						</div>
  						";

	}

	return $projectList;

}
?>
