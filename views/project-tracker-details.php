<?php

require('project-incident-loading.php');
?>
<!doctype html>
<html lang="en">
<head>
	<?php include 'header_script.php'; ?>
	<link href="../css/project-style.css?ver=<?php echo time();?>" rel="stylesheet" />

</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

<?php include 'nav.php'; ?>
    <div class="content">
        <div class="container-fluid">
					<div class="row">
							<div class="col-md-12 card-header" >
								<div class='row'>
									<div class="col-md-5" >
											<h4><b>All Incidents Assigned To <?php echo base64_decode($_GET['support']);?> </b></h4>
									</div>
									<div class="col-md-7 " >
											<a href="project-tracker-dash.php"><button type="button" class="btn btn-info pull-right"  >Back &nbsp;&nbsp;</button></a>
											<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#Sort"><i class='material-icons'>dashboard</i>Sort By &nbsp;&nbsp;</button>
											<button type="button" class="btn btn-danger pull-right" onclick="window.location.assign('project-tracker-details.php?project_id=<?php echo $_GET['project_id']?>&support=<?php echo $_GET['support']?>')"><i class='material-icons'>refresh</i>Load All &nbsp;&nbsp;</button>
											<button type="button" class="btn btn-default pull-right" onclick="printElem('printArea');"><i class='material-icons'>print</i> Print &nbsp;&nbsp;</button>

								 </div>
								</div>
							</div>
					</div>
					<div id='printArea'>
						<?php echo $projectList;?>
					</div>
            <!-- <div class="row">
								<div class="col-md-12 card-1 cardv" >
								</div>
    			  </div> -->


					<?php include 'footer.php'?>
			 </div>

		</div>
		<!--=======================SORTING MODAL ====================================-->
		<!-- Modal -->
	<div id="Sort" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	         <h4 class="modal-title"><center><b>Select Criteria to Sort</b></center></h4>
	      </div>
	      <div class="modal-body">
					<form method="post">
					<!--===============START ROW======================-->
					 <div class='row'>
						 <div class="col-md-12">
								<div class="form-group label-floating">
								 <label class="control-label">Sort By Issue Status</label>
								 <select class="form-control" name='status'  rows="5" >
										 <option value=""></option>
										 <option value="1">UNASIGNED</option>
										 	<option value="2">ASIGNED</option>
											<option value="3">WIP</option>
											<option value="4">RESOLVED</option>
											<option value="5">CLOSED</option>
											<option value="7">CANCELLED</option>

								 </select>

								</div>
						 </div>

					 </div>
						<!--===============END ROW======================-->

							<!--===============START ROW======================-->
							 <div class='row'>
								 <div class="col-md-2">
									 <button type="submit"  name ='btnsort' class="btn btn-info" id='sort'>Query &nbsp;&nbsp;</button>

								 </div>

							 </div>
								<!--===============END ROW======================-->
						</form>
	      </div>

	    </div>

	  </div>
	</div>
	<!--=======================END SORTING MODAL ====================================-->


</body>

	<?php include 'footer_script.php'?>
	<?php include 'project_footer.php'?>

</html>
