
<?php

require_once('../Connections/ticket.php');
require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();

	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");
}
$IS_ADMIN=$_SESSION['IS_ADMIN'];
if($IS_ADMIN==1)
{
//header("Location: dashboard.php");
}


$projectList=queryList(null,$conn);

if(isset($_POST["btnsort"]))
{
	//sorting section
	$projectList=queryList($_POST,$conn);

}




function queryList($DATA,$conn)
{
	$project_id=base64_decode($_GET['project_id']);
	if($DATA==null)
	{
		//load all
		$sql= "SELECT * FROM project_linking
		inner join incident on incident.Ticket_Id=project_linking.ticket_id
		 where project_id='$project_id'";

	}
	else
	{

		$status=$DATA["status"];
		$where="";
		if(strlen($status)>0)
		{
			$where=" AND incident.Status='$status'";

		}
		$sql= "SELECT * FROM project_linking
		inner join incident on incident.Ticket_Id=project_linking.ticket_id
		 where project_id='$project_id' $where  ";
	}

  	$projectList="";
  	$stmt = $conn->prepare($sql);
  	$stmt->execute();
  	$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if(count($res)==0){
				$projectList="<div class='row'>
										<div class='col-md-12 card-1 cardv' >
											<center><h3>Nothing To Display</h3></center>
										</div>
										</div>";
		}
  	for ($y = 0; $y < count($res); $y++)
  	{
  	$Summary=$res[$y]['Summary'];
  	$Details=$res[$y]['Details'];
		$Date_Logged=$res[$y]['Date_Logged'];
		$Person_Affected=$res[$y]['Person_Affected'];
		$Affected_Person_Email=$res[$y]['Affected_Person_Email'];
		$Affected_Product=$res[$y]['Affected_Product'];
		$Affected_Area=$res[$y]['Affected_Area'];
		$Assigned_Date=$res[$y]['Assigned_Date'];
		$Resolved_Date=$res[$y]['Resolved_Date'];
		$Status=$res[$y]['Status'];
		$Resolution_Details=$res[$y]['Resolution_Details'];
		$Date_Resolved=$res[$y]['Resolved_Date'];
		$type=$res[$y]['Type'];
		$Ticket_Id=$res[$y]['Ticket_Id'];
		$root_cause=$res[$y]['root_cause'];
		$Assigned_To=$res[$y]['Assigned_To'];
		//GETTING SUPPORT NAME
		$supportFullName="";
		$sql= "SELECT * FROM users WHERE Login_Id='$Assigned_To'";
		$stmts = $conn->prepare($sql);
      	$stmts->execute();
      	$ress = $stmts->fetchAll(PDO::FETCH_ASSOC);
		if(isset($ress[0]['Full_Name']))
		{
		    $supportFullName=$ress[0]['Full_Name'];
		}
		
		$Status1="";
		//**************************************
		$now = time(); // or your date as well
						$your_date = strtotime($Date_Logged);
						$datediff = $now - $your_date;
						$days=round($datediff / (60 * 60 * 24));
						if($days>1){
							$days.=" Days";
						}
						else
						{
								$days.=" Day";
						}

						//****************************
				switch($Status)
						{
						case 1:
						{
						$Status1="Unassigned";
							continue;
						}
						case 2:
						{
							$Status1="Assigned";
							continue;
						}
						case 3:
						{
							$Status1="In Progress";
							continue;
						}
						case 4:
						{
							$Status1="Resolved";
							continue;
						}
						case 5:
						{
							$Status1="Closed";
							continue;
						}

				}


							//LOADING COMMENTS
						$RecentComment="";
						$RecentDate="";
						$sql_comment= "SELECT * From comments WHERE Ticket_Id='$Ticket_Id' ORDER BY Date_Posted DESC";
						//echo $sql;die;

							$stmt_comment = $conn->prepare($sql_comment);
							$stmt_comment->execute();

						 $res_comment = $stmt_comment->fetchAll(PDO::FETCH_ASSOC);
						 $COMMENT_IMAGE_OUTPUT="";
						 $COMMENT_OUTPUT="";
						for ($c = 0; $c < count($res_comment); $c++)
						{
							if($c==0){
								$RecentComment=$res_comment[$c]['Comment'];
								$RecentDate=$res_comment[$c]['Date_Posted'];
							}
						$COMMENT_POSTED_BY=$res_comment[$c]['Posted_By'];
						$COMMENT_COMMENT=$res_comment[$c]['Comment'];
						$COMMENT_IMAGE_=$res_comment[$c]['Image_Path'];
						$COMMENT_DATE_POSTED_=$res_comment[$c]['Date_Posted'];
						$COMMENT_IMAGE_OUTPUT="<output style='width:60%;'>";

							$Image_Path_Arr=explode(",",$COMMENT_IMAGE_);
							for($u=0;$u<sizeof($Image_Path_Arr);$u++)
							{
								$Image_Src=$Image_Path_Arr[$u];
								$COMMENT_IMAGE_OUTPUT.="<img src='$Image_Src' style='width:100%;'>";
							}
							$COMMENT_IMAGE_OUTPUT.="</output>";

							$COMMENT_OUTPUT.=
							"
							 <li class='left clearfix'><span class='chat-img pull-left'>
														<img src='../images/user1.png' width='50px' alt='User Avatar' class='img-circle' />
												</span>
														<div class='chat-body clearfix'>
																<div class='header'>
																		<strong class='primary-font'>Reply From : $COMMENT_POSTED_BY</strong> <small class='pull-right text-muted'>
																				<span class='fa fa-time'></span>Updated On: $COMMENT_DATE_POSTED_</small>
																</div>
																<p>
																	$COMMENT_COMMENT <br/> $COMMENT_IMAGE_OUTPUT
																</p>
														</div>
												</li>
							";
						}



							$projectList.="
					  						<div class='row'>
					  								<div class='col-md-12 card-1 cardv' >

					  									<div class='row' style='margin-top:-30px;'>
					  										<div class='col-md-1'>
					  										<center><img src='../images/ticket.png' width='30px;'/></center>
					  										</div>
					  										<div class='col-md-10'>
					  										<center><h3>$Summary-#$Ticket_Id</h3></center>
					  										</div>

																<div class='col-md-1'>
																<a href='#demo$y' class='pull-right' data-toggle='collapse'><b style='font-size:20px'>+</b></a>

					  										</div>

					  									</div>

															<div class='row'>
																<div class='col-md-6'>
																	<b>Issue Description:</b><br/> <i>$Details</i>
																 </div>

																 <div class='col-md-2'>
 																	<b>Status:</b><br/> <font color='blue'><i>$Status1<br/>($supportFullName)</i></font>
 																 </div>
																 <div class='col-md-2'>
																		<b>Recent Update:</b><br/> <i>$RecentComment</i>
																	 </div>
																 <div class='col-md-2'>
																 <b>Last Update:</b><br/> <i>$RecentDate</i>
																</div>

															</div>


																	<div class='row'>
																			<div class='col-md-12'>
																				 <div id='demo$y' class='collapse'>
																				 <hr/>
																				 <b>Resolution Details :</b><br/><i>$Resolution_Details-</i><br/>
																				 <b>Root Cause:</b><br/><i>$root_cause-</i>

																				 <div class='row'>

											  										<div class='col-md-12'>
											  											<div class='row'>
											  													<div class='col-md-4'>
											  													<span style='color:#858585'><b>Affected Person:</b> </span><br/><i>$Person_Affected-$Affected_Person_Email</i>
											  													</div>
											  													<div class='col-md-4'>
											  													<span style='color:#858585'><b>Affected Product:</b> </span><br/><i>$Affected_Product</i>
											  													</div>
											  													<div class='col-md-4'>
											  													<span style='color:#858585'><b>Affected Area:</b></span><br/> <i>$Affected_Area</i>
											  													</div>
											  											</div>
											  											<div class='row'>
											  													<div class='col-md-4'>
											  														<font color='blue'>Assigned Date:</font> <i>$Assigned_Date-</i>
																										</div>
											  													<div class='col-md-4'>
											  														<font color='brown'>Incident Type:</font> <i>$type</i>
											  													</div>
											  													<div class='col-md-4'>
											  													<font color='red'>Status:</font> <i>$Status1</i>
											  													</div>
											  											</div>

																							<div class='row'>
																									<div class='col-md-4'>
																										<font color='blue'>Date Logged:</font> <i>$Date_Logged-</i>
																										</div>
																									<div class='col-md-4'>
																										<font color='brown'>Resolved Date:</font> <i>$Date_Resolved-</i>
																									</div>
																									<div class='col-md-4'>
																									<font color='red'>Days Pending:</font> <i>$days-</i>
																									</div>
																							</div>

																							<div class='row'>
																							<hr/>
																								<div class='col-md-5'>
																								<h3> <center>Recent Comments</center></h3>
																								$COMMENT_OUTPUT
																								</div>
																							</div>

											  										</div>



											  									</div>
											  								</div>


																			</div>

																		</div>


					  								</div>

					  						</div>
					  						";

	}

	return $projectList;

}
?>
