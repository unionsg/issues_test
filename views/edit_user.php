<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}

	$sql= "SELECT users.Login_Id,users.Email,users.Department,users.Full_Name,client.Company_Name FROM users 
	INNER JOIN client ON client.Company_Id=users.Company_Id ORDER BY users.Full_Name ASC";
						//echo $sql;die;

			$table_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			
				$email=$res[$y]['Email'];
				$dept=$res[$y]['Department'];
				$fullname=$res[$y]['Full_Name'];
				$Company_Name=$res[$y]['Company_Name'];
				$id=$res[$y]['Login_Id'];
				$table_data.=
				"
				  <tr>
	                <td><b>$count</b></td>
	                <td>$fullname</td>
	                <td>$email</td>
	                <td>$dept</td>
					 <td>$Company_Name</td>
					 <td><u><span style='font-size:10px;' > <button onclick='fetch_user($id);' data-toggle='modal' data-target='#edit_users_dlg'><font color='green'><b>Edit </b></font></button></span></u></td>

					 <td><u><span style='font-size:10px;' > <button onclick='delete_user($id);'><font color='red'><b>Delete</b></font></button></span></u></td>
	                 </tr>
	                                       
				";
			}
			
			
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
	   <link href='../assets/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

	   <?php include 'nav.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Edit User Profile </h4>
									  </div>
	                            <div class="card-content" >
	                           
	                                   
	                                    <div class="row">
	                                        <div class="col-md-12">
												 <div class="card-content table-responsive">
												<table class="table table-hover" id='form_content'>
													<thead class="text-primary">
														<th>ID</th>
														<th>Name</th>
														<th>Email</th>
														<th>Department</th>
														<th>Company</th>
														<th>Edit</th>
														<th>Delete</th>
													</thead>
													<tbody>
													   <?php echo $table_data;?>
													   
													</tbody>
												</table>
											</div>
												
												
	                                        </div>
	                                    </div>


										<div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						
	                </div>
	            </div>
	        </div>

	       	<?php include 'footer.php'?>
		</div>
	</div>

</body>

	<?php include 'footer_script.php'?>
	<script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	
	<!--<script src="../js/add_company.js" type="text/javascript"></script>----->
	<script>
	$(document).ready(function(){
    $('#form_content').DataTable();
});

</script>
		<script src="../js/add_user.js" type="text/javascript"></script>
	
	
	<script>
	function fetch_user(id)
	{
					document.getElementById("detail_edit_user").innerHTML='<center> Fetching Data...<div class="loader"></div></center>';
							
					$.post( "../js/edit_user.php", 
					{ id: id})
					  .done(function( data )
					  {
						 
							 document.getElementById("detail_edit_user").innerHTML=data;
							
					   });
	}



</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
function delete_user(id)
	{
		if(confirm ("Do you really want to delete this Record"))
    //document.forms[0].submit(); 
 
		$.post( "delete_user.php", 
					{ id: id})
					  .done(function( data )
					  {

						//alert(data); return;
						console.log(data);
		if(data=="0"){
			alert("Record Deleted Successfully");return;
		}
		alert("Error deleting record");	
					   }

					   );
					  else
    return false;
		
		window.location.reload();
	}


</script>



<script>

  function UpdateRecord()  
  {
  	
if(confirm ("Do you really want to update this Record")){
  	var company= $('#company').val();	
	
  	var fname= $('#fname').val();	
  	var lname= $('#lname').val();	
  	var fullname= $('#fullname1').val();	
  	var phone= $('#phone').val();	
  	var email= $('#email').val();	
  	var city= $('#city').val();	
  	var country= $('#country').val();	
  	var is_client= $('#is_client').val();	
  	var description= $('#desc').val();
  	var department= $('#department').val();	
  	var is_cc= $('#is_cc').val();	
  	var id= $('#id').val();	
       // alert(id); return;
  

	var obj ={
		company:company,
		lname:lname,
		fname:fname,
		fullname1:fullname,
		phone:phone,
		email:email,
		city: city,
		country:country,
		is_client:is_client,
		desc:description,
		department:department,
		is_cc:is_cc,
		id:id
	};



	$.post("edit_user.update.php",obj).done(function(data) {

		//alert(data); return;

		console.log(data);
		if(data=="0"){
			alert("Record Updated Successfully");return;
		}
		alert("Error updating record");

	});

}

else
{return false;}

 }
</script>



</html>






