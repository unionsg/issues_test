<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
				//unset($_SESSION['FIRST_WELCOME']);die;
			if(!isset($_SESSION['USER_ID']))
			{
			header("Location: ../login.php");	
			}
			$comp_id=$_SESSION["IS_CLIENT"];
			$USER_ID=$_SESSION["USER_ID"];
			
			//CHECKING IF ADMIN OR USER
			$AND ="";
			if($comp_id!=0)
			{
				$AND="Company_Id='$comp_id' AND Logged_By_Id='$USER_ID' AND";
			}
			
			
			$sql= "SELECT * FROM users WHERE Company_Id='$comp_id' ORDER BY Date_Created  LIMIT 3";
						//echo $sql;die;

			$table_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
				$fullname=$res[$y]['F_Name'];
				$email=$res[$y]['Email'];
				$dept=$res[$y]['Department'];
				$table_data.=
				"
				  <tr>
	                <td>$count</td>
	                <td>$fullname</td>
	                <td>$email</td>
	              
	                 </tr>
	                                       
				";
			}
			
			
			//------------------------WEEKLY ISSUES RAISED--------------------------------------
			//------------------------WEEKLY ISSUES RAISED--------------------------------------
			
			$sql= "SELECT DAYNAME(Date_Logged) AS DAY, COUNT(Incident_Id) AS TOTAL FROM incident WHERE $AND 
			WEEK(Date_Logged)=WEEK(CURDATE()) AND MONTH(Date_Logged)=MONTH(CURDATE()) GROUP BY DAYNAME(Date_logged) ORDER BY DAY(Date_logged) ASC";
			//echo $sql;die;
			$WEEKLY_ARRAY_DAY="";
			$WEEKLY_ARRAY_DATA="";
			
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				
			$DAY=$res[$y]['DAY'][0];
			$TOTAL=$res[$y]['TOTAL'];
			if($y+1 < count($res))
			{
				$WEEKLY_ARRAY_DAY.="$DAY,";
				$WEEKLY_ARRAY_DATA.="$TOTAL,";
			}
			else
			{
				$WEEKLY_ARRAY_DAY.="$DAY";
				$WEEKLY_ARRAY_DATA.="$TOTAL";	
			}
			
			
			}
		
			
			//------------------------MONTHLY ISSUES RAISED--------------------------------------
			//------------------------MONTHLY ISSUES RAISED--------------------------------------
			
			$sql= "SELECT MONTHNAME(Date_Logged) AS MONTH_LOGGED, COUNT(Incident_Id) AS TOTAL FROM incident
			WHERE $AND YEAR(Date_Logged)=YEAR(CURDATE()) 
			GROUP BY MONTHNAME(Date_logged) ORDER BY MONTH(Date_logged) ASC";
			
			$MONTHLY_ARRAY_MONTH="";
			$MONTHLY_ARRAY_DATA="";
			
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				
			$MONTH=substr($res[$y]['MONTH_LOGGED'],0,3);
			
			$TOTAL=$res[$y]['TOTAL'];
			if($y+1 < count($res))
			{
				$MONTHLY_ARRAY_MONTH.="$MONTH,";
				$MONTHLY_ARRAY_DATA.="$TOTAL,";
			}
			else
			{
				$MONTHLY_ARRAY_MONTH.="$MONTH";
				$MONTHLY_ARRAY_DATA.="$TOTAL";	
			}
			
			
			}
			
			//------------------------HOURLY COMPLETED RAISED--------------------------------------
			//------------------------HOURLY COMPLETED RAISED--------------------------------------
			
			$sql= " SELECT DATE_FORMAT(Resolved_Date,'%h %p') AS HOUR_RESOLVED, COUNT(Incident_Id) AS TOTAL FROM incident 
			WHERE $AND DATE(Resolved_Date)=DATE(CURDATE()) 
			GROUP BY DATE_FORMAT(Resolved_Date,'%h %p') ORDER BY HOUR(Resolved_Date) ASC";
			
			
			$HOURLY_ARRAY_HOURLY="";
			$HOURLY_ARRAY_DATA="";
			
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				
			$HOUR=$res[$y]['HOUR_RESOLVED'];
			
			$TOTAL=$res[$y]['TOTAL'];
			if($y+1 < count($res))
			{
				$HOURLY_ARRAY_HOURLY.="$HOUR,";
				$HOURLY_ARRAY_DATA.="$TOTAL,";
			}
			else
			{
				$HOURLY_ARRAY_HOURLY.="$HOUR";
				$HOURLY_ARRAY_DATA.="$TOTAL";	
			}
			
			
			}
			
			
			//------------------------PENDING ISSUES LISTINGS ON GRID--------------------------------------
			//------------------------PENDING ISSUES LISTINGS ON GRID--------------------------------------
				$AND ="";
				if($comp_id!=0)
				{
					$AND="WHERE Company_Id='$comp_id' AND Logged_By_Id='$USER_ID' AND Status<3";
				}
			
				$sql= "SELECT Summary, Date_Logged, Incident_Id,Ticket_Id FROM incident $AND
				ORDER BY Date_Logged DESC  LIMIT 3 ";// IF STATUS ==1 MEAN 'NEW'
				//echo $sql;die;
			
			$PENDING_GRID_TABLE='';
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			
			for ($y = 0; $y < count($res); $y++) 
			{
				
			$Summary=$res[$y]['Summary'];
			$logged=$res[$y]['Date_Logged'];
			$Incident_Id=$res[$y]['Incident_Id'];
			$Ticket_Id=$res[$y]['Ticket_Id'];
			
			//GETTING COMMENT
			$sql_comment= "SELECT COUNT(Comment) AS TOTAL FROM comments WHERE Ticket_Id='$Ticket_Id'";
				//echo $sql;die;
			
			
			$stmt_comment = $conn->prepare($sql_comment);
			$stmt_comment->execute();
			$res_comment = $stmt_comment->fetchAll(PDO::FETCH_ASSOC);
			$COMMENT=$res_comment[0]['TOTAL'];
			
			$PENDING_GRID_TABLE.=
			"
			<tr>
														<td>
															<div class='checkbox'>
																<label>
																	<i class='fa fa-bug' style='font-size:24px;'></i>
																</label>
															</div>
														</td>
														<td>$Summary <font color='#9c27b0' style='font-size:10px;'><i><br/>Last Logged : $logged </i></font> </td>
														<td class='td-actions text-right'>
															<button type='button' rel='tooltip' title='View Issue' class='btn btn-primary btn-simple btn-xs' data-toggle='modal' data-target='#detail_incident' onclick=\"get_details('$Ticket_Id');\">
																<i class='fa fa-folder-open'></i>&nbsp;&nbsp;&nbsp;<i class='fa fa-comments-o' style='color:red;' >&nbsp; <u>$COMMENT <span style='font-size:10px;'> comment</span></u></i>
															</button>
															
														</td>
													</tr>
			"
			;
			
			
			}
			
			//------------------------CLOSED ISSUES LISTINGS ON GRID--------------------------------------
			//------------------------CLOSED ISSUES LISTINGS ON GRID--------------------------------------
				$AND ="";
				if($comp_id!=0)
				{
					$AND="Company_Id='$comp_id' AND Logged_By_Id='$USER_ID' AND ";
				}
			
				$sql= "SELECT Summary, Date_Logged FROM incident WHERE  $AND
				Status='5' ORDER BY Date_Logged DESC  LIMIT 3 ";
			
			$CLOSED_GRID_TABLE='';
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				
			$Summary=$res[$y]['Summary'];
			$logged=$res[$y]['Date_Logged'];
			$CLOSED_GRID_TABLE.=
			'
													<tr>
														<td>
															<div class="checkbox">
																<label>
																	<i class="fa fa-bug" style="font-size:24px;"></i>
																</label>
															</div>
														</td>
														<td>'.$Summary.' <font color="#9c27b0" style="font-size:10px;"><i><br/>Last Logged :'.$logged.' </i></font> </td>
														<td class="td-actions text-right">
															<button type="button" rel="tooltip" title="View Issue" class="btn btn-primary btn-simple btn-xs">
																<i class="fa fa-question-circle"></i>
															</button>
															
														</td>
													</tr>
			';
			
			
			}
			
			//------------------------RESOLVED ISSUES LISTINGS ON GRID--------------------------------------
			//------------------------RESOLVED ISSUES LISTINGS ON GRID--------------------------------------
			
				$sql= "SELECT Summary, Date_Logged FROM incident WHERE $AND
				Status='4' ORDER BY Date_Logged DESC  LIMIT 3 ";// 
			
			$RESOLVED_GRID_TABLE='';
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				
			$Summary=$res[$y]['Summary'];
			$logged=$res[$y]['Date_Logged'];
			$RESOLVED_GRID_TABLE.=
			'
													<tr>
														<td>
															<div class="checkbox">
																<label>
																	<i class="fa fa-bug" style="font-size:24px;"></i>
																</label>
															</div>
														</td>
														<td>'.$Summary.' <font color="#9c27b0" style="font-size:10px;"><i><br/>Last Logged :'.$logged.' </i></font> </td>
														<td class="td-actions text-right">
															<button type="button" rel="tooltip" title="View Issue" class="btn btn-primary btn-simple btn-xs">
																<i class="fa fa-question-circle"></i>
															</button>
															
														</td>
													</tr>
			';
			
			
			}
			
			
			$sql= "SELECT * from users Where Company_Id='0'";
			$_SESSION["ALL_USERS"]="";
			$_SESSION["ALL_USERS_TEMP"]="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				
			$Email=$res[$y]['Email'];
			$FullName=$res[$y]['Full_Name'];
			$id=$res[$y]['Login_Id'];
			$_SESSION["ALL_USERS"].="<option value='$Email'>$FullName</option>";
			$_SESSION["ALL_USERS_TEMP"].="<option value='$id'>$FullName</option>";
			    
			}
			
		
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
</head>

<body>
	<?php include 'dialog.php';?>
	<div class="wrapper">

	   <?php include 'nav.php'; ?>
		

			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="orange">
									<i class="material-icons">content_copy</i>
								</div>
								<div class="card-content">
									<p class="category">Unassigned  Issues</p>
									<h3 class="title"><?php echo $PENDING;?></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons ">update</i> <a href="<?php echo "search_incident.php?status=".base64_encode("1");?>">View Details...</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="blue">
									<i class="fa fa-cart-arrow-down"></i>
								</div>
								<div class="card-content">
									<p class="category">Assigned Issues</p>
									<h3 class="title"><?php echo $ASSIGNED;?></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> <a href="<?php echo "search_incident.php?status=".base64_encode("2");?>">View Details...</a>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="purple">
									<i class="fa fa-clock-o"></i>
								</div>
								<div class="card-content">
									<p class="category">Issues Work In Progress</p>
									<h3 class="title"><?php echo $INPROGRESS;?></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
									
										<i class="material-icons">update</i> <a href="<?php echo "search_incident.php?status=".base64_encode("3");?>">View Details...</a>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header" data-background-color="green">
									<i class="fa fa-check"></i>
								</div>
								<div class="card-content">
									<p class="category">Resolved Issues</p>
									<h3 class="title"><?php echo $RESOLVED;?></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
									
										<i class="material-icons">update</i> <a href="<?php echo "search_incident.php?status=".base64_encode("4");?>">View Details...</a>
									</div>
								</div>
							</div>
						</div>
						

						
					</div>

					<div class="row">
						<div class="col-lg-6 col-md-12 col-sm-12">
							<div class="card card-stats">
								<div class="card-header" data-background-color="red">
									<i class="fa fa-external-link"></i>
								</div>
								<div class="card-content">
									<p class="category">Closed Issues</p>
									<h3 class="title"><?php echo $CLOSED;?></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">access_time</i> <a href="<?php echo "search_incident.php?status=".base64_encode("5");?>">View Details...</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-12 col-sm-12">
							<div class="card card-stats">
								<div class="card-header" data-background-color="pink">
									<i class="fa fa-thumbs-o-down"></i>
								</div>
								<div class="card-content">
									<p class="category">Suspended</p>
									<h3 class="title"><?php echo $COMMENTS;?></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> <a href="<?php echo "search_incident.php?status=".base64_encode("6");?>">View Details...</a>
									</div>
								</div>
							</div>
						</div>
						
						

						<div class="col-lg-6 col-md-12 col-sm-12">
							<div class="card card-stats">
								<div class="card-header" data-background-color="pink">
									<i class="fa fa-thumbs-o-down"></i>
								</div>
								<div class="card-content">
									<p class="category">Cancelled</p>
									<h3 class="title"><?php echo $CANCE;?></h3>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">update</i> <a href="<?php echo "search_incident.php?status=".base64_encode("7");?>">View Details...</a>
									</div>
								</div>
							</div>
						</div>
						


					</div>
					<!--SECOND ROW---------->
					
					<div class="row">
						<div class="col-md-4">
							<div class="card">
								<div class="card-header card-chart" data-background-color="green">
									<div class="ct-chart" id="dailySalesChart"></div>
								</div>
								<div class="card-content">
									<h4 class="title">Weekly Issues Raised</h4>
									<p class="category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> Issues raised  </span> From Mon- Fri.</p>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">access_time</i> updated 1 minutes ago
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<div class="card">
								<div class="card-header card-chart" data-background-color="orange">
									<div class="ct-chart" id="emailsSubscriptionChart"></div>
								</div>
								<div class="card-content">
									<h4 class="title">Monthly Issues Raised</h4>
								<p class="category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> Issues raised  </span> From Jan- Dec.</p>
								</div>
								<div class="card-footer">
									<div class="stats">
											<i class="material-icons">access_time</i> updated 1 minutes ago
									</div>
								</div>

							</div>
						</div>

						<div class="col-md-4">
							<div class="card">
								<div class="card-header card-chart" data-background-color="red">
									<div class="ct-chart" id="completedTasksChart"></div>
								</div>
								<div class="card-content">
									<h4 class="title">Daily Completed Issues </h4>
									<p class="category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> Issues completed  </span> 24hrs Update.</p>
								
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons">access_time</i> updated 1 minutes ago
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div class="card card-nav-tabs">
								<div class="card-header" data-background-color="purple">
									<div class="nav-tabs-navigation">
										<div class="nav-tabs-wrapper">
											<span class="nav-tabs-title">Recent #3:</span>
											<ul class="nav nav-tabs" data-tabs="tabs">
												<li class="active">
													<a href="#profile" data-toggle="tab">
														<i class="material-icons">bug_report</i>
														ISSUES
													<div class="ripple-container"></div></a>
												</li>
												<li class="">
													<a href="#messages" data-toggle="tab">
														<i class="material-icons">code</i>
														Closed
													<div class="ripple-container"></div></a>
												</li>
												<li class="">
													<a href="#settings" data-toggle="tab">
														<i class="material-icons">cloud</i>
														Resolved
													<div class="ripple-container"></div></a>
												</li>
											</ul>
										</div>
									</div>
								</div>

								<div class="card-content">
									<div class="tab-content">
										<div class="tab-pane active" id="profile">
											<div class="card-content table-responsive">
											<table class="table">
												<tbody>
													<?php echo $PENDING_GRID_TABLE;?>
													
												</tbody>
											</table>
											</div>
										</div>
										<div class="tab-pane" id="messages">
											<table class="table">
												<tbody>
													<?php echo $CLOSED_GRID_TABLE;?>
													
												</tbody>
											</table>
										</div>
										<div class="tab-pane" id="settings">
											<table class="table">
												<tbody>
													<?php echo $RESOLVED_GRID_TABLE;?>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="card">
	                            <div class="card-header" data-background-color="orange">
	                                <h4 class="title">Recent Users </h4>
	                                <p class="category">Recent users Created </p>
	                            </div>
	                            <div class="card-content table-responsive" style='overflow-y:scroll; width:100%;'>
	                                <table class="table table-hover">
	                                    <thead class="text-warning">
	                                        <th>ID</th>
	                                    	<th>Name</th>
	                                    	<th>Email</th>
	                                    	
	                                    </thead>
	                                    <tbody>
	                                       <?php echo $table_data;?>
	                                       
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
						</div>
					</div>
				</div>
			</div>

			
			<?php include 'footer.php'?>
		</div>
	</div>
	

</body>

	<?php include 'footer_script.php'?>
	
<script>

		type = ['','info','success','warning','danger'];
		//--------------------WEEKLY STATISTICS
		var WEEKLY=new Array();
		var WEEKLY_DATA=new Array();
		var WEEKLY1="<?php echo $WEEKLY_ARRAY_DAY?>";
		var WEEKLY_DATA1="<?php echo $WEEKLY_ARRAY_DATA?>";
		WEEKLY=WEEKLY1.split(',');
		WEEKLY_DATA=WEEKLY_DATA1.split(',')
		//--------------------MONTHLY STATISTICS
		var MONTHLY=new Array();
		var MONTHLY_DATA=new Array();
		var MONTHLY1="<?php echo $MONTHLY_ARRAY_MONTH?>";
		var MONTHLY_DATA1="<?php echo $MONTHLY_ARRAY_DATA?>";
		MONTHLY=MONTHLY1.split(',');
		MONTHLY_DATA=MONTHLY_DATA1.split(',')	
		
		//--------------------HOURLY STATISTICS
		var HOURLY=new Array();
		var HOURLY_DATA=new Array();
		var HOURLY1="<?php echo $HOURLY_ARRAY_HOURLY?>";
		var HOURLY_DATA1="<?php echo $HOURLY_ARRAY_DATA?>";
		HOURLY=HOURLY1.split(',');
		HOURLY_DATA=HOURLY_DATA1.split(',')	
		
	

demo = {
    initPickColor: function(){
        $('.pick-class-label').click(function(){
            var new_class = $(this).attr('new-class');
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if(display_div.length) {
            var display_buttons = display_div.find('.btn');
            display_buttons.removeClass(old_class);
            display_buttons.addClass(new_class);
            display_div.attr('data-class', new_class);
            }
        });
    },

    initFormExtendedDatetimepickers: function(){
        $('.datetimepicker').datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
         });
    },

    initDocumentationCharts: function(){
        /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

        dataDailySalesChart = {
            labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
            series: [
                [12, 17, 7, 17, 23, 18, 38]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart);
    },

    initDashboardPageCharts: function(){

        /* ----------==========     Daily Sales Chart initialization    ==========---------- */

        dataDailySalesChart = {
            labels: WEEKLY,
            series: [
                WEEKLY_DATA
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart);



        /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

        dataCompletedTasksChart = {
            labels: HOURLY,
            series: [
               HOURLY_DATA
            ]
        };

        optionsCompletedTasksChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 10, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
        }

        var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

        // start animation for the Completed Tasks Chart - Line Chart
        md.startAnimationForLineChart(completedTasksChart);



        /* ----------==========     Emails Subscription Chart initialization    ==========---------- */

        var dataEmailsSubscriptionChart = {
          labels: MONTHLY,
          series: [
           MONTHLY_DATA

          ]
        };
        var optionsEmailsSubscriptionChart = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 250,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
        };
        var responsiveOptions = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];
        var emailsSubscriptionChart = Chartist.Bar('#emailsSubscriptionChart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);

        //start animation for the Emails Subscription Chart
        md.startAnimationForBarChart(emailsSubscriptionChart);

    }



}

</script>
<?php
if(($comp_id!=0))
{?>
<!--Start of Tawk Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='twak.js';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);


})();

</script>
<!--End of Tawk Script-->	
<?php	
}
?>
<script src="../js/get_incident.js" type="text/javascript"></script>
</html>
