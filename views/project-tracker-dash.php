<?php

require('project-main-loading.php');

?>
<!doctype html>
<html lang="en">
<head>
	<?php include 'header_script.php'; ?>
	<link href="../css/project-style.css?ver=<?php echo time();?>" rel="stylesheet" />

</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

<?php include 'nav.php'; ?>
    <div class="content">
        <div class="container-fluid">
					<div class="row">
							<div class="col-md-12 card-header" >
								<div class='row'>
									<div class="col-md-6" >
											<h4><b>Recent Project</b></h4>
									</div>
									<div class="col-md-6 " >
											<button type="button" class="btn btn-info pull-right"  data-toggle="modal" data-target="#newProjectModal">+ New Project &nbsp;&nbsp;</button>
											<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#Sort"><i class='material-icons'>dashboard</i> Sort By &nbsp;&nbsp;</button>
											<button type="button" class="btn btn-danger pull-right" onclick="window.location.assign('project-tracker-dash.php')"><i class='material-icons'>refresh</i>Load All &nbsp;&nbsp;</button>
											
								 </div>
								</div>
							</div>
					</div>

					<div id='printArea'>
						<?php echo $projectList;?>
					</div>
            <!-- <div class="row">
								<div class="col-md-12 card-1 cardv" >
								</div>
    			  </div> -->


					<?php include 'footer.php'?>
			 </div>

		</div>


		<!--=======================NEW PROJECT MODAL ====================================-->
		<div id="newProjectModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">
 				<!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title"><center><b>Add A New Project</b></center></h4>
			      </div>
			      <div class="modal-body">
							<form method='post'>
							<!--===============START ROW======================-->
				       <div class='row'>
								 <div class="col-md-4">
										<div class="form-group label-floating">
										 <label class="control-label">Project ID</label>
											 <input type="text" readonly value="<?php echo time();?>" class="form-control" name='projectid' required>
										</div>
								 </div>
								 <div class="col-md-8">
										<div class="form-group label-floating">
										 <label class="control-label">Project Name</label>
											 <input type="text" class="form-control" name='projectname' required>
										</div>
								 </div>
							 </div>
							 	<!--===============END ROW======================-->


								<!--===============START ROW======================-->
								 <div class='row'>
									 <div class="col-md-12">
										 <div class="form-group label-floating">
											<label class="control-label">Project Description</label>
											<textarea class="form-control" rows="3" name='project_description' required></textarea>

										 </div>
									 </div>

								 </div>
									<!--===============END ROW======================-->

								<!--===============START ROW======================-->
								 <div class='row'>
									 <div class="col-md-6">
											<div class="form-group label-floating">
											 <label class="control-label">Client Name</label>
											 <select class="form-control" name='client_name'  rows="5" required>
													 <option value=""></option>
													 <?php echo $option_data;?>
											 </select>

											</div>
									 </div>
									 <div class="col-md-6">
											<div class="form-group label-floating">
											 <label class="control-label">Country</label>
											 <select class="form-control" name='project_country'  rows="5" required>
											 <option value=""></option>
											 <option value="Ghana">Ghana</option>
											  <option value="Liberia">Liberia</option>
												 <option value="Sierra Leone">Sierra Leone</option>
										 	 </select>
											</div>
									 </div>
								 </div>
									<!--===============END ROW======================-->

									<!--===============START ROW======================-->
									 <div class='row'>
										 <div class="col-md-6">
												<div class="form-group label-floating">
												 <label class="control-label">Start Date</label>
												 <input type='date' class="form-control" name='start_date' value='<?php echo date('Y-m-d');?>' />

												</div>
										 </div>
										 <div class="col-md-6">
												<div class="form-group label-floating">
												 <label class="control-label">End Date</label>
												  <input type='date' class="form-control" name='end_date' value='<?php echo date('Y-m-d');?>' />
												</div>
										 </div>
									 </div>
										<!--===============END ROW======================-->


									<!--===============START ROW======================-->
									 <div class='row'>
										 <div class="col-md-12">
												<div class="form-group label-floating">
												 <label class="control-label">Assign To</label>
												 <select class="form-control" name='support_id'  rows="5"  required>
														 <option value=""></option>
													  <?php echo $_SESSION["ALL_USERS_TEMP"];?>
												 </select>

												</div>
										 </div>

									 </div>
										<!--===============END ROW======================-->

									<!--===============START ROW======================-->
								 <div class='row'>
									 <div class="col-md-12">
										 <div class="form-group label-floating">
											<label class="control-label">Percentage Completion</label>
											<textarea class="form-control" rows="3" name='percentage_completion' required></textarea>

										 </div>
									 </div>

								 </div>
									<!--===============END ROW======================-->




									<!--===============START ROW======================-->
								 <div class='row'>
									 <div class="col-md-12">
										 <div class="form-group label-floating">
											<label class="control-label">Expected Resolution date</label>
											 <input type='date' class="form-control" name='expected_resolution_date' value='<?php echo date('Y-m-d');?>'  required />
										 </div>
									 </div>

								 </div>
									<!--===============END ROW======================-->



										<!--===============START ROW======================-->
										 <div class='row'>
											 <div class="col-md-2">
												 <button type="submit"  name ='create_project' class="btn btn-info" id='create_project'>Create Project &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>

											 </div>

										 </div>
											<!--===============END ROW======================-->

								</form>
			      </div>
						<!-- Modal Body End-->
			    </div>
					<!-- Modal content End-->

			  </div>
	 </div>

	<!--=======================END OF NEW PROJECT MODAL ====================================-->
	
	
	
		<!--=======================EDIT PROJECT MODAL ====================================-->
		<div id="edit_ProjectModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">
 				<!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title"><center><b>Edit Project</b></center></h4>
			      </div>
			      <div class="modal-body">
							<form method='post'>
							<!--===============START ROW======================-->
				       <div class='row'>
								 <div class="col-md-4">
										<div class="form-group label-floating">
										 <!--<label class="control-label">Project ID</label>-->
											 <input type="text" readonly class="form-control" name='edit_projectid'  id='edit_projectid' required>
										</div>
								 </div>
								 <div class="col-md-8">
										<div class="form-group label-floating">
										 <!--<label class="control-label">Project Name</label>-->
											 <input type="text" class="form-control" name='edit_projectname'  id='edit_projectname' required>
										</div>
								 </div>
							 </div>
							 	<!--===============END ROW======================-->


								<!--===============START ROW======================-->
								 <div class='row'>
									 <div class="col-md-12">
										 <div class="form-group label-floating">
											<!--<label class="control-label">Project Description</label>-->
											<textarea class="form-control" rows="3" name='edit_project_description' id='edit_project_description' required></textarea>

										 </div>
									 </div>

								 </div>
									<!--===============END ROW======================-->

								<!--===============START ROW======================-->
								 <div class='row'>
									 <div class="col-md-6">
											<div class="form-group label-floating">
											 <label class="control-label">Client Name</label>
											 <select class="form-control" name='edit_client_name' id='edit_client_name' rows="5" required>
													 <option value=""></option>
													 <?php echo $option_data;?>
											 </select>

											</div>
									 </div>
									 <div class="col-md-6">
											<div class="form-group label-floating">
											 <label class="control-label">Country</label>
											 <select class="form-control" name='edit_project_country' id='edit_project_country' rows="5" required>
											 <option value=""></option>
											 <option value="Ghana">Ghana</option>
											  <option value="Liberia">Liberia</option>
												 <option value="Sierra Leone">Sierra Leone</option>
										 	 </select>
											</div>
									 </div>
								 </div>
									<!--===============END ROW======================-->

									<!--===============START ROW======================-->
									 <div class='row'>
										 <div class="col-md-6">
												<div class="form-group label-floating">
												 <label class="control-label">Start Date</label>
												 <input type='date' class="form-control" name='edit_start_date' id='edit_start_date' value='<?php echo date('Y-m-d');?>' />

												</div>
										 </div>
										 <div class="col-md-6">
												<div class="form-group label-floating">
												 <label class="control-label">End Date</label>
												  <input type='date' class="form-control" name='edit_end_date' id='edit_end_date' value='<?php echo date('Y-m-d');?>' />
												</div>
										 </div>
									 </div>
										<!--===============END ROW======================-->


									<!--===============START ROW======================-->
									 <div class='row'>
										 <div class="col-md-12">
												<div class="form-group label-floating">
												 <label class="control-label">Assign To</label>
												 <select class="form-control" name='edit_support_id' id='edit_support_id'  rows="5"  required>
														 <option value=""></option>
													  <?php echo $_SESSION["ALL_USERS_TEMP"];?>
												 </select>

												</div>
										 </div>

									 </div>
										<!--===============END ROW======================-->
										<!--===============START ROW======================-->
										 <div class='row'>
											 <div class="col-md-2">
												 <button type="submit"  name ='btnedit' class="btn btn-info" id='btnedit'>Save Project &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>

											 </div>

										 </div>
											<!--===============END ROW======================-->

								</form>
			      </div>
						<!-- Modal Body End-->
			    </div>
					<!-- Modal content End-->

			  </div>
	 </div>

	<!--=======================EDIT OF  PROJECT MODAL ====================================-->
	
	

	<!--=======================SORTING MODAL ====================================-->
	<!-- Modal -->
<div id="Sort" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title"><center><b>Select Criteria to Sort</b></center></h4>
      </div>
      <div class="modal-body">
				<form method="post">
				<!--===============START ROW======================-->
				 <div class='row'>
					 <div class="col-md-12">
							<div class="form-group label-floating">
							 <label class="control-label">Sort By Client Name</label>
							 <select class="form-control" name='sort_client_name'  rows="5" >
									 <option value=""></option>
									 <?php echo $option_data;?>
							 </select>

							</div>
					 </div>

				 </div>
					<!--===============END ROW======================-->


					<!--===============START ROW======================-->
					 <div class='row'>
						 <div class="col-md-12">
								<div class="form-group label-floating">
								 <label class="control-label">Sort By Name Of Support Person</label>
								 <select class="form-control" name='sort_support_name'  rows="5" >
										 <option value=""></option>
										 <?php echo $_SESSION["ALL_USERS_TEMP"];?>
								 </select>

								</div>
						 </div>

					 </div>
						<!--===============END ROW======================-->

						<!--===============START ROW======================-->
						 <div class='row'>
							 <div class="col-md-2">
								 <button type="submit"  name ='btnsort' class="btn btn-info" id='create_project'>Query &nbsp;&nbsp;</button>

							 </div>

						 </div>
							<!--===============END ROW======================-->
					</form>
      </div>

    </div>

  </div>
</div>
<!--=======================END SORTING MODAL ====================================-->

</body>

	<?php include 'footer_script.php'?>
<?php include 'project_footer.php'?>
<script>
function goto(projectId,supportPersonName)
{
	window.location.assign("project-tracker-details.php?project_id="+projectId+"&support="+supportPersonName);
	return
}

function editProject(projectId,ProjectName,ProjectDesc,projectClient,projectCountry,assignedTo,start_date,end_date)
{
    $("#edit_projectid").val(projectId);
    $("#edit_projectname").val(ProjectName);
    $("#edit_project_description").val(ProjectDesc);
    $("#edit_client_name").val(projectClient);
    $("#edit_project_country").val(projectCountry);
    $("#edit_support_id").val(assignedTo);
    $("#edit_start_date").val(start_date);
    $("#edit_end_date").val(end_date);
    $('#edit_ProjectModal').modal('show'); 
}
</script>
</html>
