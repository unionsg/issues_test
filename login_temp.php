<?php

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	
	if(isset($_SESSION['USER_ID']))
	{
	header("location:views/dashboard.php");	
	}
?>


<!DOCTYPE html>

<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="extras/193b385c71"></script><script src="extras/nr-1039.min.js"></script>
</script><script data-apikey="99fc514c21bd30330f98cc1f203eb4f4" src="extras/bugsnag-2.min.js"></script>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"><meta content="width=device-width, initial-scale=1" name="viewport">
<link rel="shortcut icon" type="image/png" href="extras/logo1.png"/>
<title>Log in - Union Support</title>

<link href="extras/style.css" media="all" rel="stylesheet" type="text/css">
<script src="extras/public-282fb087887a597f9671f745c74c1f98.js" type="text/javascript"></script>
<style>
  body {
    background-color: #ffffff;
    background-image: url(extras/mountain.jpeg);
  }
</style>
<meta content="authenticity_token" name="csrf-param">
<meta content="R2byv458aBNne4kYV3N2Mhgc4yHOzHTHReuhjCMpMvE35mcyEYKsJZ1ioXlrOguVVqfl8CilsWNeAguQdcb9Lg==" name="csrf-token">
</head>
<body class="sessions create sessions_create">
<div class="container-fluid" id="main-container">
<div class="row" id="main-wrapper">
<section class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1" id="main">
<article class="panel panel-default">
<div class="bordered-smart"></div>
<div class="panel-body">
<img alt="" class="logo img-responsive" src="extras/logo.png"><img alt="" class="logo img-responsive" style='width:50px;' src="extras/support.jpg">

<div id="flash_messages">
</div>
	<center><h6><div id='mess' ></div><div class="loader" style="float:right;display:none;"></h6></center>
<div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"><input name="authenticity_token" type="hidden" value="R2byv458aBNne4kYV3N2Mhgc4yHOzHTHReuhjCMpMvE35mcyEYKsJZ1ioXlrOguVVqfl8CilsWNeAguQdcb9Lg=="></div><div class="form-group email required login_form_email has-error"><div class=""><input class="string email required form-control form-control" id="email" name="login_form[email]" placeholder="Email address" size="50" type="email" value=""></div></div>
<div class="form-group password required login_form_password has-error"><div class=""><input class="password required form-control" id="password" name="login_form[password]" placeholder="Password" size="50" type="password"></div></div>
<div class="form-group boolean optional login_form_remember_me"><div class=""><div class="checkbox"><input name="login_form[remember_me]" type="hidden" value="0"><div class="icheckbox_square-blue" style="position: relative;"><input class="boolean optional" id="login_form_remember_me" name="login_form[remember_me]" type="checkbox" value="1" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div><label class="boolean optional" for="login_form_remember_me"><h4><center>Welcome to our support portal</center></h4></label></div></div></div>
<div class="form-group"><input class="button btn btn-lg btn-primary btn-block" name="commit" type="button" id='login_' value="Log in"></div>



<p class="other_pages">
<a href="http://unionsg.com" class="btn btn-link">About Union Systems</a>
<a href="http://unionsg.com" class="btn btn-link">Our Products</a>
</p>

</div>
</article>
</section>
</div>
</div>
<footer>
&copy; <?php echo date('Y');?> Union Systems Support. All Rights Reserved <a href="http://unionsg.com/">Unionsg Teams</a>
<script src='js/jquery.min.js'></script>
<script src='js/login_process.js'></script>
</footer>


