<?php
@session_start();
//print_r($_SESSION);die;
$_SESSION['FILES']="";
$_SESSION['ID']=time();
?>
<html>
<head>
<link href="css/uploadfile.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="js/jquery.uploadfile.min.js"></script>
</head>
<body>
<div id="fileuploader">Upload</div>
<script>
$(document).ready(function() {
	$("#fileuploader").uploadFile({
		url:"php/upload.php",
		fileName:"myfile",
		sequential:true,
		sequentialCount:3,
		  acceptFiles: ".jpeg,.jpg,.png,.gif,.pdf",
		maxFileCount:5,
		maxFileSize:10000*1024 ,
		dragDrop: true, 
		showDelete: true,

deleteCallback: function (data, pd) {
	data=data.replace('["','');
	data=data.replace('"]','');
	//console.log(data);
        $.post("php/delete.php", {op: "delete",name: data},
            function (resp,textStatus, jqXHR) {
                //Show Message	

                console.log(resp);
            });
   
    pd.statusbar.hide(); //You choice.

},

onLoad:function(obj)
   {
   	$.ajax({
	    	cache: false,
		    url: "php/load.php",
	    	dataType: "json",
		    success: function(data) 
		    {
			    for(var i=0;i<data.length;i++)
   	    	{ 
       			obj.createProgress(data[i]["name"],data[i]["path"],data[i]["size"]);
       		}
	        }
		});
  }

	});
});
</script>
</body>
</html>

