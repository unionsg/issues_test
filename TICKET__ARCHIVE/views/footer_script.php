<!--   Core JS Files   -->

	<script src="../assets/js/jquery-3.1.0.min.js" type="text/javascript"></script>
	<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="../assets/js/material.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="../assets/js/chartist.min.js"></script>

	<!--  Notifications Plugin    -->
	<script src="../assets/js/bootstrap-notify.js"></script>
	
	
	<!--  Google Maps Plugin    -->
	

	<!--  javascript methods -->
	<script src="../assets/js/material-dashboard.js"></script>

	<!--  methods, don't include it in your project! -->
	<script src="../assets/js/demo.js"></script>
	
	<!--  update user password and prfile here -->
	<script src="../js/update_profile.js"></script>

	<script type="text/javascript">
	
	var PAGE ="<?php echo $PAGE;?>";
	
	switch(PAGE)
	{
	case '/views/dashboard.php':
	{
		$('#page1').addClass("active");
		break;
	}	
	case '/views/user.php':
	{
		$('#page2').addClass("active");
		break;
	}
	case '/views/company.php':
	{
		$('#page3').addClass("active");
		break;
	}		
	case '/views/incident.php':
	{
		$('#page4').addClass("active");
		break;
	}	
	case '/views/search_incident.php':
	{
		$('#page5').addClass("active");
		break;
	}	
	case '/views/assign.php':
	{
		$('#page6').addClass("active");
		break;
	}	
	case '/views/status.php':
	{
		$('#page7').addClass("active");
		break;
	}	
	case '/views/close.php':
	{
		$('#page8').addClass("active");
		break;
	}	
	case '/views/edit_user.php':
	{
		$('#page9').addClass("active");
		break;
	}	
	case '/views/edit_company.php':
	{
		$('#page10').addClass("active");
		break;
	}	
	case '/views/comment.php':
	{
		$('#page11').addClass("active");
		break;
	}	
	
	default:
	{
		
	}
		
	}
	
	var welcome=<?php if(!isset($_SESSION['FIRST_WELCOME'])){echo 0;}else{echo 1;}?>;
	var name="<?php echo $_SESSION['FNAME'].' From  '.$_SESSION['COMPANY'];?>";
	
	if (welcome!=1)
	{
		
		$.notify({
	// options
	message: 'Welcome Back,  '+name+'  ' ,
	icon: "notifications"
	},{
		// settings
		type: 'success'
	});

		
		//demo.showNotification('bottom','right');
		var assigned=<?php echo $ASSIGNED;?>;
		var pending=<?php echo $PENDING;?>;
		var comp_id=<?php echo $comp_id;?>;
		if(comp_id==0)
		{
		 setTimeout(function(){  echo(pending+" issues are pending and is currently not assigned to an agent","success");}, 5000);
		}
		else
		{
			if(assigned==0)
		{
			setTimeout(function(){  echo("Your have "+pending+" pending issues ","success");}, 5000);
		}
		else
		{
			setTimeout(function(){  echo("Your have no "+pending+" pending issues and "+assigned+" Of Your Issues Have Been Assigned to An Agent ","success");}, 5000);
		}
		}
		
		
		
	}
	else
	{
		var after="<?php $_SESSION['FIRST_WELCOME']=1; echo 1; ?>";
	}
    	$(document).ready(function(){

			// Javascript method's body can be found in assets/js/demos.js
        	demo.initDashboardPageCharts();

    	});
		function echo (message,type_)
		{
				$.notify({
	// options
	message: message ,
	icon: "notifications"
	},{
		// settings
		type: type_
	});
			
		}
		function reload_(a,b)
		{
		echo(a,b);	
		setTimeout(function(){window.location.reload();}, 1000);
		}
	</script>
	<script>
  function handleFileSelect(evt) {
	  echo("Uploading Image Files...","info");
	  $("#loader").show("slow");
	 
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
	var base64_array="";
	var base64_array1="";
	var done=0;
    for (var i = 0, f; f = files[i]; i++) {
		done=1;

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
		  
		 
		 base64_array=e.target.result;
		 base64_array+="~";
		  
		 
		  
		  document.getElementById("attachment").innerHTML+=base64_array;
		
		
		
          span.innerHTML = ['<img class="thumb"  src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
		  
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
	   
    }
	
	 if(done==1)
	 {
		setTimeout(function(){  echo("Upload Complete...","success"); $("#loader").hide("slow");}, 5000); 
	 }
	 else
	 {
		setTimeout(function(){  echo("Upload Failed...","danger"); $("#loader").hide("slow");}, 2000);  
	 }
	 
	
	
  }

  document.getElementById('file').addEventListener('change', handleFileSelect, false);
  
  function show_section(select)
  {
	  
	  var com="<textarea  rows='1' id='comm1' Placeholder='Please Comment Here'></textarea>";
	
	switch(Number(select))
				{
				
				case 2:
				{
					
					$.post( "../js/get_users.php", 
					{ select: select })
					  .done(function( data )
					  {
						 //console.log(data);
						
							var assign_users=" <select id='assign_agent1' style='valign:top;' ></select>";
							
						  document.getElementById("assign_users").innerHTML=assign_users;
						   document.getElementById("assign_agent1").innerHTML=data;
						    document.getElementById("comm").innerHTML=com;
						  });
					
					
				
					break;
				}	
				case 3:
				{
						document.getElementById("assign_users").innerHTML="";
					   document.getElementById("comm").innerHTML=com;
					break;
				}	
				case 4:
				{		document.getElementById("assign_users").innerHTML="";
					   document.getElementById("comm").innerHTML=com;
					break;
				}
				case 5:
				{
						document.getElementById("assign_users").innerHTML="";
					   document.getElementById("comm").innerHTML=com;
					   
					break;
				}			
				case 6:
				{
						document.getElementById("assign_users").innerHTML="";
					   document.getElementById("comm").innerHTML=com;
					   
					break;
				}			
				default:
				{	document.getElementById("assign_users").innerHTML="";
					document.getElementById("comm").innerHTML="";
				}
				}  
  }
  
  
  
	  function save_ticket_status(ticket_id,cstatus)
	  {
		  var comment='';
		  var assign_to='';
		switch(Number(cstatus))
					{
					
					case 2:
					{
						
					//---------------------------ASSIGN CODE HERE--------------------------------
					
					var assign_ticket=ticket_id;
					var assign_agent=$("#assign_agent1").val();
					comment=$("#comm1").val();
					
					if(assign_agent.length<1)
					{
						echo("Please Assign An Agent","danger");
						return;
					}
				
					$(".loader").show("slow");
				
					$.post( "../js/assign.php", 
					{ assign_ticket: assign_ticket,assign_agent: assign_agent,comment: comment })
					  .done(function( data )
					  {
						 console.log(data);
						 if(data==1)
						 {
							 document.getElementById("assign_container").innerHTML='<center> <h4 class="title">Ticket Id :<font color="red"><b>'+assign_ticket+'</b></font> Has Been Assigned Sucessfullly.  </h4><br/><img src="../images/check.png" style="width:50px;"></center>';
							$("#btn_assign").hide();
							$(".loader").hide();				
							
							echo("Incident Assigned Successfully","success"); 
							echo("Email Sent To Person Affected About Status Update","success"); 
							echo("Notification Sent To Support Person","success"); 
							setTimeout(function(){ window.location.reload();}, 4000);
								
						 }
						 else
						 {
							 
							echo("Error Assigning An Agent","danger"); 
							$(".loader").hide();
						 }
						
						
						
					   });
					
					
					//----------------------------END OF ASSIGFN CODE-------------------------------
					
						break;
					}	
					case 3:
					{
						//---------------START OF WIP-----------------------------------
						
						var status_ticket=ticket_id;
						var status_code=3;
						var status_comment=$("#comm1").val();
						if(status_code=='-')
						{
							echo("Please Change Status","danger");
							return;
						}
						if(status_comment.length<1)
						{
							echo("Please Comment ","danger");
							return;
						}
						
						
					
						$(".loader").show("slow");
					document.getElementById("btn_status").disabled = true;
						$.post( "../js/status.php", 
						{ assign_ticket: status_ticket,status_code: status_code,status_comment: status_comment })
						  .done(function( data )
						  {
							 console.log(data);
							 if(data==1)
							 {
								 document.getElementById("status_container").innerHTML='<center> <h4 class="title">Status Changed For Ticket Id :<font color="red"><b>'+status_ticket+'</b></font></h4><br/><img src="../images/check.png" style="width:50px;"></center>';
								
								$("#btn_status").hide();
								$(".loader").hide();				
								
								echo("Status Changed Successfully","success"); 
								echo("Email Sent To Person Affected About Status Update","success"); 
								echo("Notification Sent To Support Person About Status Update","success"); 
								document.getElementById("btn_status").disabled = false;
								setTimeout(function(){ window.location.reload();}, 4000);
									
							 }
							 else
							 {
								 
								echo("Error Changing Status","danger"); 
								$(".loader").hide();
							 }
							
							
							
						   });
						
						//--------------END OF WIP--------------------------
						
						
						
						break;
					}	
					case 4:
					{		
					
					//---------------START OF RESOLVED-------------------------------------------------------------
						
						var status_ticket=ticket_id;
						var status_code=4;
						var status_comment=$("#comm1").val();
						if(status_code=='-')
						{
							echo("Please Change Status","danger");
							return;
						}
						if(status_comment.length<1)
						{
							echo("Please Comment ","danger");
							return;
						}
						
						
					
						$(".loader").show("slow");
					document.getElementById("btn_status").disabled = true;
						$.post( "../js/status.php", 
						{ assign_ticket: status_ticket,status_code: status_code,status_comment: status_comment })
						  .done(function( data )
						  {
							 console.log(data);
							 if(data==1)
							 {
								 document.getElementById("status_container").innerHTML='<center> <h4 class="title">Status Changed For Ticket Id :<font color="red"><b>'+status_ticket+'</b></font></h4><br/><img src="../images/check.png" style="width:50px;"></center>';
								
								$("#btn_status").hide();
								$(".loader").hide();				
								
								echo("Status Changed Successfully","success"); 
								echo("Email Sent To Person Affected About Status Update","success"); 
								echo("Notification Sent To Support Person About Status Update","success"); 
								document.getElementById("btn_status").disabled = false;
								setTimeout(function(){ window.location.reload();}, 4000);
									
							 }
							 else
							 {
								 
								echo("Error Changing Status","danger"); 
								$(".loader").hide();
							 }
							
							
							
						   });
						
						//--------------END OF RESOLVED--------------------------
						
					
					
					
					
						break;
					}
					case 5:
					{
						///------------------START OF CLOSE--------------------------------------
						
						var close_ticket=ticket_id;
						var close_code=5;
						var close_comment=$("#comm1").val();
						if(close_code=='-')
						{
							echo("Please Change Status","danger");
							return;
						}
						if(close_comment.length<1)
						{
							echo("Please Comment ","danger");
							return;
						}
						
						
					
						$(".loader").show("slow");
						document.getElementById("btn_status").disabled = true;
						$.post( "../js/close.php", 
						{ assign_ticket: close_ticket,status_code: close_code,status_comment: close_comment })
						  .done(function( data )
						  {
							 console.log(data);
							 if(data==1)
							 {
								 document.getElementById("close_container").innerHTML='<center> <h4 class="title">Issue With Ticket Id :<font color="red"><b>'+close_ticket+'</b></font> Closed Successfully</h4><br/><img src="../images/check.png" style="width:50px;"></center>';
								$("#btn_status").hide();
								$(".loader").hide();				
								
								echo("Issue Closed Successfully","success"); 
								echo("Email Sent To Person Affected About Closed Issue","success"); 
								echo("Notification Sent To Support Person About  Closed Issue","success"); 
								setTimeout(function(){ window.location.reload();}, 4000);
								document.getElementById("btn_status").disabled = false;
									
							 }
							 else
							 {
								 
								echo("Error Closing Issue","danger"); 
								$(".loader").hide();
							 }
							
							
							
						   });
						
						//-------------------END OF CLOSE---------------------------------------
						   
						break;
					}	

					case 6:
					{
						///------------------START OF SUSPEND--------------------------------------
						
						var close_ticket=ticket_id;
						var close_code=6;
						var close_comment=$("#comm1").val();
						if(close_code=='-')
						{
							echo("Please Change Status","danger");
							return;
						}
						if(close_comment.length<1)
						{
							echo("Please Comment ","danger");
							return;
						}
						
						
					
						$(".loader").show("slow");
						document.getElementById("btn_status").disabled = true;
						$.post( "../js/close.php", 
						{ assign_ticket: close_ticket,status_code: close_code,status_comment: close_comment })
						  .done(function( data )
						  {
							 console.log(data);
							 if(data==1)
							 {
								 document.getElementById("close_container").innerHTML='<center> <h4 class="title">Issue With Ticket Id :<font color="red"><b>'+close_ticket+'</b></font> Closed Successfully</h4><br/><img src="../images/check.png" style="width:50px;"></center>';
								$("#btn_status").hide();
								$(".loader").hide();				
								
								echo("Issue Closed Successfully","success"); 
								echo("Email Sent To Person Affected About Closed Issue","success"); 
								echo("Notification Sent To Support Person About  Closed Issue","success"); 
								setTimeout(function(){ window.location.reload();}, 4000);
								document.getElementById("btn_status").disabled = false;
									
							 }
							 else
							 {
								 
								echo("Error Closing Issue","danger"); 
								$(".loader").hide();
							 }
							
							
							
						   });
						
						//-------------------END OF SUSPEND---------------------------------------
						   
						break;
					}
					
					default:
					{	document.getElementById("assign_users").innerHTML="";
						document.getElementById("comm").innerHTML="";
					}
					}    
		  
	  }
  
</script>
