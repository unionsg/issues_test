




<div id='detail_incident' class='modal fade' role='dialog'  >
								  <div class='modal-dialog modal-lg' style='width:75%;margin-top:-1px;'>

									<!-- Modal content-->
									<div class='modal-content'>
									 
									  <div class='modal-body' >
		
										<center><div id="detail_container"> </div></center>
									  </div>
									 <div class="modal-footer">
								
								  </div>
									</div>

								  </div>
								</div>
								
								
								
								<!------------EDIT USERS---------------->
								
								<div id='edit_users_dlg' class='modal fade' role='dialog'  >
								  <div class='modal-dialog modal-md' style='margin-top:-1px'>

									<!-- Modal content-->
									<div class='modal-content'>
									 <div class='modal-header'>
									  <h3>Edit User <img src='../images/profile.png' style='width:60px;float:right;'> </h3>
									
										
										
									  </div>
									 
									  <div class='modal-body' >
		
										<center><div id="detail_edit_user"> </div></center>
									  </div>
									 <div class="modal-footer">
								
								  </div>
									</div>

								  </div>
								</div>
								
								
								<!------------END OF EDIT USERS---------->
								
								
								
								<!------------EDIT COMPANY---------------->
								
								<div id='edit_company_dlg' class='modal fade' role='dialog'  >
								  <div class='modal-dialog modal-lg' style='width:75%;margin-top:-1px;'>

									<!-- Modal content-->
									<div class='modal-content'>
									 
									  <div class='modal-body' >
		
										<center><div id="detail_edit_company"> </div></center>
									  </div>
									 <div class="modal-footer">
								
								  </div>
									</div>

								  </div>
								</div>
								
								
								<!------------END OF EDIT COMPANY---------->
								
								
								
			<!------UPDATE PROFILR  --->
								
								<div id='update_profile' class='modal fade' role='dialog'  >
								  <div class='modal-dialog  modal-sm' >

									<!-- Modal content-->
									<div class='modal-content'>
									  <div class='modal-header'>
									  <h3>Profile Update <img src='../images/profile.png' style='width:60px;float:right;'> </h3>
									
										
										
									  </div>
									  <div class='modal-body' >
		
										<center><div id="profile_container"> 
										
										
										
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Full Name</label>
													<input type="text"  id='fullname' class="form-control" value="<?php echo $_SESSION['FULL_NAME'];?>" >
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Email</label>
													<input type="text"  id='email'  class="form-control" value="<?php echo $_SESSION['EMAIL'];?>">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Phone</label>
													<input type="tel"  id='phone'  class="form-control" value="<?php echo $_SESSION['PHONE'];?>">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										</div></center>
									  </div>
									 <div class="modal-footer">
									<button type="button" class="btn btn-default" id="update">Update &nbsp;<div class='loader' style='float:right;display:none;'></div> </button><button type="button" class="btn btn-default" data-dismiss="modal" id='prclose'>Close</button>
								  </div>
									</div>

								  </div>
								</div>
								
								
								
								
								
								
								
								<!------CHANGE PASSWORD  --->
								
								<div id='change_password' class='modal fade' role='dialog'   >
								  <div class='modal-dialog  modal-sm' >

									<!-- Modal content-->
									<div class='modal-content'>
									  <div class='modal-header'>
									  <h3>Reset Password  <img src='../images/password1.png' style='width:60px;float:right;'> </h3>
									
										
										
									  </div>
									  <div class='modal-body' >
		
										<center><div id="password_container"> 
										
										
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Old Password</label>
													<input type="password" id='oldpass'  class="form-control">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">New Password</label>
													<input type="password"  id='newpass' class="form-control" >
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Confirm Password</label>
													<input type="password"  id='confirmnewpass'  class="form-control">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										</div></center>
									  </div>
									 <div class="modal-footer" >
									<button type="button" class="btn btn-default" id="reset">Reset &nbsp;<div class='loader' style='float:right;display:none;'></div></button><button type="button" class="btn btn-default" data-dismiss="modal" id='pclose'>Close</button>
								  </div>
									</div>

								  </div>
								</div>
								
								
								
								
								
								<!------ASSIGN INCIDENT  --->
								
								<div id='assign_dialog' class='modal fade' role='dialog'   >
								  <div class='modal-dialog  modal-sm' >

									<!-- Modal content-->
									<div class='modal-content'>
									  <div class='modal-header'>
									  <h3>Assign Inicident  <i class='fa fa-cart-arrow-down' style='color:red;width:60px;float:right;'></i></h3>
									
										
										
									  </div>
									  <div class='modal-body' >
		
										<center><div id="assign_container"> 
										
										
										
										<div class="row" style='display:none;'>
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Ticket</label>
													<input type="text" id='assign_ticket'  class="form-control" style='color:red;font-size:bold;' readonly>
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Assign To</label>
													<select id='assign_agent' class="form-control" >
													
													</select>
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Assign To</label>
													<input type="text" id='acomment'  class="form-control" >
											
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										
										</div></center>
									  </div>
									 <div class="modal-footer" >
									<button type="button" class="btn btn-default" id="btn_assign">Assign &nbsp;<div class='loader' style='float:right;display:none;'></div></button><button type="button" class="btn btn-default" data-dismiss="modal" id='aclose'>Close</button>
								  </div>
									</div>

								  </div>
								</div>
								
								
								
								
								<!------CHANGE STATUS  --->
								
								<div id='status_dialog' class='modal fade' role='dialog'   >
								  <div class='modal-dialog  modal-sm' >

									<!-- Modal content-->
									<div class='modal-content'>
									  <div class='modal-header'>
									  <h3>Change Status <i class='fa fa-edit' style='color:red;width:60px;float:right;'></i></h3>
									
										
										
									  </div>
									  <div class='modal-body' >
		
										<center><div id="status_container"> 
										
										
										
										<div class="row" style='display:none;'>
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Ticket</label>
													<input type="text" id='status_ticket'  class="form-control" style='color:red;font-size:bold;' readonly>
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Change To</label>
													<select id='status_code' class="form-control" >
													<option value='-'>---Not Selected---</option>
													<option value='3'>Work In Progress</option>
													<option value='4'>Resolved</option>
													
													</select>
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Comment</label>
													<input type="text" id='status_comment'  class="form-control" >
												
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										
										</div></center>
									  </div>
									 <div class="modal-footer" >
									<button type="button" class="btn btn-default" id="btn_status">Change &nbsp;<div class='loader' style='float:right;display:none;'></div></button><button type="button" class="btn btn-default" data-dismiss="modal" id='aclose'>Close</button>
								  </div>
									</div>

								  </div>
								</div>
								
								
								
									
								<!------CLOSE ISSUE  --->
								
								<div id='close_dialog' class='modal fade' role='dialog'   >
								  <div class='modal-dialog  modal-sm' >

									<!-- Modal content-->
									<div class='modal-content'>
									  <div class='modal-header'>
									  <h3>Close Issue <i class='fa fa-remove' style='color:red;width:60px;float:right;'></i></h3>
									
										
										
									  </div>
									  <div class='modal-body' >
		
										<center><div id="close_container"> 
										
										
										
										<div class="row" style='display:none;'>
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Ticket</label>
													<input type="text" id='close_ticket'  class="form-control" style='color:red;font-size:bold;' readonly>
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Change To</label>
													<select id='close_code' class="form-control" >
													<option value='-'>---Not Selected---</option>
													<option value='5'>Close</option>
													
													
													</select>
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Comment</label>
													<input type="text" id='close_comment'  class="form-control" >
												
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										
										</div></center>
									  </div>
									 <div class="modal-footer" >
									<button type="button" class="btn btn-default" id="btn_close">Close Issue &nbsp;<div class='loader' style='float:right;display:none;'></div></button><button type="button" class="btn btn-default" data-dismiss="modal" id='aclose'>Close</button>
								  </div>
									</div>

								  </div>
								</div>
								