 <?php 
			$PAGE=$_SERVER['REQUEST_URI'];
			$comp_id=$_SESSION["IS_CLIENT"];
			$USER_ID=$_SESSION["USER_ID"];
			//CHECKING IF ADMIN OR USER
			$AND ="";
			if($comp_id!=0)
			{
				$AND="Company_Id='$comp_id' AND Logged_By_Id='$USER_ID' AND";
			}
 //------------------------PENDING ISSUES--------------------------------------
			//------------------------PENDING ISSUES--------------------------------------
			$PENDING=0; //PENDING ISSUES
			$sql= "SELECT COUNT(Status) AS PENDING FROM incident WHERE $AND Status='1'";// IF STATUS ==1 MEAN 'NEW'
			//echo $sql;die;
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$PENDING=$res[0]['PENDING'];
			
			
			
			
			//------------------------ASSIGNED ISSUES--------------------------------------
			//------------------------ASSIGNED ISSUES--------------------------------------
			$ASSIGNED=0; //ASSIGNED ISSUES
			$sql= "SELECT COUNT(Status) AS ASSIGNED FROM incident WHERE $AND Status='2'";// IF STATUS ==2 MEAN 'ASSIGNED'
			//echo $sql;die;
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$ASSIGNED=$res[0]['ASSIGNED'];
			
			
			//------------------------INPROGRESS ISSUES--------------------------------------
			//------------------------INPROGRESS ISSUES--------------------------------------
			$INPROGRESS=0; //PENDING ISSUES
			$sql= "SELECT COUNT(Status) AS INPROGRESS FROM incident WHERE $AND Status='3'";// IF STATUS ==1 MEAN 'NEW'
			//echo $sql;die;
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$INPROGRESS=$res[0]['INPROGRESS'];
			
			
			//------------------------RESOLVED ISSUES--------------------------------------
			//------------------------RESOLVED ISSUES--------------------------------------
			$RESOLVED=0; //RESOLVED ISSUES
			$sql= "SELECT COUNT(Status) AS RESOLVED FROM incident WHERE $AND  Status='4'";// IF STATUS ==4 MEAN 'RESOLVED'
			//echo $sql;die;
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$RESOLVED=$res[0]['RESOLVED'];
			
			//------------------------CLOSED ISSUES--------------------------------------
			//------------------------CLOSED ISSUES--------------------------------------
			$CLOSED=0; //CLOSED ISSUES
			$sql= "SELECT COUNT(Status) AS CLOSED FROM incident WHERE $AND Status='5'";// IF STATUS ==5 MEAN 'CLOSED'
			//echo $sql;die;
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$CLOSED=$res[0]['CLOSED'];
			
			
			//------------------------COMMENTS ISSUES--------------------------------------
			//------------------------COMMENTS ISSUES--------------------------------------
			$COMMENTS=0; //COMMENTS ISSUES
			$sql= "SELECT  COUNT(Status) AS SUSPENDED FROM incident WHERE $AND Status='6'";// IF STATUS ==5 MEAN 'CLOSED'
			//echo $sql;die;
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$COMMENTS=$res[0]['SUSPENDED'];

			
			$IS_ADMIN=$_SESSION['IS_ADMIN'];
			
			$HIDE_NAV="";
			$HIDE_NAV1="";
			$HIDE="style='display:none'";
			if($IS_ADMIN==0)
			{
				$HIDE_NAV="style='display:none'";
			}else
			{
			$HIDE_NAV1="style='display:none'";
			}
			
		$logo="";	
		if($comp_id!=0)
		{
			if(empty($_SESSION['LOGO']))
			{
				$logo="../images/logo.png";	
			}
			else{
			$logo="../logo/".$_SESSION['LOGO']."_0.png";
			}
		}	
		else
		{
		$logo="../images/logo.png";	
		}
 
 ?>
 <div class="sidebar" data-color="blue" data-image="../assets/img/sidebar-1.jpg">
			<!--
		        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

		        Tip 2: you can also add an image using data-image tag
		    -->

			<div class="logo">
				<a href="http://unionsg.com" class="simple-text">
				
					<img src='<?php echo $logo;?>' style='margin-left:-30px;width:235px'>
				</a>
			</div>

	    	<div class="sidebar-wrapper">
	            <ul class="nav">
	                <li id='page1'>
	                    <a href="dashboard.php">
	                        <i class="material-icons">dashboard</i>
	                        <p>Dashboard</p>
	                    </a>
	                </li>
					<li id='page5'>
	                    <a href="search_incident.php">
	                        <i class="material-icons">search</i>
	                        <p>Search Incident</p>
	                    </a>
	                </li>
	                <li <?php echo $HIDE_NAV;?> id='page2'>
	                    <a href="user.php">
	                        <i class="material-icons">person</i>
	                        <p>Add User </p>
	                    </a>
	                </li>
					<li <?php echo $HIDE_NAV;?> id='page3'>
	                    <a href="company.php">
	                        <i class="material-icons">home</i>
	                        <p>Add Client </p>
	                    </a>
	                </li>
	                <li  id='page4'>
	                    <a href="incident.php">
	                        <i class="material-icons">content_paste</i>
	                        <p>New Incident</p>
	                    </a>
	                </li>
					
					 
	                <li <?php echo $HIDE_NAV;?> id='page6'>
	                    <a href="assign.php">
	                        <i class="material-icons">library_books</i>
	                        <p>Assign Issues</p>
	                    </a>
	                </li>
					 <li <?php echo $HIDE_NAV;?> id='page7'>
	                    <a href="status.php">
	                        <i class="fa fa-edit"></i>
	                        <p>Change Status</p>
	                    </a>
	                </li>
					 <li <?php echo $HIDE_NAV;?> id='page8'>
	                    <a href="close.php">
	                        <i class="fa fa-close"></i>
	                        <p>Close Issue</p>
	                    </a>
	                </li>
					 
	                <li <?php echo $HIDE_NAV;?> id='page9'>
	                    <a href="edit_user.php">
	                         <i class="fa fa-gear"></i>
	                        <p>View/ Edit Users</p>
	                    </a>
	                </li>
	                 <li <?php echo $HIDE_NAV;?> id='page10'>
	                    <a href="edit_company.php">
	                         <i class="fa fa-gear"></i>
	                        <p>View/ Edit Client</p>
	                    </a>
	                </li>
					 <li <?php echo $HIDE_NAV1;?> id='page11'>
	                    <a href="comment.php">
	                        <i class="material-icons">chat</i>
	                        <p>Comment</p>
	                    </a>
	                </li>
	                
	            </ul>
	    	</div>
	    </div>

	    <div class="main-panel">
			<nav class="navbar navbar-transparent navbar-absolute">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#" style='margin-top:-1px'><font color='black'>Logged In As <?php echo $_SESSION['FNAME'];?> </font></a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
									<i class="material-icons">dashboard</i>
									<p class="hidden-lg hidden-md">Dashboard</p>
								</a>
								<ul class="dropdown-menu">
									<li><a href="dashboard.php">Dashboard</a></li>
									
									
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="material-icons">notifications</i>
									<span class="notification"  <?php if($ASSIGNED==0){echo $HIDE;}?>><?php echo $ASSIGNED;?></span>
									<p class="hidden-lg hidden-md">Notifications</p>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo "search_incident.php?status=".base64_encode("1");?>">	<span class="notification" style='background-color:gold;'><?php echo $PENDING;?></span>Pending Issues</a></li>
									<li><a href="<?php echo "search_incident.php?status=".base64_encode("2");?>">	<span class="notification" ><?php echo $ASSIGNED;?></span>Assigned Issues</a></li>
									<li><a href="<?php echo "search_incident.php?status=".base64_encode("4");?>"><span class="notification" style='background-color:blue;'><?php echo $RESOLVED;?></span>Resolved Issues</a></li>
									<li><a href="<?php echo "search_incident.php?status=".base64_encode("5");?>"><span class="notification" style='background-color:green;'><?php echo $CLOSED;?></span>Closed Issues</a></li>
									
								</ul>
							</li>
							<li>
								<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
	 							   <i class="material-icons">person</i>
	 							   <p class="hidden-lg hidden-md">Profile</p>
		 						</a>
									<ul class="dropdown-menu">
									<li><a  href="#" data-toggle='modal' data-target='#change_password'>Change Password</a></li>
									<li><a  href="#" data-toggle='modal' data-target='#update_profile'>Update Profile</a></li>
									<li><a href="logout.php">Sign Off</a></li>
									
								</ul>
							</li>
						</ul>

						
					</div>
				</div>
			</nav>
			
			
			
			