<?php

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	
	if(isset($_SESSION['USER_ID']))
	{
	header("location:views/dashboard.php");	
	}
?>

<!DOCTYPE html>
<html>

<!-- Head -->
<head>

	<title>UNION SYSTEMS | SUPPORT</title>

	<!-- Meta-Tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="keywords" content="UNION SYSTEMS SUPPORT" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //Meta-Tags -->

	<!-- Custom-Styleheet-Links -->
		<link rel="stylesheet" href="css/style.css" 		 type="text/css" media="all">
		<link rel="stylesheet" href="css/animate-custom.css" type="text/css" media="all">
	<!-- //Custom-Styleheet-Links -->

	<!-- Fonts -->
		<link rel="stylesheet" href="css/font1.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/font2.css" 		  type="text/css" media="all">
	<!-- //Fonts -->


</head>
<!-- //Head -->



<!-- Body -->
<body>


<br/>
	<div class="container SUPPORT agileits">

		<div class="content-left SUPPORT agileits">
			<img src="images/background.jpg" alt="SUPPORT Agileits"><br/><br/>
			<p style='color: #13114a;'>Welcome To Union Systems Global- Online Support</p>
			<a class="more SUPPORT agileits" href="#">LEARN MORE</a>
			
		</div>

		<div class="content-right SUPPORT agileits">
			<section>
				<div id="container_demo">
					<a class="hiddenanchor SUPPORT agileits" id="tologin"></a>
					<a class="hiddenanchor SUPPORT agileits" id="toregister"></a>
					<div id="wrapper">
						<div id="login" class="animate SUPPORT agileits form">
							<h2 class="SUPPORT agileits">Sign In</h2>
							<form  action="#" autocomplete="on" method="post">
								<label>E-mail</label>
								<input type="text" Name="Userame" required="">
								<label>Password</label>
								<input type="password" Name="Password" required="">
								<div class="send-button SUPPORT agileits">
									<p><a href="#">Forgot Password?</a></p>
									<form>
										<input type="submit" value="SIGN IN">
									</form>
									<div class="clear"></div>
								</div>
								<p class="change_link SUPPORT agileits" >
									Please Login With a Valid <a href="#toregister" class="to_register" >Email/ Password!</a>
								</p>
								<div class="clear"></div>
							</form>
							<div class="social-icons SUPPORT agileits">
								<p>Follow US On</p>
								<ul>
									<li class="fb w3ls SUPPORT agileits"><a href="#"><span class="icons SUPPORT agileits"></span><span class="text SUPPORT agileits">Facebook</span></a></li>
									<li class="twt w3ls SUPPORT agileits"><a href="#"><span class="icons SUPPORT agileits"></span><span class="text SUPPORT agileits">Twitter</span></a></li>
									<div class="clear"></div>
								</ul>
							</div>
							<div class="clear"></div>
						</div>
						<div id="register" class="animate SUPPORT agileits form">
						<h6><div id='mess' ></div><div class="loader" style="float:right;display:none;"></h6>
							<h2>Login Here </h2>
							
							<br/>
									<!---style='background:#faffbd;'--->
									<label>E-mail</label>
									<input type="text" Name="Email" id='email' required="">
									<label>Password</label>
									<input type="password" Name="Password"id='password' required="">
									<div class="send-button SUPPORT agileits">
										
											</div><button   class="send-button1" id='login_'>LOGIN</button>
										
									</div>
							
								
								<div class="clear"></div>
							
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</section>
		</div>
		<div class="clear"></div>

	</div>

	<div class="footer SUPPORT agileits">
		<p> &copy; <?php echo date('Y');?> Union Systems Support. All Rights Reserved <a href="http://unionsg.com" target="_blank">Union  Systems</a></p>
	</div>
<script src='js/jquery.min.js'></script>
<script src='js/login_process.js'></script>

</body>
<!-- //Body -->

</script>
</html>