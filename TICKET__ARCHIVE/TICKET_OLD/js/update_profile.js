$("#reset").click(function(){
   update_pass();
});

$("#update").click(function(){
   update_profile();
});


function update_pass()
{
	var oldpass=$("#oldpass").val();
	var newpass=$("#newpass").val();
	if(oldpass.length<2&&newpass.length<2)
	{
		echo("Missing Some Inputs..","danger");
		return;
	}
	$(".loader").show();
	var confirmnewpass=$("#confirmnewpass").val();
	if(newpass!=confirmnewpass)
	{
		echo("Confirmed Password Does Not Match..","danger");
		$(".loader").hide();
		return;
	}
	
	
	$.post( "../views/update_profile.php", 
	{ oldpass: oldpass,newpass: newpass })
	  .done(function( data ) {
	 
	
				//console.log(data);
				
				if(data==1)
				{
					//FAILED
					echo("Old Password is Not Valid..","danger");
					$(".loader").hide();	
					return;
					
					
				}
				
				else if(data==0)
				{
					//SUCESSFULL
					//echo("Working On Ticket Id: "+ticket_id,"success");
					document.getElementById("password_container").innerHTML='<center><h4 class="title">Your Password Has Been Changed</h4></br><img src="../images/check.png" style="width:50px;"></center>';
					
					$(".loader").hide();	
					$("#reset").hide();	
					$("#pclose").hide();	
					echo("Password Update Successful..","success");
				
				
						
				}
				else
				{
					//FAILED
					echo("Password Update Failed..","danger");
					$(".loader").hide();	
					return;
					
						
				}
							
							});
	
}

function update_profile()
{
	$(".loader").show();
}