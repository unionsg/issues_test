$("#add").click(function(){
   validate();
});

function validate()
{

var fname=$("#fname").val();	
var lname=$("#lname").val();	
var fullname=$("#fullname").val();	
var phone=$("#phone").val();	
var email=$("#email").val();	
var company=$("#company").val();	
var city=$("#city").val();	
var country=$("#country").val();	
var is_client=$("#is_client").val();	
var desc=$("#desc").val();	
var department=$("#department").val();	
var is_cc=$("#is_cc").val();	
$(".loader").show("slow");
if(fname.length>2&&lname.length>2&&fullname.length>2&&phone.length>2&&email.length>2&&company.length>0
&&city.length>2&&country.length>2&&is_client.length>0&&desc.length>2&&department.length>0&&is_cc.length>0)
{	echo("Adding, Please wait..","danger");	
	document.getElementById("add").disabled = true;
	$.post( "../js/add_user.php", 
	{ fname: fname,lname: lname,fullname: fullname,phone: phone,email: email,company: company,
	city: city,country: country,is_client: is_client,desc: desc,department: department,is_cc: is_cc })
	  .done(function( data ) {
	 
	
				console.log(data);
				if(data==1)
				{
					document.getElementById("form_content").innerHTML='<center> <h4 class="title">User Created Sucessfullly</h4><br/><img src="../images/check.png" style="width:50px;"></center>';
					if(is_cc==0){echo("An Email Notification has been sent to "+email,"success");}
					else{echo("User Created Sucessfullly","success");}
						
				}
				else if(data==2)
				{
					echo("There Exist A User With Same Email In The System","danger");	
					document.getElementById("add").disabled = false;					
				}
				else
				{
				document.getElementById("form_content").innerHTML='<center><h4 class="title">User Creation Failed</h4></br><img src="../images/failed.png" style="width:50px;"></center>';
					echo("User Creation Failed","danger");		
				}
							
							});
}
else
{
echo("Please Fill Out All Fields..","danger");	
}
setTimeout(function(){$(".loader").hide("slow");}, 1000);
return;
}