
			<!------UPDATE PROFILR  --->
								
								<div id='update_profile' class='modal fade' role='dialog'  >
								  <div class='modal-dialog  modal-sm' >

									<!-- Modal content-->
									<div class='modal-content'>
									  <div class='modal-header'>
									  <h3>Profile Update <img src='../images/profile.png' style='width:60px;float:right;'> </h3>
									
										
										
									  </div>
									  <div class='modal-body' >
		
										<center><div id="profile_container"> 
										
										
										
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Full Name</label>
													<input type="text"  id='fullname' class="form-control" value="<?php echo $_SESSION['FULL_NAME'];?>" >
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Email</label>
													<input type="text"  id='email'  class="form-control" value="<?php echo $_SESSION['EMAIL'];?>">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Phone</label>
													<input type="tel"  id='phone'  class="form-control" value="<?php echo $_SESSION['PHONE'];?>">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										</div></center>
									  </div>
									 <div class="modal-footer">
									<button type="button" class="btn btn-default" id="update">Update &nbsp;<div class='loader' style='float:right;display:none;'></div> </button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								  </div>
									</div>

								  </div>
								</div>
								
								
								
								
								
								
								
								<!------CHANGE PASSWORD  --->
								
								<div id='change_password' class='modal fade' role='dialog'   >
								  <div class='modal-dialog  modal-sm' >

									<!-- Modal content-->
									<div class='modal-content'>
									  <div class='modal-header'>
									  <h3>Reset Password  <img src='../images/password1.png' style='width:60px;float:right;'> </h3>
									
										
										
									  </div>
									  <div class='modal-body' >
		
										<center><div id="password_container"> 
										
										
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Old Password</label>
													<input type="password" id='oldpass'  class="form-control">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">New Password</label>
													<input type="password"  id='newpass' class="form-control" >
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										<div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Confirm Password</label>
													<input type="password"  id='confirmnewpass'  class="form-control">
												</div>
	                                        </div>
	                                       
	                                       
	                                    </div>
										
										</div></center>
									  </div>
									 <div class="modal-footer" >
									<button type="button" class="btn btn-default" id="reset">Reset &nbsp;<div class='loader' style='float:right;display:none;'></div></button><button type="button" class="btn btn-default" data-dismiss="modal" id='pclose'>Close</button>
								  </div>
									</div>

								  </div>
								</div>