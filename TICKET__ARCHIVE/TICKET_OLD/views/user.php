<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}

	$sql= "SELECT MAX(Login_Id) AS MAX_ID FROM users";
			//echo $sql;die;

			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$LAST_ID=$res[0]['MAX_ID']+1;
			$sql= "SELECT Company_Id, Company_Name FROM client ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Company_Id=$res[$y]['Company_Id'];
			$Company_Name=$res[$y]['Company_Name'];
			$option_data .="<option value='$Company_Id'>$Company_Name</option>";
			
			}
			
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

	   <?php include 'nav.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-8">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Create New User Profile </h4>
									<p class="category">Login ID : <font color ='black'> <b>USG<?php echo $LAST_ID;?></b></font>.  Complete details below</p>
	                            </div>
	                            <div class="card-content" id='form_content'>
	                           
	                                    <div class="row">
	                                        <div class="col-md-5">
												<div class="form-group label-floating">
													<label class="control-label">First Name</label>
													<input type="text" class="form-control" id='fname'>
												</div>
	                                        </div>
	                                        <div class="col-md-3">
												<div class="form-group label-floating">
													<label class="control-label">Last Name</label>
													<input type="text" class="form-control" id='lname'>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Full Name</label>
													<input type="text" class="form-control" id='fullname' >
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Phone</label>
													<input type="tel" class="form-control" id='phone' >
												</div>
	                                        </div>
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Email</label>
													<input type="email" class="form-control" id='email'>
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Company</label>
													<select  class="form-control"  id='company'>
													<option></option>
													<?php echo $option_data;?>
													</select>
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">City</label>
													<input type="text" class="form-control" id='city'>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Country</label>
													<input type="text" class="form-control" id='country'>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Is Client</label>
													<select  class="form-control" id='is_client'>
													<option value=''></option>
													<option value='1'> YES</option>
														<option value='0'> NO</option>
													</select>
												</div>
	                                        </div>
	                                    </div>
										
										 

	                                    <div class="row">
	                                        <div class="col-md-12">
	                                            <div class="form-group">
	                                               
													<div class="form-group label-floating">
									    				<label class="control-label"> Please Describe User Role In Company.</label>
								    					<textarea class="form-control" rows="5" id='desc'></textarea>
		                        					</div>
	                                            </div>
	                                        </div>
	                                    </div>

										<div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Department</label>
													<input type="text" class="form-control" id='department'>
												</div>
	                                        </div>
											<div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">User Type</label>
													<select  class="form-control" id='is_cc'>
													<option value=''></option>
													<option value='0'> ISSUE LOGGER</option>
														<option value='1'>GENERAL EMAIL LIST</option>
													</select>
												</div>
	                                        </div>
	                                       
	                                    </div>
	                                    <button type="button" class="btn btn-primary pull-right" id='add'>Add User &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>
	                                    <div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						<div class="col-md-4">
    						<div class="card card-profile">
    							<div class="card-avatar">
    								<a href="#">
    									<img class="img" src="../assets/img/faces/marc.jpg" />
    								</a>
    							</div>

    							<div class="content">
    								<h6 class="category text-gray">User Profile</h6>
    							
    								<p class="card-content">
    									Please fill in all users detials before submiting. Once submitted, email containing username and password will be sent to the user...
    								</p>
    								<a href="#" class="btn btn-primary btn-round" onclick='reload_("Creating New Environment Please wait","danger");'>Create New User</a>
    							</div>
    						</div>
		    			</div>
	                </div>
	            </div>
	        </div>

	       	<?php include 'footer.php'?>
		</div>
	</div>

</body>

	<?php include 'footer_script.php'?>
		<script src="../js/add_user.js" type="text/javascript"></script>
	
	<script>
//echo("Please Register A New User Here","success");
</script>
</html>
