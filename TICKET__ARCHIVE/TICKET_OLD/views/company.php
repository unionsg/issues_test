<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

	   <?php include 'nav.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-8">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Create New Client Profile</h4>
									<p class="category">Complete details below</p>
	                            </div>
	                            <div class="card-content" id='form_content'>
	                               
	                                    <div class="row">
	                                        <div class="col-md-5">
												<div class="form-group label-floating">
													<label class="control-label">Company Name</label>
													<input type="text" class="form-control" id='company_name' >
												</div>
	                                        </div>
	                                        <div class="col-md-3">
												<div class="form-group label-floating">
													<label class="control-label">Organization</label>
													<input type="text" class="form-control"id='organization' >
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Department</label>
													<input type="text" class="form-control" id='department' >
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Region</label>
													<input type="text" class="form-control"id='region' >
												</div>
	                                        </div>
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Site</label>
													<input type="text" class="form-control"  id='site'>
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Company Phone No.</label>
													<input type="tel" class="form-control"  id='phone'>
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Email</label>
													<input type="email" class="form-control" id='email' >
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Country</label>
													<input type="text" class="form-control" id='country'>
												</div>
	                                        </div>
											 <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Postal Code</label>
													<input type="text" class="form-control" id='post_code'>
												</div>
	                                        </div>
	                                       
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-12">
	                                            <div class="form-group">
	                                               
													<div class="form-group label-floating">
									    				<label class="control-label"> Company Description.</label>
								    					<textarea class="form-control" rows="5" id='description'></textarea>
		                        					</div>
	                                            </div>
	                                        </div>
	                                    </div>

	                                    <button type="button" class="btn btn-primary pull-right" id='add'>Add Company &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>
	                                    <div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						<div class="col-md-4">
    						<div class="card card-profile">
    							<div class="card-avatar">
    								<a href="#">
    									<img class="img" src="../assets/img/faces/company.jpg" />
    								</a>
    							</div>

    							<div class="content">
    								<h6 class="category text-gray">Company Profile</h6>
    							
    								<p class="card-content">
    									Please fill in all company detials before submiting. Once submitted, users  about to be created will be able to find their company when registering...
    								</p>
    								<a href="#" class="btn btn-primary btn-round" onclick='reload_("Creating New Environment Please wait","danger");'>Create New Client</a>
    							</div>
    						</div>
		    			</div>
	                </div>
	            </div>
	        </div>

	       	<?php include 'footer.php'?>
		</div>
	</div>

</body>

	<?php include 'footer_script.php'?>
	<script src="../js/add_company.js" type="text/javascript"></script>
	<script>
//echo("Please Register A New Company Here","success");
</script>
</html>
