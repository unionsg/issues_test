<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	$hide_company_name="";
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}		$comp_id=$_SESSION["IS_CLIENT"];
		$USER_ID=$_SESSION["USER_ID"];
		$AND ="";
		$stat="";
		if(isset($_GET['status']))
		{
			$stat=base64_decode($_GET['status']);
			
			
		}
		$Status1="";
		switch($stat)
				{
				case 1:
				{
				$Status1="Unassigned";
					continue;
				}	
				case 2:
				{
					$Status1="Assigned";
					continue;
				}	
				case 3:
				{
					$Status1="In Progress";
					continue;
				}	
				case 4:
				{
					$Status1="Resolved";
					continue;
				}
				case 5:
				{
					$Status1="Closed";
					continue;
				}					
				
				}
		
				if(($comp_id!=0)&&isset($_GET['status']))
				{
					$AND="WHERE incident.Company_Id='$comp_id' AND incident.Logged_By_Id='$USER_ID'AND Status BETWEEN 2 AND 4";
				}
				else if(($comp_id==0)&&isset($_GET['status']))
				{
					$AND=" WHERE Status='$stat'";
				}
				else if(($comp_id!=0)&&!isset($_GET['status']))
				{
					$AND="WHERE incident.Company_Id='$comp_id' AND incident.Logged_By_Id='$USER_ID'";
				}
				else if(($comp_id==0)&&!isset($_GET['status']))
				{
						$AND=" ";
				}
					$sql= "SELECT Summary, Date_Logged AS Date_Logged,Ticket_Id,Summary,Status,Affected_Product,users.Full_Name POSTED_BY_NAME,client.Company_Name FROM incident
					INNER JOIN client ON client.Company_Id=incident.Company_Id 
					INNER JOIN users ON users.Login_Id=incident.Logged_By_Id $AND
								ORDER BY Date_Logged DESC ";// IF STATUS ==1 MEAN 'NEW'
					//echo $sql;die;
				
				$GRID_OPTION='';
				$stmt = $conn->prepare($sql);
				$stmt->execute();
				$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
				for ($y = 0; $y < count($res); $y++) 
				{
					$count=$y+1;
					$Date_Logged=$res[$y]['Date_Logged'];
					$Ticket_Id=$res[$y]['Ticket_Id'];
					$Summary=strtoupper($res[$y]['Summary']);
					$Affected_Product=$res[$y]['Affected_Product'];
					$POSTED_BY_NAME=$res[$y]['POSTED_BY_NAME'];
					$Company_Name=$res[$y]['Company_Name'];
					
					$hide_company_name="";
					//GETTING COMMENT
				
					
					$GRID_OPTION.="
								<option value=\"$Ticket_Id\">$Summary   Ticket ID: $Ticket_Id</option>
												";
				}
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
	   	<link href='../assets/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
	   
		
</head>
<body>
	<?php include 'dialog.php';?>
	<div class="wrapper">
	    <?php include 'nav.php'; ?>

	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Comment On Incident</h4>
	                                <p class="category">Please Comment On an incident Here <?php if($comp_id!=0){?><select style="float:right;color:black;" onchange="get_data(this.value);"><option value='0'>Personal</option><option value='1'>All</option></select><?php }?></p>
	                            </div>
	                            <div class="card-content table-responsive" id='result'>
												<div class="form-group label-floating">
													<label class="control-label">Select Incident</label>
													 <select class='form-control' id='option_data'>
													<option value='-'>---Not Selected----</option>
													<?php echo $GRID_OPTION;?>
	                                     
													</select>
												</div>
												<div class="form-group label-floating">
									    				<label class="control-label">Comment.</label>
								    					<textarea  rows="5" id='chat_message' style='width:100%'></textarea>
		                        					</div>
	                                   
								<button type="button" class="btn btn-primary pull-right" id='add_comment' >Submit &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>
	                                    <div class="clearfix"></div>
	                            </div>
								
								
	                                        
	                                            
													
	                                            
	                                        
	                                   
	                        </div>
	                    </div>

	                </div>
											
	            </div>
	        </div>

	         	<?php include 'footer.php'?>
				  
		</div>
	</div>
	

</body>

				<?php include 'footer_script.php'?>
			<script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
				<!--<script src="../js/add_company.js" type="text/javascript"></script>----->
				<script>
				$(document).ready(function(){
				$('#incident_table').DataTable();
					});

			</script>
			<script>
			$( "#add_comment" ).click(function() {
			 var id=$("#option_data").val();
			 if(id.length<3)
			 {
				 echo ("Please Select An Incident","danger");
				 return;
			 }
			  var message=$("#chat_message").val();
			  if(message.length<2)
			 {
				 echo ("Please Input Message","danger");
				 return;
			 }
			 
			 $("#add_comment").attr('disabled','disabled');
			 send_message(id);
			 document.getElementById("result").innerHTML='<center> <h4 class="title">Comment Submitted Sucessfullly.  </h4><br/><img src="../images/check.png" style="width:50px;"></center>';
					
			 return;
			 
			});
			function get_data(section)
			{
				$.post( "../js/get__option_data.php", 
				{ section: section })
				  .done(function( data )
				  {
					  console.log(data);
					  document.getElementById("option_data").innerHTML=data;
					  });
			}
			</script>
			<script src="../js/get_incident.js" type="text/javascript"></script>
</html>
