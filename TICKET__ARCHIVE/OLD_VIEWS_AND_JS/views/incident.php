<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}
$IS_ADMIN=$_SESSION['IS_ADMIN'];
if($IS_ADMIN==1)
{
header("Location: dashboard.php");	
}

			$USER_EMAIL=$_SESSION['USER_EMAIL'];
			$TICKET_ID=time();
			$sql= "SELECT * FROM impact ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$impact="";
			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Type=$res[$y]['Type'];
			$Description=$res[$y]['Description'];
			$impact .="<option value='$Type'>$Description</option>";
			
			}
			$sql= "SELECT * FROM incident_type ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$incident="";
			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Type=$res[$y]['Type'];
			$Description=$res[$y]['Description'];
			$incident .="<option value='$Type'>$Description</option>";
			
			}
			$sql= "SELECT * FROM urgency ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$urgency="";
			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Type=$res[$y]['Type'];
			$Description=$res[$y]['Description'];
			$urgency .="<option value='$Type'>$Description</option>";
			
			}
			
			
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
	   
</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

	   <?php include 'nav.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-8">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Create New Incident </h4>
									<p class="category">Your Ticket ID : <font color ='black' ><b><?php echo $TICKET_ID;?></b></font>&nbsp;</p>
	                            </div>
	                            <div class="card-content" id='form_content'>
	                           
	                                    <div class="row">
	                                        <div class="col-md-5">
												<div class="form-group label-floating">
													<label class="control-label">Incident ID</label>
													<input type="text" class="form-control" id='incident_id' value=" <?php echo $TICKET_ID?>" readonly style='color:red;background:#f7f1f1;border-radius:15px;width:100px;'>
												</div>
	                                        </div>
	                                        <div class="col-md-3">
												<div class="form-group label-floating">
													
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													
												</div>
	                                        </div>
	                                    </div>
										
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Incident Summary</label>
														<input type="text" class="form-control" id='summary'>
												</div>
	                                        </div>
	                                    </div>
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Incident Details</label>
														<textarea class="form-control" rows="5" id='details'></textarea>
												</div>
	                                        </div>
	                                    </div>

	                                    <div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Area Affected</label>
													<select class="form-control" id='affected_area'>
													<option value=''></option>
													<option value='Software'>Software</option>
													<option value='Hardware'>Hardware</option>
													</select>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Affected Products</label>
													<select class="form-control" id='affected_product'>
													<option value=''></option>
													<option value='x100'>x100</option>
													<option value='Reports'>Reports</option>
													</select>
												</div>
	                                        </div>
											<div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Incident Type</label>
													<select class="form-control" id='incident'>
													<option value=""></option>
													<?php echo $incident;?>
													</select>
												</div>
	                                        </div>
	                                    </div>

	                                   

	                                    <div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Impact</label>
													<select class="form-control" id='impact'>
													<option value=''></option>
													<?php echo $impact;?>
													</select>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Urgency</label>
													<select class="form-control" id='urgency'>
													<option value=''></option>
													<?php echo $urgency;?>
													</select>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Priority</label>
													<select class="form-control" id='priority'>
													<option value=''></option>
													<option value='1'>High</option>
													<option value='2'>Medium</option>
													<option value='3'>Low</option>
													</select>
												</div>
	                                        </div>
	                                    </div>
										
										<div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Department</label>
														<input type="text" class="form-control" id='department'>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Person Affected</label>
														<input type="text" class="form-control" id='person_affected'>
												</div>
	                                        </div>
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Affected Person's Contact</label>
														<input type="tel" class="form-control" id='affected_contact'>
												</div>
	                                        </div>
	                                    </div>
										
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Affected Person's Email</label>
														<input type="email" class="form-control" id='affected_person_email'>
												</div>
	                                        </div>
	                                    </div>
										 <div class="row">
	                                        <div class="col-md-12">
												<div class="form-group label-floating">
													<input id="uploadFile" placeholder="Choose Image Files" class="form-control" disabled="disabled" />
													<div class="fileUpload btn btn-success">
														<span>Add Images</span>
														<input type="file" class="upload" id='file' onchange="getpath(this.value);checkfile(this);" accept="image/*"  multiple/>
													
													</div>
													<div id='attachment' style='background:#ccc;width:200px;height:200px;display:none'  ></div>
												</div>
	                                        </div>
	                                    </div>
										 

	                                    
	                                    <button type="button" class="btn btn-primary pull-right" id='add'>Submit &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>
	                                    <div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						<div class="col-md-4">
    						<div class="card card-profile">
    							<div class="card-avatar">
    								<a href="#">
    									<img class="img" src="../assets/img/faces/ticket.png" />
    								</a>
    							</div>

    							<div class="content" >
    								<h6 class="category text-gray">New Ticket</h6>
    							
    								<p class="card-content">
    									Please fill in all detials before submiting. Once submitted, We will send a comfirmation email to acknowlege our receipt ...
    								</p><img src="../assets/img/faces/support.png" style='width:150px;'>
    								<a href="#" class="btn btn-primary btn-round" onclick='reload_("Creating New Environment Please wait","danger");'>Create New Ticket</a>
    							</div>
    						</div>
		    			</div>
						<div class="col-md-4">
    						<div class="card card-profile" id="preview">
    							

    							<div class="content">
    								<h6 class="category text-gray">Attachment Preview<div class='loader' id='loader' style='display:none;margin-right:50px;float:right;'></div></h6>
									<div style="overflow-y: scroll;height:350px" >
    									<output id="list" style='width:300px;'></output>
										<div>
    								</div>
    						</div>
		    			</div>
	                </div>
	            </div>
	        </div>

	       	<?php include 'footer.php'?>
		</div>
		
	</div>

</body>

	<?php include 'footer_script.php'?>
		<script src="../js/add_incident.js" type="text/javascript"></script>
		
		<script>
	
	function getpath(path) {
    document.getElementById("uploadFile").value = path;
	};
	
function checkfile(sender) {

    var validExts = new Array(".jpeg", ".jpg",".png");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
		echo("Invalid file selected, Please Select Valid Image file of extension "+validExts.toString() + " types.","danger");
     
               $('input[type=file]').val('');
			       document.getElementById("uploadFile").value ="";
      return false;
    }
    else return true;
}
//echo("Please Register A New User Here","success");
</script>
	
</html>
