<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	$hide_company_name="";
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}		$comp_id=$_SESSION["IS_CLIENT"];
		$USER_ID=$_SESSION["USER_ID"];
		$AND ="";
		$stat="";
		if($comp_id!=0)
		{
			header("Location: dashboard.php");
		}
		
		if(isset($_GET['status']))
		{
			$stat=base64_decode($_GET['status']);
			
			
		}
		
				
						$AND="WHERE incident.Company_Id!='0' AND Status BETWEEN 2 AND 4";
				
				
					$sql= "SELECT Summary, Date_Logged AS Date_Logged,Ticket_Id,Summary,Status,Affected_Product,users.Full_Name POSTED_BY_NAME,client.Company_Name FROM incident
					INNER JOIN client ON client.Company_Id=incident.Company_Id 
					INNER JOIN users ON users.Login_Id=incident.Logged_By_Id $AND
								ORDER BY Date_Logged DESC ";
					//echo $sql;die;
			
			$GRID_TABLE='';
			$count=0;
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
				$Date_Logged=$res[$y]['Date_Logged'];
				$Ticket_Id=$res[$y]['Ticket_Id'];
				$Summary=strtoupper($res[$y]['Summary']);
				$Affected_Product=$res[$y]['Affected_Product'];
				$POSTED_BY_NAME=$res[$y]['POSTED_BY_NAME'];
				$Company_Name=$res[$y]['Company_Name'];
				$Status_=$res[$y]['Status'];
				$Status="";
				switch($Status_)
				{
				case 1:
				{
				$Status="UNASSIGNED";
					continue;
				}	
				case 2:
				{
					$Status="ASSIGNED";
					continue;
				}	
				case 3:
				{
					$Status="IN PROGRESS";
					continue;
				}	
				case 4:
				{
					$Status="RESOLVED";
					continue;
				}
				case 5:
				{
					$Status="CLOSED";
					continue;
				}					
				
				}
				$hide_company_name="";
				//GETTING COMMENT
			$sql_comment= "SELECT COUNT(Comment) AS TOTAL FROM comments WHERE Ticket_Id='$Ticket_Id'";
				//echo $sql;die;
			
			
			$stmt_comment = $conn->prepare($sql_comment);
			$stmt_comment->execute();
			$res_comment = $stmt_comment->fetchAll(PDO::FETCH_ASSOC);
			$COMMENT=$res_comment[0]['TOTAL'];
			$hide_company_name="";
				if($comp_id!=0)
				{
					$hide_company_name="style='display:none;'";
				}
				
				$GRID_TABLE.="
												<tr>
	                                        	<td >$count</td>
												<td style='width:150px;'>$Date_Logged</td>
	                                        	<td>$Ticket_Id</td>
												
	                                        	<td style='width:200px;'>$Summary</td>
	                                        	<td $hide_company_name>$Company_Name</td>
												<td style='width:150px;'>$Status</td>
	                                        	<td class='td-actions text-right'>
															<button type='button' rel='tooltip' title='Change Status' class='btn btn-primary btn-simple btn-xs' data-toggle='modal' data-target='#status_dialog' onclick=\"get_assign_details('$Ticket_Id');\">
																<i class='fa fa-edit' style='color:red;'></i><br>CHANGE STATUS
															</button>
															
														</td>
	                                        </tr>
											
											";
			
			}
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
	   	<link href='../assets/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
	   
		
</head>
<body>
	<?php include 'dialog.php';?>
	<div class="wrapper">
	    <?php include 'nav.php'; ?>

	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Assign Incident</h4>
	                                <p class="category">You Have <font color='black'><b><?php echo $count;?> Issues </b></font> Assigned</p>
	                            </div>
	                            <div class="card-content table-responsive">
	                                <table class="table" id='incident_table' style='width:100%;'>
	                                    <thead class="text-primary" >
	                                    	<th>Id</th>
	                                    	<th>Date</th>
	                                    	<th>Ticket Id</th>
											<th>Summary</th>
											<th <?php echo $hide_company_name;?>>Client</th>
											<th>Status</th>
											<th>Assign</th>
	                                    </thead>
	                                    <tbody id='grid_data'>
	                                        
	                                        	<?php echo $GRID_TABLE;?>
	                                        
	                                        
	                                    </tbody>
	                                </table>
								
	                            </div>
	                        </div>
	                    </div>

	                    
	                </div>
	            </div>
	        </div>

	         	<?php include 'footer.php'?>
				  
		</div>
	</div>
	

</body>

	<?php include 'footer_script.php'?>
<script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	
	<!--<script src="../js/add_company.js" type="text/javascript"></script>----->
	<script>
	$(document).ready(function(){
    $('#incident_table').DataTable();
});

</script>
<script>
	function get_assign_details(assign_ticket)
	{
		
		
		document.getElementById("status_ticket").value=assign_ticket;
		
			  
	}
	$("#btn_status").click(function()
	{
		var status_ticket=$("#status_ticket").val();
		var status_code=$("#status_code").val();
		var status_comment=$("#status_comment").val();
		if(status_code=='-')
		{
			echo("Please Change Status","danger");
			return;
		}
		if(status_comment.length<1)
		{
			echo("Please Comment ","danger");
			return;
		}
		
		
	
		$(".loader").show("slow");
	
		$.post( "../js/status.php", 
		{ assign_ticket: status_ticket,status_code: status_code,status_comment: status_comment })
		  .done(function( data )
		  {
			 console.log(data);
			 if(data==1)
			 {
				 document.getElementById("status_container").innerHTML='<center> <h4 class="title">Status Changed For Ticket Id :<font color="red"><b>'+status_ticket+'</b></font></h4><br/><img src="../images/check.png" style="width:50px;"></center>';
				$("#btn_status").hide();
				$(".loader").hide();				
				
				echo("Status Changed Successfully","success"); 
				echo("Email Sent To Person Affected About Status Update","success"); 
				echo("Notification Sent To Support Person About Status Update","success"); 
				setTimeout(function(){ window.location.reload();}, 4000);
					
			 }
			 else
			 {
				 
				echo("Error Changing Status","danger"); 
				$(".loader").hide();
			 }
			
			
			
		   });
		});
</script>

</html>
