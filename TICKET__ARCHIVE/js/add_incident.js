$("#add").click(function(){
   validate();
});

function validate()
{
	
	var ticket_id=$("#incident_id").val();	
	var file=document.getElementById("attachment").innerHTML;

	
	var summary=$("#summary").val();
	var incident=$("#incident").val();	
	var details=$("#details").val();	
	var affected_area=$("#affected_area").val();	
	var affected_product=$("#affected_product").val();	
	var impact=$("#impact").val();	
	var urgency=$("#urgency").val();	
	var priority=$("#priority").val();	
	var department=$("#department").val();	
	var person_affected=$("#person_affected").val();	
	var affected_contact=$("#affected_contact").val();	
	var affected_person_email=$("#affected_person_email").val();	
	var user_email="<?php echo $USER_EMAIL;?>";

	
$(".loader").show("slow");
if(ticket_id.length>0&&summary.length>0&&details.length>1&&affected_area.length>0&&affected_product.length>0&&impact.length>0
&&urgency.length>0&&priority.length>0&&department.length>0&&person_affected.length>0&&affected_contact.length>0&&affected_person_email.length>0&& incident.length>0)
{
	echo("Sending Please wait...","danger");	
	document.getElementById("add").disabled = true;
	$.post( "../js/add_incident.php", 
	{ incident: incident,file: file,ticket_id: ticket_id,summary: summary,details: details,affected_area: affected_area,affected_product: affected_product,impact: impact,
	urgency: urgency,priority: priority,department: department,person_affected: person_affected,affected_contact: affected_contact,affected_person_email: affected_person_email })
	  .done(function( data ) {
	 
	
				console.log(data);
				if(data==1)
				{
					document.getElementById("form_content").innerHTML='<center> <h4 class="title">Incident Submitted Sucessfullly. Ticket Id :<b><font color="red">'+ticket_id+'</font></b> </h4><br/><img src="../images/check.png" style="width:50px;"></center>';
					$("#preview").hide();
					echo("An Email Notification has been sent to Your Account ","success");
					setTimeout(function(){ echo("An Email Notification has also been sent to affected person with email "+affected_person_email,"success");}, 1000);
					$(".loader").hide("slow");
				
						
				}
				else if(data==2)
				{
					echo("There Exist An Incident With The Same Ticket Id In The System","danger");	
					document.getElementById("add").disabled = false;
					$(".loader").hide("slow");					
				}
				else
				{
						$("#preview").hide();
				document.getElementById("form_content").innerHTML='<center><h4 class="title"> Failed Submitting Incident.</h4></br><img src="../images/failed.png" style="width:50px;"></center>';
					echo("Failed Submitting Incident. Please Try Again","danger");	
					$(".loader").hide("slow");
				}
							
							});
}
else
{
echo("Please Fill Out All Fields..","danger");	
$(".loader").hide("slow");
}

return;
}