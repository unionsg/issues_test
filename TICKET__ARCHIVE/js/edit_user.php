<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	$hide_company_name="";
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}		
		if ($_SERVER["REQUEST_METHOD"] == "POST") 
			{
				
			$sql= "SELECT Company_Id, Company_Name FROM client ";
			//echo $sql;die;
			$option_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			$Company_Id=$res[$y]['Company_Id'];
			$Company_Name=$res[$y]['Company_Name'];
			$option_data .="<option value='$Company_Id'>$Company_Name</option>";
			
			}
			$comp_id=$_SESSION["IS_CLIENT"];
			$USER_ID=$_SESSION["USER_ID"];
			$id=$_POST['id'];
			$FORM="";
			
			$sql= "SELECT * FROM users WHERE Login_Id='$id'";
			
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res= $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
				$fullname=$res[0]['Full_Name'];	
				$F_Name=$res[0]['F_Name'];	
				$L_Name=$res[0]['L_Name'];	
				$Phone=$res[0]['Phone'];	
				$Email=$res[0]['Email'];	
				$City=$res[0]['City'];	
				$Country=$res[0]['Country'];	
				$Description=$res[0]['Description'];	
				$Department=$res[0]['Department'];	
				$Is_Client=$res[0]['Is_Client'];
				$Is_Client_="";
				if($Is_Client==1)
				{
					$Is_Client_="YES";
				}
				else
				{
				$Is_Client_="NO";	
				}				
				
				
				$Is_Cc=$res[0]['Is_Cc'];
				$Is_Cc_="";
				if($Is_Cc==0)
				{
					$Is_Cc_="ISSUE LOGGER";
				}
				else if($Is_Cc==1)
				{
				$Is_Cc_="GENERAL EMAIL LIST";
				}
				else if($Is_Cc==2)
				{
				$Is_Cc_="SUPPORT PERSON";
				}		
				
				$comp_id=$res[0]['Company_Id'];	
				
				
				$sql= "SELECT Company_Name FROM client WHERE Company_Id='$comp_id'";
				$stmt1 = $conn->prepare($sql);
				$stmt1->execute();
				$res1= $stmt1->fetchAll(PDO::FETCH_ASSOC);
				$comp_name=$res1[0]['Company_Name'];	
				
				
				$FORM.=
				"
				 <div class='card-content' id='form_content'>
								 <div class='row'>
	                                        <div class='col-md-12'>
												<div class='form-group label-floating'>
													<label class='control-label'>Company</label>
													<select  class='form-control'  id='company'>
													<option value='$comp_id'>$comp_name</option>
													$option_data;
													</select>
												</div>
	                                        </div>
	                                    </div>
	                           
	                                    <div class='row'>
	                                        <div class='col-md-5'>
												<div class='form-group label-floating'>
													<label class='control-label'>First Name</label>
													<input type='text' class='form-control' id='fname' value='$F_Name'>
												</div>
	                                        </div>
	                                        <div class='col-md-3'>
												<div class='form-group label-floating'>
													<label class='control-label'>Last Name</label>
													<input type='text' class='form-control' id='lname' value='$L_Name'>
												</div>
	                                        </div>
	                                        <div class='col-md-4'>
												<div class='form-group label-floating'>
													<label class='control-label'>Full Name</label>
													<input type='text' class='form-control' id='fullname1' value='$fullname' >
												</div>
	                                        </div>
	                                    </div>

	                                    <div class='row'>
	                                        <div class='col-md-6'>
												<div class='form-group label-floating'>
													<label class='control-label'>Phone</label>
													<input type='tel' class='form-control' id='phone' value='$Phone'>
												</div>
	                                        </div>
	                                        <div class='col-md-6'>
												<div class='form-group label-floating'>
													<label class='control-label'>Email</label>
													<input type='email' class='form-control' id='email_' value='$Email'>
												</div>
	                                        </div>
	                                    </div>

	                                   

	                                    <div class='row'>
	                                        <div class='col-md-4'>
												<div class='form-group label-floating'>
													<label class='control-label'>City</label>
													<input type='text' class='form-control' id='city' value='$City'>
												</div>
	                                        </div>
	                                        <div class='col-md-4'>
												<div class='form-group label-floating'>
													<label class='control-label'>Country</label>
													<input type='text' class='form-control' id='country' value='$Country'>
												</div>
	                                        </div>
	                                        <div class='col-md-4'>
												<div class='form-group label-floating'>
													<label class='control-label'>Is Client</label>
													<select  class='form-control' id='is_client'>
													<option value='$Is_Client'>$Is_Client_</option>
													<option value='1'> YES</option>
														<option value='0'> NO</option>
													</select>
												</div>
	                                        </div>
	                                    </div>
										
										 

	                                    <div class='row'>
	                                        <div class='col-md-12'>
	                                            <div class='form-group'>
	                                               
													<div class='form-group label-floating'>
									    				<label class='control-label'> Please Describe User Role In Company.</label>
								    					<textarea class='form-control' rows='5' id='desc'>$Description</textarea>
		                        					</div>
	                                            </div>
	                                        </div>
	                                    </div>

										<div class='row'>
	                                        <div class='col-md-4'>
												<div class='form-group label-floating'>
													<label class='control-label'>Department</label>
													<input type='text' class='form-control' id='department' value='$Department'>
												</div>
	                                        </div>
											<div class='col-md-4'>
												<div class='form-group label-floating'>
													<label class='control-label'>User Type</label>
													<select  class='form-control' id='is_cc'>
													<option value='$Is_Cc'>$Is_Cc_</option>
													<option value='0'> ISSUE LOGGER</option>
														<option value='1'>GENERAL EMAIL LIST</option>
														<option value='2'>SUPPORT PERSON</option>
													</select>
												</div>
	                                        </div>
	                                       
	                                    </div>
	                                    <button type='button' class='btn btn-primary pull-right' id='edit'>Edit User &nbsp;&nbsp;<div class='loader' style='float:right;display:none;margin-top:-2.5px;'></div></button>
	                                    <div class='clearfix'></div>
	                               
	                            </div>
				
				
				";
				
			echo $FORM;
		
			}
		
?>