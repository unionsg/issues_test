<!--   Core JS Files   -->

	<script src="../assets/js/jquery-3.1.0.min.js" type="text/javascript"></script>
	<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="../assets/js/material.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="../assets/js/chartist.min.js"></script>

	<!--  Notifications Plugin    -->
	<script src="../assets/js/bootstrap-notify.js"></script>
	
	
	<!--  Google Maps Plugin    -->
	

	<!--  javascript methods -->
	<script src="../assets/js/material-dashboard.js"></script>

	<!--  methods, don't include it in your project! -->
	<script src="../assets/js/demo.js"></script>
	
	<!--  update user password and prfile here -->
	<script src="../js/update_profile.js"></script>

	<script type="text/javascript">
	
	var PAGE ="<?php echo $PAGE;?>";
	switch(PAGE)
	{
	case '/ticket/views/dashboard.php':
	{
		$('#page1').addClass("active");
		break;
	}	
	case '/ticket/views/user.php':
	{
		$('#page2').addClass("active");
		break;
	}
	case '/ticket/views/company.php':
	{
		$('#page3').addClass("active");
		break;
	}		
	case '/ticket/views/incident.php':
	{
		$('#page4').addClass("active");
		break;
	}	
	case '/ticket/views/search_incident.php':
	{
		$('#page5').addClass("active");
		break;
	}	
	case '/ticket/views/assign.php':
	{
		$('#page6').addClass("active");
		break;
	}	
	case '/ticket/views/status.php':
	{
		$('#page7').addClass("active");
		break;
	}	
	case '/ticket/views/close.php':
	{
		$('#page8').addClass("active");
		break;
	}	
	case '/ticket/views/edit_user.php':
	{
		$('#page9').addClass("active");
		break;
	}	
	case '/ticket/views/edit_company.php':
	{
		$('#page10').addClass("active");
		break;
	}	
	case '/ticket/views/comment.php':
	{
		$('#page11').addClass("active");
		break;
	}	
	
	default:
	{
		
	}
		
	}
	
	var welcome=<?php if(!isset($_SESSION['FIRST_WELCOME'])){echo 0;}else{echo 1;}?>;
	var name="<?php echo $_SESSION['FNAME'].' From  '.$_SESSION['COMPANY'];?>";
	
	if (welcome!=1)
	{
		
		$.notify({
	// options
	message: 'Welcome Back,  '+name+'  ' ,
	icon: "notifications"
	},{
		// settings
		type: 'success'
	});

		
		//demo.showNotification('bottom','right');
		var assigned=<?php echo $ASSIGNED;?>;
		var pending=<?php echo $PENDING;?>;
		var comp_id=<?php echo $comp_id;?>;
		if(comp_id==0)
		{
		 setTimeout(function(){  echo(pending+" issues are pending and is currently not assigned to an agent","success");}, 5000);
		}
		else
		{
			if(assigned==0)
		{
			setTimeout(function(){  echo("Your have "+pending+" pending issues ","success");}, 5000);
		}
		else
		{
			setTimeout(function(){  echo("Your have no "+pending+" pending issues and "+assigned+" Of Your Issues Have Been Assigned to An Agent ","success");}, 5000);
		}
		}
		
		
		
	}
	else
	{
		var after="<?php $_SESSION['FIRST_WELCOME']=1; echo 1; ?>";
	}
    	$(document).ready(function(){

			// Javascript method's body can be found in assets/js/demos.js
        	demo.initDashboardPageCharts();

    	});
		function echo (message,type_)
		{
				$.notify({
	// options
	message: message ,
	icon: "notifications"
	},{
		// settings
		type: type_
	});
			
		}
		function reload_(a,b)
		{
		echo(a,b);	
		setTimeout(function(){window.location.reload();}, 1000);
		}
	</script>
	<script>
  function handleFileSelect(evt) {
	  echo("Uploading Image Files...","info");
	  $("#loader").show("slow");
	 
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
	var base64_array="";
	var base64_array1="";
	var done=0;
    for (var i = 0, f; f = files[i]; i++) {
		done=1;

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
		  
		 
		 base64_array=e.target.result;
		 base64_array+="~";
		  
		 
		  
		  document.getElementById("attachment").innerHTML+=base64_array;
		
		
		
          span.innerHTML = ['<img class="thumb"  src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
		  
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
	   
    }
	
	 if(done==1)
	 {
		setTimeout(function(){  echo("Upload Complete...","success"); $("#loader").hide("slow");}, 5000); 
	 }
	 else
	 {
		setTimeout(function(){  echo("Upload Failed...","danger"); $("#loader").hide("slow");}, 2000);  
	 }
	 
	
	
  }

  document.getElementById('file').addEventListener('change', handleFileSelect, false);
</script>
