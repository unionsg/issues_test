<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}

	$sql= "SELECT Company_Id,Company_Name, Region, Phone, Email FROM client ORDER BY Company_Name ASC";
						//echo $sql;die;

			$table_data="";
			$stmt = $conn->prepare($sql);
			$stmt->execute();

			 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
			
				$email=$res[$y]['Email'];
				$region=$res[$y]['Region'];
				$phone=$res[$y]['Phone'];
				$Company_Name=$res[$y]['Company_Name'];
				$id=$res[$y]['Company_Id'];
				$table_data.=
				"
				  <tr>
	                <td><b>$count</b></td>
	                <td>$Company_Name</td>
	                <td>$email</td>
	                <td>$phone</td>
					 <td>$region</td>
					 <td><u><span style='font-size:10px;' > <button onclick='alert();fetch_user($id );'><font color='red'><b>Edit / Delete</b></font></button></span></u></td>
	                 </tr>
	                                       
				";
			}
			
			
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
	   <link href='../assets/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
</head>
<body>
<?php include 'dialog.php';?>
<div class="wrapper">

	   <?php include 'nav.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Edit Company Details </h4>
									  </div>
	                            <div class="card-content" >
	                           
	                                   
	                                    <div class="row">
	                                        <div class="col-md-12">
												 <div class="card-content table-responsive">
												<table class="table table-hover" id='form_content'>
													<thead class="text-primary">
														<th>ID</th>
														<th>Name</th>
														<th>Email</th>
														<th>Department</th>
														<th>Company</th>
														<th>Details</th>
													</thead>
													<tbody>
													   <?php echo $table_data;?>
													   
													</tbody>
												</table>
											</div>
												
												
	                                        </div>
	                                    </div>


										<div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						
	                </div>
	            </div>
	        </div>

	       	<?php include 'footer.php'?>
		</div>
	</div>

</body>

	<?php include 'footer_script.php'?>
	<script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	
	<!--<script src="../js/add_company.js" type="text/javascript"></script>----->
	<script>
	$(document).ready(function(){
    $('#form_content').DataTable();
});

</script>
		<script src="../js/add_user.js" type="text/javascript"></script>
	
	<script>
//echo("Please Register A New User Here","success");
</script>
</html>
