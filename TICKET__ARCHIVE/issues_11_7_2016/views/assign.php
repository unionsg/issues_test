<?php
require_once('../Connections/ticket.php');
	require_once('../includes/usedfunctions.php');

	if (!isset($_SESSION)) {
	  session_start();
	  ob_start();
	 
	}
	$hide_company_name="";
	//unset($_SESSION['FIRST_WELCOME']);die;
if(!isset($_SESSION['USER_ID']))
{
header("Location: ../login.php");	
}		$comp_id=$_SESSION["IS_CLIENT"];
		$USER_ID=$_SESSION["USER_ID"];
		$AND ="";
		$stat="";
		if($comp_id!=0)
		{
			header("Location: dashboard.php");
		}
		
		if(isset($_GET['status']))
		{
			$stat=base64_decode($_GET['status']);
			
			
		}
		
				
						$AND="WHERE incident.Company_Id!='0' AND Status='1'";
				
				
					$sql= "SELECT Summary, Date_Logged AS Date_Logged,Ticket_Id,Summary,Status,Affected_Product,users.Full_Name POSTED_BY_NAME,client.Company_Name FROM incident
					INNER JOIN client ON client.Company_Id=incident.Company_Id 
					INNER JOIN users ON users.Login_Id=incident.Logged_By_Id $AND
								ORDER BY Date_Logged DESC ";// IF STATUS ==1 MEAN 'NEW'
					//echo $sql;die;
			
			$GRID_TABLE='';
			$count=0;
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			for ($y = 0; $y < count($res); $y++) 
			{
				$count=$y+1;
				$Date_Logged=$res[$y]['Date_Logged'];
				$Ticket_Id=$res[$y]['Ticket_Id'];
				$Summary=strtoupper($res[$y]['Summary']);
				$Affected_Product=$res[$y]['Affected_Product'];
				$POSTED_BY_NAME=$res[$y]['POSTED_BY_NAME'];
				$Company_Name=$res[$y]['Company_Name'];
				$Status_=$res[$y]['Status'];
				$Status="";
				switch($Status_)
				{
				case 1:
				{
				$Status="UNASSIGNED";
					continue;
				}	
				
				
				}
				$hide_company_name="";
				//GETTING COMMENT
			$sql_comment= "SELECT COUNT(Comment) AS TOTAL FROM comments WHERE Ticket_Id='$Ticket_Id'";
				//echo $sql;die;
			
			
			$stmt_comment = $conn->prepare($sql_comment);
			$stmt_comment->execute();
			$res_comment = $stmt_comment->fetchAll(PDO::FETCH_ASSOC);
			$COMMENT=$res_comment[0]['TOTAL'];
			$hide_company_name="";
				if($comp_id!=0)
				{
					$hide_company_name="style='display:none;'";
				}
				
				$GRID_TABLE.="
												<tr>
	                                        	<td >$count</td>
												<td style='width:150px;'>$Date_Logged</td>
	                                        	<td>$Ticket_Id</td>
												
	                                        	<td style='width:200px;'>$Summary</td>
												
												<td style='width:150px;'>$POSTED_BY_NAME</td>
	                                        	<td $hide_company_name>$Company_Name</td>
	                                        	<td class='td-actions text-right'>
															<button type='button' rel='tooltip' title='Assign Issue' class='btn btn-primary btn-simple btn-xs' data-toggle='modal' data-target='#assign_dialog' onclick=\"get_assign_details('$Ticket_Id');\">
																<font color='red'><u>Assign</u></font>
															</button>
															<button type='button' rel='tooltip' title='View Issue' class='btn btn-primary btn-simple btn-xs' data-toggle='modal' data-target='#detail_incident' onclick=\"get_details('$Ticket_Id');\">
																&nbsp;&nbsp;&nbsp; <u><span style='font-size:10px;'> <font color='red'>View</font></span></u>
															</button>
														</td>
	                                        </tr>
											
											";
			
			}
?>
<!doctype html>
<html lang="en">
<head>
	   <?php include 'header_script.php'; ?>
	   	<link href='../assets/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
	   
		
</head>
<body>
	<?php include 'dialog.php';?>
	<div class="wrapper">
	    <?php include 'nav.php'; ?>

	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Assign Incident</h4>
	                                <p class="category">You Have <font color='black'><b><?php echo $count;?> Issues </b></font> Currently Not Assigned</p>
	                            </div>
	                            <div class="card-content table-responsive">
	                                <table class="table" id='incident_table' style='width:100%;'>
	                                    <thead class="text-primary" >
	                                    	<th>Id</th>
	                                    	<th>Date</th>
	                                    	<th>Ticket Id</th>
											<th>Summary</th>
											
											<th>Logged_By</th>
											<th <?php echo $hide_company_name;?>>Client</th>
											<th>Assign</th>
	                                    </thead>
	                                    <tbody id='grid_data'>
	                                        
	                                        	<?php echo $GRID_TABLE;?>
	                                        
	                                        
	                                    </tbody>
	                                </table>
								
	                            </div>
	                        </div>
	                    </div>

	                    
	                </div>
	            </div>
	        </div>

	         	<?php include 'footer.php'?>
				  
		</div>
	</div>
	

</body>

	<?php include 'footer_script.php'?>
<script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	
	<!--<script src="../js/add_company.js" type="text/javascript"></script>----->
	<script>
	$(document).ready(function(){
    $('#incident_table').DataTable();
});

</script>
<script>
function get_assign_details(assign_ticket)
{
	
	
	$.post( "../js/get_users.php", 
	{ assign_ticket: assign_ticket })
	  .done(function( data )
	  {
		 //console.log(data);
		
			 document.getElementById("assign_ticket").value=assign_ticket;
		  document.getElementById("assign_agent").innerHTML=data;
		  });
		  
}
	$("#btn_assign").click(function()
	{
		var assign_ticket=$("#assign_ticket").val();
		var assign_agent=$("#assign_agent").val();
		if(assign_agent.length<1)
		{
			echo("Please Assign An Agent","danger");
			return;
		}
	
		$(".loader").show("slow");
	
		$.post( "../js/assign.php", 
		{ assign_ticket: assign_ticket,assign_agent: assign_agent })
		  .done(function( data )
		  {
			 console.log(data);
			 if(data==1)
			 {
				 document.getElementById("assign_container").innerHTML='<center> <h4 class="title">Ticket Id :<font color="red"><b>'+assign_ticket+'</b></font> Has Been Assigned Sucessfullly.  </h4><br/><img src="../images/check.png" style="width:50px;"></center>';
				$("#btn_assign").hide();
				$(".loader").hide();				
				
				echo("Incident Assigned Successfully","success"); 
				echo("Email Sent To Person Affected About Status Update","success"); 
				echo("Notification Sent To Support Person","success"); 
				setTimeout(function(){ window.location.reload();}, 4000);
					
			 }
			 else
			 {
				 
				echo("Error Assigning An Agent","danger"); 
				$(".loader").hide();
			 }
			
			
			
		   });
		});
</script>
<script src="../js/get_incident.js" type="text/javascript"></script>
</html>
