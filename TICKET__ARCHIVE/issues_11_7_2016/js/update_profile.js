$("#reset").click(function(){
   update_pass();
});

$("#update").click(function(){
   update_profile();
});


function update_pass()
{
	var oldpass=$("#oldpass").val();
	var newpass=$("#newpass").val();
	if(oldpass.length<2||newpass.length<2)
	{
		echo("Missing Some Inputs..","danger");
		return;
	}
	$(".loader").show();
	var confirmnewpass=$("#confirmnewpass").val();
	if(newpass!=confirmnewpass)
	{
		echo("Confirmed Password Does Not Match..","danger");
		$(".loader").hide();
		return;
	}
	
	
	$.post( "../views/update_profile.php", 
	{ oldpass: oldpass,newpass: newpass })
	  .done(function( data ) {
	 
	
				//console.log(data);
				
				if(data==1)
				{
					//FAILED
					echo("Old Password is Not Valid..","danger");
					$(".loader").hide();	
					return;
					
					
				}
				
				else if(data==0)
				{
					//SUCESSFULL
					//echo("Working On Ticket Id: "+ticket_id,"success");
					document.getElementById("password_container").innerHTML='<center><h4 class="title">Your Password Has Been Changed</h4></br><img src="../images/check.png" style="width:50px;"></center>';
					
					$(".loader").hide();	
					$("#reset").hide();	
					$("#pclose").hide();	
					echo("Password Update Successful..","success");
				
				document.getElementById("password_container").innerHTML='<center><h4 class="title">System Will Logout In 3sec To Effect</h4></br><img src="../images/check.png" style="width:50px;"></center>';
					
				setTimeout(function(){ 
				
				document.getElementById("password_container").innerHTML='<center><h4 class="title">Logging Out..</h4></center>';
				setTimeout(function(){ window.location.assign("logout.php"); }, 2000);	

				}, 3000);		
				}
				else
				{
					//FAILED
					echo("Password Update Failed..","danger");
					$(".loader").hide();	
					return;
					
						
				}
							
							});
	
}

function update_profile()
{
	
	
	
			var fullname=$("#fullname").val();
			var email=$("#email").val();
			var phone=$("#phone").val();
			if(fullname.length<2||email.length<2||phone.length<2)
			{
				echo("Missing Some Inputs..","danger");
				return;
			}
			$(".loader").show();
			
	
			
			$.post( "../views/update_profile.php?id=1", 
			{ fullname: fullname,email: email,phone: phone })
			  .done(function( data ) {
	 
	
				console.log(data);
				
				if(data==1)
				{
					//FAILED
					echo("Failed Updating Profile..","danger");
					$(".loader").hide();	
					return;
					
					
				}
				
				else if(data==0)
				{
					//SUCESSFULL
					//echo("Working On Ticket Id: "+ticket_id,"success");
					document.getElementById("profile_container").innerHTML='<center><h4 class="title">Your Profile Has Been Updated</h4></br><img src="../images/check.png" style="width:50px;"></center>';
					
					$(".loader").hide();	
					$("#update").hide();	
					$("#prclose").hide();	
					echo("Profile Update Successful..","success");
					setTimeout(function(){
							document.getElementById("profile_container").innerHTML='<center><h4 class="title">System Will Logout In 3sec To Effect</h4></br><img src="../images/check.png" style="width:50px;"></center>';
					}, 2000);		
					setTimeout(function(){ 
					
					document.getElementById("profile_container").innerHTML='<center><h4 class="title">Logging Out..</h4></center>';
					setTimeout(function(){ window.location.assign("logout.php"); }, 2000);	

					}, 3000);		
				
					return;
				
						
				}
				else
				{
					echo("Failed Updating Profile..","danger");	
					$(".loader").hide();	
					return;
				}
							});
	
}