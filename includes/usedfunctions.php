<?php



//Function to sanitize values received from the form. Prevents SQL injection
function clean($str) {
    $str = @trim($str);
    if (get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return str_replace("'","",addslashes($str));
}

function send_notification($to,$subject,$message,$cc)
{
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	$headers .= "To: CUSTOMER <".$to.">"  . "\r\n";
	$headers .= "From: UNION SYSTEMS GLOBAL SUPPORT  <SUPPORT24X7@UNIONSG.COM>" . "\r\n";
	$headers.=$cc;
	

	   mail( $to, $subject,$message,$headers);
}
function notify_union_staff($to,$subject,$message,$from,$cc)
{
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	$headers .= "To: CUSTOMER <".$to.">"  . "\r\n";
	$headers .= "From: UNION SYSTEMS GLOBAL SUPPORT PORTAL  <$from>" . "\r\n";
	$headers.=$cc;
	

	   mail( $to, $subject,$message,$headers);
}

function notify_request($to,$subject,$message)
{
    $cc="";
	//$cc="Cc: chris.armarfio@unionsg.com\r\n";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	$headers .= "To: UNION <".$to.">"  . "\r\n";
	$headers .= "From: CHANGE MANAGEMENT ONLINE REQUEST  <SUPPORT24X7@UNIONSG.COM>" . "\r\n";
	$headers.=$cc;
	

	   mail( $to, $subject,$message,$headers);
}

function notify_one_request($to,$subject,$message)
{
  
	$cc="";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	$headers .= "To: UNION <".$to.">"  . "\r\n";
	$headers .= "From: CHANGE MANAGEMENT ONLINE REQUEST  <SUPPORT24X7@UNIONSG.COM>" . "\r\n";
	$headers.=$cc;
	

	   mail( $to, $subject,$message,$headers);
}

// end function	
?>