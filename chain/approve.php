<?php

include("dbconn.php");
require_once('../Connections/ticket.php');
require_once('../includes/usedfunctions.php');
@session_start();

if(isset($_GET['id'])&&isset($_GET['user'])){
    $requestor_id=$_GET['id'];
    $user=$_GET['user'];
    $Hasrow=false;

    $linkToApprove="";$message="";$sql="";$requestor_email="";$notification="";$submittername="";
    $PmEmail="daniel.eyeson@unionsg.com";$ChrisEmail="chris.armarfio@unionsg.com";$SamEmail="samuel.armarfio@unionsg.com";$subject="Change Management Needs Approval";
    
  	$tb_requestor_query = mysqli_query($conn,"select * from tb_requestor 
  	where requestor_id='$requestor_id'")or die(mysqli_error($conn));
		while($row = mysqli_fetch_array($tb_requestor_query)){
		 $Hasrow=true;
		 $message=base64_decode($row["CacheData"]);
		 $requestor_email=$row["requestor_email"];
		 $submittername=$row["submittername"];
		}  
		 	
		if(!$Hasrow){echo "An Error has occured while approving. Request Id Does not Exist";die;}
	
	    if(isset($_GET['code'])){
	        
	        
	         $sql="UPDATE tb_requestor set 	FinalApproverFlag='RJT' where requestor_id='$requestor_id'";
	         if(mysqli_query($conn,$sql)){
	             $notification= "<center><font color='green'>Change has been Rejected. A feedback notification has been sent to the requestor. Thank you</font></center>";
	             notify_request($requestor_email,$subject,"Your request has been rejected by a member of the approval team .<br/> Thank You.");//notify requestor
	         }
	
	
	
	    }else{
	        	
	        switch($user){
	            
		    case "teamlead":{
		     
		         $linkToApprove=" <b><font color='blue'> <a href='http://issues.unionsg.com/chain/approve.php?id=$requestor_id&user=pm'>Click here to Approve</a></font></b>
		         <br/><br/><b><font color='red'> <a href='http://issues.unionsg.com/chain/approve.php?id=$requestor_id&user=pm&code=RJT'>Click here to Reject</a></font></b>";
		         $message.= "<br/><br/> Submitted By: <b>$submittername</b></br/>".$linkToApprove;
		        
		         notify_request($PmEmail,$subject,$message);//notify the next approver
		         notify_request($requestor_email,$subject,"Your request has been approved by your team leader.<br/> Thank You.");//notify requestor

		         send_notification($PmEmail,$subject,"Your request has been approved by your team leader.<br/> Thank You.",$requestor_email)

		         $sql="UPDATE tb_requestor set 	teamleader_flag='1' where requestor_id='$requestor_id'";
		           
                 break;
		    }
		     case "pm":{
		         
		         $linkToApprove=" <b><font color='blue'> <a href='http://issues.unionsg.com/chain/approve.php?id=$requestor_id&user=final'>Click here to Approve</a></font></b>
		         <br/><br/><b><font color='red'> <a href='http://issues.unionsg.com/chain/approve.php?id=$requestor_id&user=final&code=RJT'>Click here to Reject</a></font></b>";
		          $message.= "<br/><br/> Submitted By: <b>$submittername</b></br/>".$linkToApprove;
		         notify_request($ChrisEmail.",".$SamEmail,$subject,$message);//notify the next approver
		         notify_request($requestor_email,$subject,"Your request has been approved by your project manager.<br/> Thank You.");//notify requestor

		         send_notification($ChrisEmail.",".$SamEmail,$subject,"Your request has been approved by your project manager.<br/> Thank You.",$requestor_email);

		         $sql="UPDATE tb_requestor set ProjectManagerFlag='1' where requestor_id='$requestor_id'";
		        break;
		    }
		    case "final":{
		        $sql="UPDATE tb_requestor set FinalApproverFlag='1' where requestor_id='$requestor_id'";
		         notify_request($requestor_email,$subject,"Your change has now been Accepted. You can now implement it now.<br/> Thank You.");//notify requestor
		        break;
		    }
		    
		    
		}
		
		if(mysqli_query($conn,$sql)){$notification= "<center><font color='green'>Approval Successful. A feedback notification has been sent to the requestor. Thank you</font></center>";}
		else{
		    $notification= "<center><font color='red'>An Error Has occured while approving. Please try again.</font></center>"; 
		}
		
		
     }


	   
		
    }else { $notification= "request Id(Get) and user(Get) not found";}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Approve</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body background="../extras/mountain.jpeg">
 <br/> <br/> <br/> <br/> 
<div class="container-fluid">
  <div class="alert alert-success">
  <strong>Alert!</strong> <?php echo $notification;?>
</div> 
</div>

</body>
</html>
