-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2017 at 01:15 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cmp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_ccb_final_decision`
--

CREATE TABLE IF NOT EXISTS `tb_ccb_final_decision` (
`id` int(11) NOT NULL,
  `decision` varchar(255) NOT NULL,
  `decision_date` varchar(24) NOT NULL,
  `decison_explanation` varchar(255) NOT NULL,
  `conditions` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ccb_final_decision`
--

INSERT INTO `tb_ccb_final_decision` (`id`, `decision`, `decision_date`, `decison_explanation`, `conditions`) VALUES
(1, ' Approved', 'bdfgb', 'nfgn', 'fgn');

-- --------------------------------------------------------

--
-- Table structure for table `tb_requestor`
--

CREATE TABLE IF NOT EXISTS `tb_requestor` (
`requestor_id` int(11) NOT NULL,
  `type_of_CR` varchar(255) NOT NULL,
  `project_bau_initiative` varchar(255) NOT NULL,
  `submittername` varchar(255) NOT NULL,
  `brief_description_of_request` varchar(255) NOT NULL,
  `date_submitted` varchar(255) NOT NULL,
  `date_required` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `reason_for_change` varchar(255) NOT NULL,
  `other_artifacts_impacted` varchar(255) NOT NULL,
  `name_of_artifacts_impacted` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `attachments_for_test_results` varchar(255) NOT NULL,
  `client_affected` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `version_of_x100_requring_change` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_requestor`
--

INSERT INTO `tb_requestor` (`requestor_id`, `type_of_CR`, `project_bau_initiative`, `submittername`, `brief_description_of_request`, `date_submitted`, `date_required`, `priority`, `reason_for_change`, `other_artifacts_impacted`, `name_of_artifacts_impacted`, `comments`, `attachments_for_test_results`, `client_affected`, `country`, `version_of_x100_requring_change`) VALUES
(13, ' Enhancement', 'thank/No/come', '2ff34234', '3ct5', 'Mon, Oct - 2017', '', 'High', 'vevheheh  vhjveh3e f3e efie3', 'Forms', 'dx4dd4', 'xf43fg4gt5 5yiuhgtj h orth w4rthrh rt', 'Yes', 'no', 'Ghana', 'vvre3'),
(14, ' Enhancement', 'thank/No/come', '2ff34234', '3ct5', 'Mon, Oct - 2017', '12/09/2017', 'High', 'vevheheh  vhjveh3e f3e efie3', 'Forms', 'dx4dd4', 'xf43fg4gt5 5yiuhgtj h orth w4rthrh rt', 'Yes', 'no', 'Ghana', 'vvre3'),
(15, ' Enhancement', 'r34r', 'vrb', '', 'Mon, Oct - 2017', 'dd/mm/yyyy', 'Medium', 'rgt', 'Forms', 'rg', 'rg', '', 'rg', 'Sierra Leone', 'grg'),
(16, ' Enhancement', 'r34r', 'vrb', '', 'Mon, Oct - 2017', 'dd/mm/yyyy', 'Medium', 'rgt', 'Forms', 'rg', 'rg', '', 'rg', 'Sierra Leone', 'grg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_team_lead`
--

CREATE TABLE IF NOT EXISTS `tb_team_lead` (
`teamlead_id` int(11) NOT NULL,
  `approve_reject` varchar(255) NOT NULL,
  `reason_for_rejection` varchar(255) NOT NULL,
  `duration_impact` varchar(255) NOT NULL,
  `schedule_impact` text NOT NULL,
  `cost_impact` double(10,2) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `recommendations` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_team_lead`
--

INSERT INTO `tb_team_lead` (`teamlead_id`, `approve_reject`, `reason_for_rejection`, `duration_impact`, `schedule_impact`, `cost_impact`, `comments`, `recommendations`) VALUES
(7, ' Approved', ' nv jcvf', 'hvwjerjkh', 'gchgceduj', 200.00, '0', '34rgjvc3'),
(8, ' Approved', ' nv jcvf', 'hvwjerjkh', 'gchgceduj', 200.00, '0', '34rgjvc3'),
(9, ' Approved', 'rrgrg', 'g4g4g', '4g4e4g', 200.00, 'dwrgrgtegetgf', 'fferre');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_ccb_final_decision`
--
ALTER TABLE `tb_ccb_final_decision`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_requestor`
--
ALTER TABLE `tb_requestor`
 ADD PRIMARY KEY (`requestor_id`);

--
-- Indexes for table `tb_team_lead`
--
ALTER TABLE `tb_team_lead`
 ADD PRIMARY KEY (`teamlead_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_ccb_final_decision`
--
ALTER TABLE `tb_ccb_final_decision`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_requestor`
--
ALTER TABLE `tb_requestor`
MODIFY `requestor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_team_lead`
--
ALTER TABLE `tb_team_lead`
MODIFY `teamlead_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
