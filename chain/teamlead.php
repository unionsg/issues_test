<?php
include("dbconn.php");
require_once('../Connections/ticket.php');
require_once('../includes/usedfunctions.php');
@session_start();
$message="";
     if(isset($_POST["submit"])) {
     	//looping to accept checked box
     	$approve_reject = $_POST["approve_reject"];    
      $reason_for_rejection = $_POST["reason_for_rejection"];
     	$duration_impact = $_POST["duration_impact"];
     	$schedule_impact = $_POST["schedule_impact"];
     	$cost_impact = $_POST["cost_impact"];
     	$comment = $_POST["comment"];
     	$recommendations = $_POST["recommendations"];
		$attachments_for_test_results = $_POST["attachment"];
		

      if ($approve_reject=='' or $duration_impact=='' or $schedule_impact=='' or $cost_impact=='' or $comment=='' or $recommendations==''){
    
	echo "<center><h3><font color='red'>Please complete all required fields<br/><a href='teamlead.php'><u>Click Here to try again</u></a></font></h3></center>" ;   die;
 
    exit();
    } 

      //inserting information into database
    $requestor_id=$_SESSION['requestor_id'];
     $teamleaderapprovername=$_SESSION['SUBMITTER'];
echo 1;die;
       $insert_query = "update tb_requestor set teamleaderapprover='$teamleaderapprovername', 
       teamleader_flag='$approve_reject', reason_for_rejection='$reason_for_rejection', 
       duration_impact='$duration_impact', schedule_impact='$schedule_impact', cost_impact='$cost_impact',
       comments='$comment', recommendations='$recommendations' where requestor_id='$requestor_id',attachments_for_test_results='$attachments_for_test_results'";
       //echo $insert_query;die;
              if(mysql_query($insert_query)){
    
   /* echo "<script>alert('Send Successfuly')</script>"; */ 
    $message= "<script>echo('Approval Successful. Redirecting...','success'); echo('Email sent to Requestor','success');setTimeout(function(){window.location.assign('fetch.php');},2000);</script>";
    
    $mess="";
     
     if($approve_reject="RJT"){
         
      $mess=" You raised a change <font color='blue'>$requestor_id</font>.
     This change has been <font color='red'>Rejected</font> by your team leader. Reason - <i>$reason_for_rejection</i> . It may require your re-submission at later date.
     <br/><br> 
     Thank You. ";  
     
     }else{
         
       $mess=" You raised a change <font color='blue'>$requestor_id</font>.
     This change has been <font color='green'>Approved</font> Successfully by your team leader.
     <br/><br> 
     Thank You. ";     
     }
     
    $message="
    
    <p> 
    $mess<br/><br/>
    <p>
    <b>Decision Date:</b> $decision_date <br/>
      <b>Comment:</b> $comment <br/>
        <b>Conditions:</b> $conditions <br/>
          <b>Recommendation:</b> $recommendations <br/>
    </p>
    ";
    
    $to=$_SESSION['requestor_email'];
    
    $subject="CHANGE MANAGEMENT TEAM LEADER REVIEW";
    notify_one_request($to,$subject,$message);
    
    //echo "<center><h3><font color='green'>Approval Successful. Redirecting...</font></h3></center><script>setTimeout(function(){window.location.assign('fetch.php');},2000);</script>" ;   die;
    
    }else{

    // echo "<center><h3><font color='red'>An Error has occured whiles approving. <br/><a href='teamlead.php'><u>Click Here to try again</u></a></font></h3></center>" ;   die;
     $message= "<script>	echo('An Error occurred please try again.','danger');</script>";
     }
}

?>
<!doctype html>
<html lang="en">
<head>
	   <?php include '../views/header_script1.php'; ?>
	   <link href='../views/assets/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
	     <link rel="stylesheet" href="css/pikaday.css">
<script src="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<link href="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">

	   <?php include '../views/nav1.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Team Lead-Initial Analysis</h4>
									  </div>
	                            <div class="card-content" >
	                           
	                                   
	                                    <div class="row">
	                                        <div class="col-md-12">
												 <div class="card-content table-responsive">
											
											
											<!----------START HERE--------------->
											<form method='post'>
								<div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Approve Or Reject </label>
													<select style='float:right;'class="form-control" onchange="hide_Approve(this);" ><option value=''></option><option value='1'>Approve</option><option value='2'>Reject</option></select>
<textarea  style='float:right;'  class="form-control"  name="approve_reject" id='approve_reject' placeholder='Reason For Rejection'></textarea> 
													
												</div>
	                                        </div>
                                      
											 <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Duration Impact (Hours)</label>
						<input class="form-control" id="time" name="duration_impact" required type="text">
												</div>
	                                        </div>
	                                       
	                                    </div>
										
										
										
										<div class="row">
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Schedule Impact </label>
													  <input class="form-control" name="schedule_impact"  id="datepicker-theme1" required type="text"></label>

													
												</div>
	                                        </div>
											
	                                        <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Impact </label>
													<input class="form-control" name="cost_impact" required type="text"></label>

												</div>
	                                        </div>
											 <div class="col-md-4">
												<div class="form-group label-floating">
													<label class="control-label">Comment </label>
													<textarea class="form-control"  name="comment"></textarea>

												</div>
	                                        </div>
	                                       
	                                    </div>
										
										
										
										<div class="row">
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Recommendations </label>
													 <input type="text" name="recommendations" required class="form-control"></label>

													
												</div>
	                                        </div>
											
	                                        <div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Attachment For Test Result  </label>
													<select style='float:right;'class="form-control" onchange="hide_attachment(this);" ><option value=''></option><option value='1'>Yes</option><option value='2'>No</option></select>
<br/><br/><br/><textarea  style='float:right;'  class="form-control"  name="attachment" id='attachment' placeholder='Type result here..'></textarea> 
												</div>
	                                        </div>
											
	                                       
	                                    </div>
										
<input type="submit" value="Submit" name="submit" class="btn btn-success ">
        </form>
			
											<!-----------END HERE---------------->
											</div>
												
												
	                                        </div>
	                                    </div>


										<div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						
	                </div>
	            </div>
	        </div>

	       	<?php include '../views/footer.php'?>
		</div>
	</div>

<!-------MODAL START----->
<!-- Modal -->
</body>

	<?php include '../views/footer_script.php'?>
	<script src="../views/assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	
	<!--<script src="../js/add_company.js" type="text/javascript"></script>----->
	<script>
	$('#page99').addClass("active");
	$(document).ready(function(){
    $('#form_content').DataTable();
});

</script>
var timepicker = new TimePicker('time', {
  lang: 'en',
  theme: 'dark'
});

var input = document.getElementById('time');

timepicker.on('change', function(evt) {
  
  var value = (evt.hour || '00') + ':' + (evt.minute || '00');
  evt.element.value = value;

});


	<script src="jdate/pikaday.js"></script>
    <script>

     // You can use different themes with the `theme` option
    var pickerDefault = new Pikaday(
    {
        field: document.getElementById('datepicker'),
    });

    var pickerTheme = new Pikaday(
    {
        field: document.getElementById('datepicker-theme'),
        theme: 'dark-theme'
    });
	
	 var pickerTheme1 = new Pikaday(
    {
        field: document.getElementById('datepicker-theme1'),
        theme: 'dark-theme'
    });
    
    
    var pickerTriangle = new Pikaday(
    {
        field: document.getElementById('datepicker-triangle'),
        theme: 'triangle-theme'
    });
	$('#attachment').hide();
	function hide_attachment(a){
	if(a.value==2){
		$('#attachment').hide("slow");
	}else{
		$('#attachment').show("slow");
	}
}

	$('#approve_reject').hide();
	function hide_Approve(a){
	if(a.value==1){
		$('#approve_reject').hide("slow");
	}else{
		$('#approve_reject').show("slow");
	}

	
	}

</script>	

<?php echo $message;?>
</html>
