<?php

require('../Connections/ticket.php');
@session_start();
if(isset( $_SESSION['COMPANY'])){
    if( $_SESSION['COMPANY']==0){
	$_SESSION['SUBMITTER']=$_SESSION['FULL_NAME'];
	if($_SESSION['USER_EMAIL']=="SUPPORT24X7@UNIONSG.COM"){
	  	$_SESSION['ADMIN']=1;  
	}else{
	    	$_SESSION['ADMIN']=0;  
	}

    }else{
        echo "illegal entry 1..";die;
    }
    
}else{
     echo "illegal entry 2..";die;
}

$CLEAR_LOCAL_ELEMENT=false;
if(isset($_GET['cle'])){
	if($_GET['cle']==true){
		$CLEAR_LOCAL_ELEMENT=true;
	}
}

?>
<!doctype html>
<html lang="en">
<head>
	   <?php include '../views/header_script1.php'; ?>
	   <link href='../assets/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="wrapper">

	   <?php include '../views/nav1.php'; ?>
	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="card">
	                            <div class="card-header" data-background-color="purple">
	                                <h4 class="title">Change Request-List <button type="button" style='float:right;margin-top:-7px;'class="btn btn-info " onclick="window.location.assign('archives.php');" > View Approved/ Rejected Request </button>&nbsp;&nbsp;&nbsp;<button type="button" style='float:right;margin-top:-7px;'class="btn btn-success " onclick="window.location.assign('requestor.php');" > New Request +</button><button type="button" id='btnExport' style='float:right;margin-top:-7px;'class="btn btn-default " > Export </button></h4>
									  </div>
	                            <div class="card-content" >
	                           
	                                   
	                                    <div class="row">
	                                        <div class="col-md-12">
												 <div class="card-content table-responsive" style='overflow-x:scroll;'>
											
														 <table class="table table-hover" id='form_content'>  
      
                       
  	
		<thead>		
		        <tr>	

                    
					<th style='display: none;'><b>Project Business As Usual Initiative</b></th>   
					<th><b>CH_Number</b></th>
					<th><b>IN_Number</b></th>
					<th><b>SubmitterName</b></th>
			        <th style='display: none;'><b>Brief Description Of Request</b></th>
					<th><b>Date_Submitted</b> </br></th>
					<th><b>Date_Required</b></th>
                    <th><b>Priority</b></th>
                   <th style='display: none;'><b>Reason for Change</b></th> 
                   <th style='display: none;'><b>Artifacts Impacted</b></th>
                   <th style='display: none;'><b>Names of Artifacts Impacted</b></th> 
                    <th style='display: none;'><b>Comments</b></th> 
                   <!-- <th style='display: none;'>Attachments for Test Results</th>  -->
                    <th><b>Client_Affected</b></th>
                    <th><b>Country</b></th>
                    <th style="color: green"><b>Status</b></br></th>

            <th style='display: none;'><b>Version Of x100 Requring Change</b></th> 
          			<th></th>
                    </tr>

		</thead>
<tbody>
	
<?php
		include 'dbconn.php';
		$tb_requestor_query = mysqli_query($conn,"select * from tb_requestor where FinalApproverFlag IS NULL ORDER BY Id DESC")or die(mysqli_error($conn));
		while($row = mysqli_fetch_array($tb_requestor_query)){
		$requestor_id = $row['requestor_id'];
		$requestor_email = $row['requestor_email'];

		
?>

									
		<tr>
		<td style='display: none;'><div id="<?php echo "project_bau_initiative".$requestor_id?>"><?php echo $row['project_bau_initiative']; ?></div></td> 
			<td><font color='blue'><?php echo $row['requestor_id']; ?></font></td>
			<td><font color='blue'><?php echo $row['Ticket_Id']; ?></font></td>
			<td><?php echo $row['submittername']; ?></td>
			<td style='display: none;'><div id="<?php echo "brief_description_of_request".$requestor_id?>"><?php echo $row['brief_description_of_request']; ?></div></td>
			<td><?php echo substr($row['date_submitted'], 0, -1); ?></td>
			<td><?php echo $row['date_required']; ?></td>	
           <td><?php echo $row['priority']; ?></td>
          <td style='display: none;'><div id="<?php echo "reason_for_change".$requestor_id?>"><?php echo $row['reason_for_change']; ?></div></td> 
          <td  style='display: none;'><div id="<?php echo "other_artifacts_impacted".$requestor_id?>"><?php echo $row['other_artifacts_impacted']; ?></div></td> 
         <td style='display: none;'><div id="<?php echo "name_of_artifacts_impacted".$requestor_id?>"><?php echo $row['name_of_artifacts_impacted']; ?></div></td> 
           <td style='display: none;'><div id="<?php echo "comments".$requestor_id?>"><?php echo $row['comments']; ?></div></td> 
           <!-- <td  style='display: none;'><div id="<?php echo "attachments_for_test_results".$requestor_id?>"><?php echo $row['attachments_for_test_results']; ?></div></td>  -->
           <td><?php echo $row['client_affected']; ?></td>
           <td><?php echo $row['country']; ?></td>
           <td><div style='display:none' id="<?php echo "teamleaderapprover".$requestor_id?>"><?php echo $row['teamleaderapprover']; ?></div><font color='red'>Pending</font></td>
 <td style='display: none;'><div id="<?php echo "version_of_x100_requring_change".$requestor_id?>"><?php echo $row['version_of_x100_requring_change']; ?></div></td>
         <td><a data-toggle="modal" href="#" data-target="#myModal" button class="btn btn-info btn-xs"  onclick="getdetails('<?php echo $requestor_id?>','<?php echo $requestor_email;?>');">View Details</a></td> 
            </tr>

											
	<?php } ?>   

</tbody>
</table>

											</div>
												
												
	                                        </div>
	                                    </div>


										<div class="clearfix"></div>
	                               
	                            </div>
	                        </div>
	                    </div>
						
	                </div>
	            </div>
	        </div>

	       	<?php include '../views/footer.php'?>
		</div>
	</div>

<!-------MODAL START----->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style='margin-top:-50px;'> 
 <div class="modal-dialog">  

    <!-- Modal content-->

   <div class="modal-content">
      <div class="modal-header">
 
     
      </div>
      <div class="block">
      <div class="modal-body" style='height:500px; width: 600px; overflow-y: scroll; font-family: "Times New Roman", Times, serif; font-size: 18px;'>
         
    <label>Project / Business As Usual / Initiative</label> <div id='project_bau_initiative'></div> 
    <hr>
      <label>Brief Description Of Request</label> <div id='brief_description_of_request'></div>
      <hr>
    <label>Reason For Change</label> <div id='reason_for_change'></div> 
    <hr>
       <label>Artifacts Impacted </label><div id='other_artifacts_impacted'></div>
       <hr>
     <label>Name Of Artifacts Impacted </label> <div id='name_of_artifacts_impacted'></div>
     <hr>
       <!-- <label>Attachments For Test Results </label><div id='attachments_for_test_results'></div> 
       <hr>  -->
      <label> Version Of x100 Requring Change </label> <div id='version_of_x100_requring_change'></div>
      <hr>
	  
	  <hr>
      <label> Comments</label> <div id='comments'></div>
      <hr>
	   <label> Name Of Team Leader Who  Approved</label> <div id='teamleaderapprover'></div>
      <hr>
      </div>  
  </div>
  
  <a href="#" button class="btn btn-success " id="approve" style='float:right;margin-right:5px; '><font color='white'>CONTINUE</font></a>
      <!--<button type="button" class="close" data-dismiss="modal">&times;</button> -->

       <a href="fetch.php" button class="btn btn-Danger" id="close" style='float:right;margin-right:5px; '><font color='white'>CLOSE</font></a> 
  <!--<a href="fetch.php" button class="btn btn-Danger" id="close" style='float:right;margin-right:5px; '><font color='white'>CLOSE</font></a> -->
      <div class="modal-footer">
       
      </div>
    </div>

  </div>
</div>

</body>

	<?php include '../views/footer_script.php'?>
	<script src="../assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
	
	<!--<script src="../js/add_company.js" type="text/javascript"></script>----->
	<script>
		$('#page99').addClass("active");
	$(document).ready(function(){
    $('#form_content').DataTable();
});

</script>
		
	
<script type="text/javascript">

  

	function getdetails(id,email){
document.getElementById('project_bau_initiative').innerHTML=document.getElementById('project_bau_initiative'+id).innerHTML;

document.getElementById('brief_description_of_request').innerHTML=document.getElementById('brief_description_of_request'+id).innerHTML;
document.getElementById('reason_for_change').innerHTML=document.getElementById('reason_for_change'+id).innerHTML;
document.getElementById('other_artifacts_impacted').innerHTML=document.getElementById('other_artifacts_impacted'+id).innerHTML;
document.getElementById('name_of_artifacts_impacted').innerHTML=document.getElementById('name_of_artifacts_impacted'+id).innerHTML;
//document.getElementById('attachments_for_test_results').innerHTML=document.getElementById('attachments_for_test_results'+id).innerHTML;
document.getElementById('version_of_x100_requring_change').innerHTML=document.getElementById('version_of_x100_requring_change'+id).innerHTML;
document.getElementById('comments').innerHTML=document.getElementById('comments'+id).innerHTML;
document.getElementById('teamleaderapprover').innerHTML=document.getElementById('teamleaderapprover'+id).innerHTML;
document.getElementById('approve').href="where.php?requestor_id="+id+"&email="+email;

	}
</script> 
<script>
	var CLEAR_LOCAL_ELEMENT="<?php echo $CLEAR_LOCAL_ELEMENT; ?>";
	if(CLEAR_LOCAL_ELEMENT){
		if (typeof(Storage) !== "undefined") {
			
			document.getElementById('mainbody').innerHTML="<center><h3> <font color='green'>Change Logged Successfully</font></h3><br/><a href='../views/dashboard.php'><u>Click Here to go back</u></a></center>";
		
			localStorage.clear();
	
		}
	}
	
	
	
	$("#btnExport").click(function (e) {

    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('form_content'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"Change_Management.xls");
    }
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));


});
	
	</script>
	
</html>
