-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2017 at 08:23 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `final_decision`
--

CREATE TABLE IF NOT EXISTS `final_decision` (
`id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `decision` varchar(255) NOT NULL,
  `decision_date` varchar(24) NOT NULL,
  `decision_explanation` varchar(255) NOT NULL,
  `conditions` varchar(255) NOT NULL,
  `approver1` varchar(50) NOT NULL,
  `date1` date NOT NULL,
  `approver2` varchar(50) NOT NULL,
  `date2` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `final_decision`
--

INSERT INTO `final_decision` (`id`, `requester_id`, `decision`, `decision_date`, `decision_explanation`, `conditions`, `approver1`, `date1`, `approver2`, `date2`) VALUES
(1, 0, 'Approved', 'Thu Nov 02 2017', 'juytd', 'jhg', '', '0000-00-00', '', '0000-00-00'),
(2, 0, 'Approved', 'Thu Nov 02 2017', 'fgh', 'retrytg', 'Lawrence', '0000-00-00', '', '0000-00-00'),
(3, 0, 'Approved', 'Thu Nov 02 2017', 'fgh', 'retrytg', '', '0000-00-00', 'Lawrence', '2017-11-02'),
(4, 0, 'Approved', 'Wed Nov 01 2017', 'qqq', 'qqq', 'Solomon', '2017-11-02', '', '0000-00-00'),
(5, 0, 'Rejected', 'Thu Nov 02 2017', 'qqq', 'qqq', '', '0000-00-00', 'Solomon', '2017-11-02'),
(6, 0, 'Approved', 'Thu Nov 02 2017', 'qqqs', 'ssds', 'Solomon', '2017-11-02', '', '0000-00-00'),
(7, 0, 'more Formation', 'Thu Nov 02 2017', 'jnijbija', 'n ai ae', '', '0000-00-00', 'Solomon', '2017-11-02'),
(8, 0, 'more Formation', 'Wed Nov 01 2017', 'eee', 'eee', 'Solomon', '2017-11-02', '', '0000-00-00'),
(9, 0, 'Approved', 'Wed Nov 01 2017', 'ssss', 'sssss', '', '0000-00-00', 'Solomon', '2017-11-02'),
(10, 0, 'Approved', 'Wed Nov 01 2017', 'sss', 'sss', 'Solomon', '2017-11-02', '', '0000-00-00'),
(11, 0, 'Approved', 'Thu Nov 02 2017', 'DKJD BD', 'jnc ijdx', '', '0000-00-00', 'Solomon', '2017-11-02'),
(12, 0, 'Approved', 'Wed Nov 01 2017', 'sdcds', 'dsd', 'Solomon', '2017-11-02', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_requestor`
--

CREATE TABLE IF NOT EXISTS `tb_requestor` (
`requestor_id` int(11) NOT NULL,
  `type_of_CR` varchar(255) NOT NULL,
  `project_bau_initiative` varchar(255) NOT NULL,
  `submittername` varchar(255) NOT NULL,
  `brief_description_of_request` varchar(255) NOT NULL,
  `date_submitted` varchar(255) NOT NULL,
  `date_required` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `reason_for_change` varchar(255) NOT NULL,
  `other_artifacts_impacted` varchar(255) NOT NULL,
  `name_of_artifacts_impacted` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `attachments_for_test_results` varchar(255) NOT NULL,
  `client_affected` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `version_of_x100_requring_change` varchar(255) NOT NULL,
  `approver1` varchar(50) NOT NULL,
  `date1` date NOT NULL,
  `approver2` varchar(50) NOT NULL,
  `date2` date NOT NULL,
  `teamleaderapprover` varchar(50) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_requestor`
--

INSERT INTO `tb_requestor` (`requestor_id`, `type_of_CR`, `project_bau_initiative`, `submittername`, `brief_description_of_request`, `date_submitted`, `date_required`, `priority`, `reason_for_change`, `other_artifacts_impacted`, `name_of_artifacts_impacted`, `comments`, `attachments_for_test_results`, `client_affected`, `country`, `version_of_x100_requring_change`, `approver1`, `date1`, `approver2`, `date2`, `teamleaderapprover`, `date`) VALUES
(81, 'Enhancement', 'Project', 'Evans Ampiah', 'Software Complicated', 'October 24, 2017, 12:44 PM', '20/12/2017', 'Medium', 'new', 'Forms', 'kk', 'i will fix it in no time', 'yes', 'All ', 'Ghana', 'v.50', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(82, 'Enhancement', 'Project', 'nnwnww', 'wnwnwn', 'October 24, 2017, 02:36 PM', '11/11/1111', 'Low', ' w w w', 'Forms', 'wwnwnw', 'wwnwnw', 'yes', 'w w wnwn', 'Ghana', 'w w  ww ', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(83, 'Enhancement', 'Project', 'nk qri g', 'hj ci qh ', 'October 24, 2017, 02:40 PM', '11/11/1111', 'Low', 'jbaflaf', 'Forms', 'jnbajl', 'jbfajlf', 'yes', 'ajbjfl', 'Ghana', 'jafljalf', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(84, 'Enhancement', 'Project', 'ng ig', 'nssn', 'October 25, 2017, 11:26 AM', '11/2/2014', 'Low', 'snssn', 'Forms', 'nsss', 'snsns', 'yes', 'snsns', 'Ghana', 's s ', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(85, 'Enhancement', 'Project', 'gggg', 'buhvo ', 'October 25, 2017, 12:27 PM', '11/09/1009', 'Low', 'hehej', 'Forms', 'hdu ', 'uhdud ', 'yes', 'ud u', 'Ghana', 'uh u', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(86, 'Enhancement', 'Project', 'gggg', 'buhvo ', 'October 25, 2017, 12:27 PM', '11/09/1009', 'Low', 'hehej', 'Forms', 'hdu ', 'uhdud ', 'yes', 'ud u', 'Ghana', 'uh u', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(87, 'Enhancement', 'Project', 'br3 j', 'ub8ufb8', 'October 25, 2017, 05:04 PM', '11/11/2222', 'Low', 'jnjfb8', 'Forms', 'ubdb8', 'bu8bd', 'yes', 'bb8', 'Ghana', 'n', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(88, 'Enhancement', 'Project', 'nj gre i', 'jbicb8', 'October 25, 2017, 05:26 PM', '12/10/2013', 'Low', 'knfon', 'Forms', 'bjjdb9', 'jbjbbo', 'yes', 'ibi', 'Ghana', 'ibib', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(89, 'Enhancement', 'Project', 'Solomon Asante', 'buhvhuvvf', 'October 25, 2017, 06:16 PM', '11/11/1111', 'Low', 'hb2b', 'Function', 'boubu', 'hvuxvuv', 'no', '   b u', 'Liberia', '555', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(90, 'Enhancement', 'Project', 'Lawrence Casely_hayford', 'bjrjr', 'October 27, 2017, 12:08 PM', '10/03/2017', 'Low', 'djjddjd', 'Forms', 'rrrr', 'rrrr', 'yes', 'rrr', 'Ghana', 'rrrr', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(91, 'Enhancement', 'Project', 'Lawrence', 'hththt', 'October 31, 2017,', '10/03/2017', 'Low', 'hfhfhfr', 'Forms', 'hfhfhf', 'fnfnf', 'yes', 'fbfbfb', 'Ghana', 'bfbfbf', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(92, 'Enhancement', 'Project', 'Asante Solomon', 'It giving So much error in the system', 'October 31, 2017,', '10/31/2017', 'Regulatory', 'Old system', 'Procedures', 'x100', 'please ', 'yes', 'Un bank', 'Ghana', 'v1.56', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(93, 'Enhancement', 'Project', 'name', 'shshsh', 'November 1, 2017,', '11/06/2017', 'Low', 'hvxvuh', 'Forms', 'vguxcvixckj', 'cxugx', 'yes', 'jhgvcikuvc', 'Ghana', 'hvduvd', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(94, 'Enhancement', 'Business As Usual', 'Solomon Asante', 'Vitual System', 'November 1, 2017,', '11/02/2017', 'Low', 'upgrade due to the new version of android.', 'Procedures', 'saame', 'Please customers are complaining too much', 'no', 'Two', 'Sierra Leone', 'v.12', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(96, 'Enhancement', 'Project', 'Solomon Asante', 'wsb eq', 'November 2, 2017,', 'Wed Nov 01 2017', 'Low', 'b u i2', 'Forms', 'hu2vu', '8hvvh', 'yes', 'yuv', 'Ghana', 'huu', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(97, 'Enhancement', 'Project', 'Solomon Asante', 'b ygyg', 'November 2, 2017,', 'Thu Nov 02 2017', 'Low', 'b j ujeu', 'Forms', 'h uq vuo', 'hucv9hc', 'yes', 'bhhh', 'Ghana', 'jjj', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(98, 'Enhancement', 'Project', 'Solomon', 'ddddd', 'November 2, 2017,', 'Wed Nov 01 2017', 'Low', 'wwwww', 'Forms', 'www', 'www', 'yes', 'www', 'Ghana', 'www', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00'),
(99, 'Enhancement', 'Project', 'Solomon', 'nkdsnkdks', 'November 2, 2017,', 'Wed Nov 01 2017', 'Low', 'ddcd', 'Forms', 'ddd', 'ddd', 'yes', 'dd', 'Ghana', 'dd', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_team_lead`
--

CREATE TABLE IF NOT EXISTS `tb_team_lead` (
`teamlead_id` int(11) NOT NULL,
  `approve_reject` varchar(255) NOT NULL,
  `reason_for_rejection` varchar(255) NOT NULL,
  `duration_impact` varchar(255) NOT NULL,
  `schedule_impact` text NOT NULL,
  `cost_impact` double(10,2) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `recommendations` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_team_lead`
--

INSERT INTO `tb_team_lead` (`teamlead_id`, `approve_reject`, `reason_for_rejection`, `duration_impact`, `schedule_impact`, `cost_impact`, `comments`, `recommendations`) VALUES
(29, 'australia', 'dhj  uis', 'bssvshs', 'ghhgshshs', 0.00, 'sgsgsss', 'sssgshs'),
(30, 'Approve', 'ndndnd', 'nenenhee', 'eehnenen', 0.00, 'ebbebe', 'bbebebe'),
(31, 'Reject', 'ndndnd', 'nenenhee', 'eehnenen', 0.00, 'ebbebe', 'bbebebe'),
(32, 'Approve', 'ndndnd', 'nenenhee', 'eehnenen', 0.00, 'ebbebe', 'bbebebe'),
(33, 'Approve', 'bdbfbd', 'bebebe', 'bebebe', 0.00, 'bbebe', ' ebee'),
(34, 'Approve', 'nfnwf', 'farq', 'dqwde', 0.00, 'dqw', 'ddd'),
(35, 'Approve', 'ww', 'ww', 'ww', 0.00, 'ww', 'ww'),
(36, 'Approve', 'dwddw', 'wdwd', 'wdwd', 0.00, 'wddw', 'wddw'),
(37, 'Approve', 'hrgvehv', 'bivcdvu', 'vhucwvhv', 0.00, 'usuu', 'ucvh'),
(38, 'Approve', 'cec', 'ccc', 'ccbvwh', 0.00, 'u', 'uhc'),
(39, 'Approve', 'approved', 'so', ' nf', 0.00, ' jde j', 'm d'),
(40, 'Approve', 'so many errors', 'june', 'July ', 0.00, 'thanks', 'thanks'),
(41, 'Approve', 'thanks', 'thanks', 'thanks', 0.00, 'thanks', 'thanks'),
(42, 'Approve', 'sol  ', 'j dj ', ' j jn j', 0.00, 'j cj j', 'j cj'),
(43, 'Approve', 'kn 3v3 i j BIJCB I', ' i i  ijb', ' h hu  u', 0.00, 'b j j', 'b jb b'),
(44, 'Approve', 'k i ij', 'i ', 'u', 0.00, 'h', ' uhr'),
(45, 'Approve', 'b y2 yg', ' yecd68g', 'tcgc', 0.00, 'c9ycytc', '`ycc9'),
(46, 'Approve', 'vygc8qygc', 'ycytctyct', 'ccgy', 0.00, 'ygf', 'gfcgf'),
(47, 'Approve', 'fjhbfibf', 'hj f f', 'h uf', 0.00, 'ffff', 'fff'),
(48, 'Approve', 'fjhbfibf', 'hj f f', 'h uf', 0.00, 'ffff', 'fff'),
(49, 'Approve', 'fjhbfibf', 'hj f f', 'h uf', 0.00, 'ffff', 'fff');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `final_decision`
--
ALTER TABLE `final_decision`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_requestor`
--
ALTER TABLE `tb_requestor`
 ADD PRIMARY KEY (`requestor_id`);

--
-- Indexes for table `tb_team_lead`
--
ALTER TABLE `tb_team_lead`
 ADD PRIMARY KEY (`teamlead_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `final_decision`
--
ALTER TABLE `final_decision`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_requestor`
--
ALTER TABLE `tb_requestor`
MODIFY `requestor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `tb_team_lead`
--
ALTER TABLE `tb_team_lead`
MODIFY `teamlead_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
