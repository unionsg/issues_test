<?php
			require_once('../Connections/ticket.php');
			require_once('../includes/usedfunctions.php');

			if (!isset($_SESSION)) {
			  session_start();
			  ob_start();
			 
			}
			if ($_SERVER["REQUEST_METHOD"] == "POST") 
			{
                        $confirm="";
                        $restype="";
                        $rsa="";
						$assign_ticket=clean($_POST['assign_ticket']);
						$status_code=clean($_POST['status_code']);
						$status_comment=clean($_POST['status_comment']);
					    $root_cause=clean($_POST['root_cause']);
						$status_date=date('Y-m-d H:i:s'); 
						$Status="";		
						if(isset($_POST['restype'])){
						    $restype=$_POST['restype'];
						}
						
								switch($status_code)
						{
						
						case 3:
						{
							$Status="WORK IN PROGRESS";
							continue;
						}	
						case 4:
						{
							$Status="RESOLVED";
							$confirm="<a href='http://issues.unionsg.com/views/confirm?ticket_id=$assign_ticket'>Please click here to confirm resolved issue</a><br/>";
							
							continue;
						}
						
						case 5:
						{
							$Status="CLOSED";
							continue;
						}
						case 6:
						{
							$Status="SUSPENDED";
							continue;
						}

						case 7:
						{
							$Status="CANCELLED";
							continue;
						}
										
						
						}
						
						$sql= "UPDATE incident SET Status='$status_code'
						WHERE Ticket_Id='$assign_ticket'";
						$stmt = $conn->prepare($sql);
						$stmt->execute();
						
						
						//SEND NOTIFICATIONs
						$sql= "SELECT incident.Affected_Person_Email,incident.Summary,incident.Details,users.Email 
						FROM incident INNER JOIN users ON users.Login_Id=incident.Logged_By_Id WHERE Ticket_Id='$assign_ticket' ";
						//echo $sql;die;
						$option_data="";
						$stmt = $conn->prepare($sql);
						$stmt->execute();
						
						$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
						$cc="";
						$summary="";
						$details="";
						for ($y = 0; $y < count($res); $y++) 
						{
							$count=$y+1;
						$email1=$res[$y]['Affected_Person_Email'];
						$email2=$res[$y]['Email'];
						$subject=$res[$y]['Summary'];
						$details=$res[$y]['Details'];
						
						
						$cc="Cc: $email1\r\n";
						$to=$email2;
						
						$resmessage="";
						//when user has selected a resolution type
						if(strlen($restype)>2){
						    $resmessage="<i><h4>Resolution type :</h4></i>".$restype."<br/>";
						}
							
							
							$message="
									<html>
									<body>
									Dear Cherished Client, The Status Of Ticket Number <b><font color='red'>$assign_ticket</font></b> is now at <b>$Status</b> <br/> 
									Thank You.<br>
									$confirm
									<i><h4>Comment Below</h4></i><hr/>
									<font color='red'>$status_comment</font>
								    
								    $resmessage
									<font color='red'>
									<i>
									$details
									</i>
									</font>
									<br/>
										<br/><a href='http://issues.unionsg.com'><u><b>Click Here to Login</b></u></a><br/>
									<img  src='http://unionsg.com/assets/img/logo.png' class='img-circle' />
								
									</body></html>
									";
							$subject="$summary ";
							
							send_notification($to,$subject,$message,$cc); // send Email alerts to user created
							
						}
						//POSTING COMMENT INTO THE COMMENTS PLACE
						if($status_code==4)
						{
						$by= $_SESSION['USER_ID'];
						$sql= "UPDATE incident SET Resolution_Details='$status_comment',Resolved_Date='$status_date' ,Resolved_By='$by', Resolution_Type='$restype', root_cause='$root_cause'
						WHERE Ticket_Id='$assign_ticket'";
						$stmt = $conn->prepare($sql);
						$stmt->execute();	
						}
						else
						{
						$posted_by=$_SESSION['FNAME']; 
						$sql_comment= "INSERT INTO comments (Ticket_Id,Posted_By,Comment,Date_Posted,Is_Image_Attached) VALUES('$assign_ticket','$posted_by','$status_comment','$status_date','0') ";
						$stmt_comment = $conn->prepare($sql_comment);
						$stmt_comment->execute();
						}
						
						
						echo 1;
			}
?>