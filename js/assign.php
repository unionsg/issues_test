<?php
			require_once('../Connections/ticket.php');
			require_once('../includes/usedfunctions.php');

			if (!isset($_SESSION)) {
			  session_start();
			  ob_start();
			 
			}
			if ($_SERVER["REQUEST_METHOD"] == "POST") 
			{
						date_default_timezone_set(Africa/Accra);

                        $posted_by=$_SESSION['FNAME']; 
						$assign_ticket=clean($_POST['assign_ticket']);
						$assign_agent=clean($_POST['assign_agent']);
						$comment=clean($_POST['comment']);
						$assigned_date=date('Y-m-d H:i:s'); 
						$status_date=date('Y-m-d H:i:s'); 

						       $sql= "SELECT Full_Name from users where Login_Id='$assign_agent'";
        						$stmt = $conn->prepare($sql);
        						$stmt->execute();
                                $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                $assign_person = $res[0]['Full_Name'];
						
    							$sql= "SELECT Status from incident where Ticket_Id='$assign_ticket'";
        						$stmt = $conn->prepare($sql);
        						$stmt->execute();
        						$check_status=$res[0]['Status'];
						
						if(empty($check_status) || $check_status=="")
						{
						    	$sql= "UPDATE incident SET Status='2', Assigned_To='$assign_agent',Assigned_Date='$assigned_date'
        						WHERE Ticket_Id='$assign_ticket'";
        						$stmt = $conn->prepare($sql);
        						$stmt->execute();

        						$sql= "INSERT INTO assigned_log_summary (Ticket_Id, Assigned_From, Assigned_To, Assigned_Date, Comment) VALUES ('$assign_ticket', '$posted_by', 
        							'$assign_person', '$assigned_date', '$comment') " ;
										$stmt = $conn->prepare($sql);
        								$stmt->execute(); 
						}
						else
						{
						 	$sql= "UPDATE incident SET Assigned_To='$assign_agent',Assigned_Date='$assigned_date'
        						WHERE Ticket_Id='$assign_ticket'";
        						$stmt = $conn->prepare($sql);
        						$stmt->execute(); 


        						$sql= "INSERT INTO assigned_log_summary (Ticket_Id, Assigned_From, Assigned_To, Assigned_Date, Comment) VALUES ('$assign_ticket', '$posted_by', 
        							'$assign_agent', '$assigned_date', '$comment') " ;
										$stmt = $conn->prepare($sql);
        								$stmt->execute(); 

						}

							
						
					
						
						//SEND NOTIFICATIONs
						$sql= "SELECT incident.Affected_Person_Email,incident.Summary,incident.Assigned_To,incident.Details,users.Email 
						FROM incident INNER JOIN users ON users.Login_Id=incident.Logged_By_Id WHERE Ticket_Id='$assign_ticket' ";
						//echo $sql;die;
						$option_data="";
						$stmt = $conn->prepare($sql);
						$stmt->execute();
						
						$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
						$cc="";
						$summary="";
						$details="";
						$Who_is_working_on="";
						for ($y = 0; $y < count($res); $y++) 
						{
							$count=$y+1;
						$email1=$res[$y]['Affected_Person_Email'];
						$email2=$res[$y]['Email'];
						$subject=$res[$y]['Summary'];
						$details=$res[$y]['Details'];
						$Who_is_working_on=$res[$y]['Assigned_To'];
						
							//--------------GETTING NAME OF WHO IS WORKING ON----------------------------
								$sql= "SELECT Full_Name FROM users where Login_Id='$Who_is_working_on' ";
							$stmtV = $conn->prepare($sql);
							$stmtV->execute();
							$resV = $stmtV->fetchAll(PDO::FETCH_ASSOC);
							$WHO_IS_ON_IT=$resV[0]['Full_Name'];
							//-------------------------------------------------------------------
						
						
						$cc="Cc: $email1\r\n";
						$to=$email2;
							
							$message="
									<html>
									<body>
									Dear Cherished Client, Incident Number <b><font color='red'>$assign_ticket</font></b>  has been assigned to a Support Person.<br/>
									<b>Name Of Support Person:	$WHO_IS_ON_IT</b><br/>
									Thank You.
									<br/>
									<font color='red'>
									<i>
									$details
									</i>
									</font>
									<br/><a href='http://issues.unionsg.com'><u><b>Click Here to Login</b></u></a><br/>
									<img  src='http://unionsg.com/assets/img/logo.png' class='img-circle' />
									</body></html>
									";
							$subject="$summary ";
							
							send_notification($to,$subject,$message,$cc); // send Email alerts to user created
							
						
						}
						
						//SEND A MAIL TO THE SUPPORT GUY NOTIFYING HIM OF THE  ISSUE ASSIGNED TO HIM
						
						$sql= "SELECT users.Email ,users.F_Name
						FROM users WHERE Login_Id='$assign_agent' ";
						//echo $sql;die;
						$option_data="";
						$stmt = $conn->prepare($sql);
						$stmt->execute();
						
						 $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
						 $cc="";
						for ($y = 0; $y < count($res); $y++) 
						{
							$count=$y+1;
						$fname=$res[$y]['F_Name'];
						$support_email=$res[$y]['Email'];
						
						
						
						$cc="";
						$to=$support_email;
							
							$message="
									<html>
									<body>
									Dear $fname,  Incident Number <b><font color='red'>$assign_ticket</font></b>  has been assigned to You.<br/>
									
									
									Summary: $summary<br/>
									Notes: $details<br/>\
									
									Please Login to Support Platform To review it. <br/> Thank You.<br/>
									<a href='http://issues.unionsg.com'><u>Click here to login</u></a>
									<br/>
									<img  src='http://unionsg.com/assets/img/logo.png' class='img-circle' />
									</body></html>
									";
							$subject="$summary ";
							
							send_notification($to,$subject,$message,$cc); // send Email alerts to user created
							
						
						}
						
						if(!empty($comment))
						{
							
						$posted_by=$_SESSION['FNAME']; 
						$sql_comment= "INSERT INTO comments (Ticket_Id,Posted_By,Comment,Date_Posted,Is_Image_Attached) VALUES('$assign_ticket','$posted_by','$comment','$status_date','0') ";
						$stmt_comment = $conn->prepare($sql_comment);
						$stmt_comment->execute();
						}


						echo 1;


			}
?>
