$("#add").click(function(){
   validate();
});

function validate()
{

var company_name=$("#company_name").val();	
var organization=$("#organization").val();	
var department=$("#department").val();	
var region=$("#region").val();	
var site=$("#site").val();	
var phone=$("#phone_").val();	
var email=$("#email").val();	
var country=$("#country").val();	
var post_code=$("#post_code").val();	
var description=$("#description").val();
var file=document.getElementById("attachment").innerHTML;
	

$(".loader").show("slow");
if(company_name.length>2&&organization.length>2&&region.length>2&&site.length>2&&phone.length>0
&&email.length>2&&country.length>2&&post_code.length>0&&description.length>2)
{
	document.getElementById("add").disabled = true;
	echo("Adding Please wait..","danger");	
	$.post( "../js/add_company.php", 
	{ company_name: company_name,organization: organization,department: department,region: region,site: site,phone: phone,
	email: email,country: country,post_code: post_code,description: description,file:file })
	  .done(function( data ) {
	 
	console.log(data);
				if(data==1)
				{
					document.getElementById("form_content").innerHTML='<center> <h4 class="title">Client Created Sucessfullly</h4><br/><img src="../images/check.png" style="width:50px;"></center>';
				$("#preview").hide();
				echo("Client Created Sucessfullly","success");
						
				}
				else if(data==2)
				{
					echo("There Exist A Similar Client In The System","danger");	
				document.getElementById("add").disabled = false;					
				}
				else
				{
					$("#preview").hide();
				document.getElementById("form_content").innerHTML='<center><h4 class="title">Client Creation Failed</h4></br><img src="../images/failed.png" style="width:50px;"></center>';
					echo("Client Creation Failed","danger");		
				}
							
							});
}
else
{
echo("Please Fill Out All Fields..","danger");	
}
setTimeout(function(){$(".loader").hide("slow");}, 1000);
return;
}